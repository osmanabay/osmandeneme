﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BoltController : ControllerBase
    {
        IBoltService _BoltService;

        public BoltController(IBoltService BoltService)
        {
            _BoltService = BoltService;
        }


        [HttpGet("/Get")]
        public IActionResult GetList()
        {
            return Ok(_BoltService.GetList(null));
        }
        //Bolts
        [HttpPost("/api/Bolts")]
        public IActionResult GetList([FromBody] BoltRequest request)
        {
            return Ok(_BoltService.GetList(x=>x.DailyTeamWorkOrderId==request.Id, request.IsDto));
        }

        //Bolt/
        [HttpPost]
        public async Task<IActionResult> BoltCrud([FromBody] BoltRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_BoltService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Bolt Bolt = new Bolt
                {
                    DailyTeamWorkOrderId=request.Bolt.DailyTeamWorkOrderId,
                    Meter = request.Bolt.Meter,
                    WeftAmount = request.Bolt.WeftAmount,
                    TargetAmount=request.Bolt.TargetAmount

                };
                return Ok(await _BoltService.Add(Bolt,request.TagId));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _BoltService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Bolt Bolt = (Bolt)response.Data;
                    Bolt.Meter = request.Bolt.Meter;
                    Bolt.DailyTeamWorkOrderId = request.Bolt.DailyTeamWorkOrderId;

                    Bolt.TargetAmount = request.Bolt.TargetAmount;

                    return Ok(_BoltService.Update(Bolt));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _BoltService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Bolt Bolt = (Bolt)response.Data;
                    return Ok(_BoltService.Delete(Bolt));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}