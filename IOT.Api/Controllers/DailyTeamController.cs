﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DailyTeamController : ControllerBase
    {
        IDailyTeamService _dailyTeamService;

        public DailyTeamController(IDailyTeamService dailyTeamService)
        {
            _dailyTeamService = dailyTeamService;
        }

        //DailyTeam/
        [HttpPost]
        public IActionResult DailyTeamCrud([FromBody] DailyTeamRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_dailyTeamService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                DailyTeam item = new DailyTeam
                {
                    TeamId = request.DailyTeam.TeamId,
                    DeviceId = request.DailyTeam.DeviceId,
                    LeadEmployeeId = request.DailyTeam.LeadEmployeeId,
                    LeadEmployeeName = request.DailyTeam.LeadEmployeeName
                };
                return Ok(_dailyTeamService.Add(item));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _dailyTeamService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DailyTeam item = (DailyTeam)response.Data;
                    item.TeamId = request.DailyTeam.TeamId;
                    item.DeviceId = request.DailyTeam.DeviceId;
                    item.LeadEmployeeId = request.DailyTeam.LeadEmployeeId;
                    item.LeadEmployeeName = request.DailyTeam.LeadEmployeeName;
                    return Ok(_dailyTeamService.Update(item));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _dailyTeamService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DailyTeam item = (DailyTeam)response.Data;
                    return Ok(_dailyTeamService.Delete(item));
                }
                else
                    return Ok(response);
            }
            else if (request.IsAddDailyTeamEmployee)
            {
                return Ok(_dailyTeamService.AddDailyTeamEmployee(request.DailyTeamEmployee));
            }
            else if (request.IsDeleteDailyTeamEmployee)
            {
                return Ok(_dailyTeamService.DeleteDailyTeamEmployee(request.DailyTeamEmployee));
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }

    }
}