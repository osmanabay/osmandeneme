﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DailyTeamStopController : ControllerBase
    {
        IDailyTeamStopService _dailyTeamStopService;
        IDailyTeamService _dailyTeamService;
        ITagService _tagService;

        public DailyTeamStopController(IDailyTeamStopService dailyTeamStopService, IDailyTeamService dailyTeamService, ITagService tagService)
        {
            _dailyTeamStopService = dailyTeamStopService;
            _dailyTeamService = dailyTeamService;
            _tagService = tagService;
        }

        //DailyTeamStops
        [HttpPost("/api/DailyTeamStops")]
        public IActionResult GetList([FromBody] DailyTeamStopRequest request)
        {
            return Ok(_dailyTeamStopService.GetList(x => x.DailyTeamId == request.Id));
        }

        //DailyTeamStop/
        [HttpPost]
        public async Task<IActionResult> DailyTeamStopCrud([FromBody] DailyTeamStopRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_dailyTeamStopService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                if (request.DailyTeamStop.DailyTeamId == 0)
                {
                    ResponseData responseMongo = await _tagService.GetMongo(request.DailyTeamStop.TagId);
                    ETag tag = (ETag)responseMongo.Data;

                    DailyTeam dailyTeam = new DailyTeam();
                    dailyTeam.DeviceId = Convert.ToInt32(tag.DeviceId);
                    dailyTeam.TeamId = null;
                    dailyTeam.LeadEmployeeId = null;

                    ResponseData responseDailyTeam = _dailyTeamService.Add(dailyTeam);
                    dailyTeam =(DailyTeam)responseDailyTeam.Data;
                    request.DailyTeamStop.DailyTeamId = dailyTeam.Id;
                }
                request.DailyTeamStop.StartDate = DateTime.Now;
                return Ok(await _dailyTeamStopService.AddAsync(request.DailyTeamStop));
            }
            else if (request.Type == Enums.RequestTypes.Update && request.Id > 0)
            {
                ResponseData response = _dailyTeamStopService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DailyTeamStop item = (DailyTeamStop)response.Data;
                    request.DailyTeamStop.StartDate = item.StartDate;
                    request.DailyTeamStop.EndDate = DateTime.Now;
                    request.DailyTeamStop.Duration = Convert.ToInt32((request.DailyTeamStop.EndDate - request.DailyTeamStop.StartDate).Value.TotalSeconds);
                    request.DailyTeamStop.Id = item.Id;
                    request.DailyTeamStop.StopId = request.DailyTeamStop.StopId!=0?request.DailyTeamStop.StopId:item.StopId;
                    request.DailyTeamStop.DailyTeamId = item.DailyTeamId;
                    return Ok(await _dailyTeamStopService.UpdateAsync(request.DailyTeamStop));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _dailyTeamStopService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DailyTeamStop item = (DailyTeamStop)response.Data;
                    return Ok(_dailyTeamStopService.Delete(item));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}