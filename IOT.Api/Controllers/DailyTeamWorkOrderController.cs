﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DailyTeamWorkOrderController : ControllerBase
    {
        IDailyTeamWorkOrderService _service;
        IWorkOrderService _workOrderService;
        IDailyTeamService _dailyTeamService;
        IShiftService _shiftService;
        ITagService _tagService;
        IDeviceService _deviceService;



        public DailyTeamWorkOrderController(IDailyTeamWorkOrderService service,
            IShiftService shiftService,
            IWorkOrderService workOrderService,
            ITagService tagService,
             IDailyTeamService dailyTeamService,
             IDeviceService deviceService)
        {
            _workOrderService = workOrderService;
            _service = service;
            _shiftService = shiftService;
            _tagService = tagService;
            _dailyTeamService = dailyTeamService;
            _deviceService = deviceService;
        }

        //DailyTeamWorkOrder/
        [HttpPost]
        public async Task<IActionResult> DailyTeamWorkOrderCrud([FromBody] DailyTeamWorkOrderRequest request)
        {
            try
            {
                if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
                {
                    return Ok(_service.Get(x => x.Id == request.Id));
                }
                else if (request.Type == Enums.RequestTypes.Create)
                {
                    var now = DateTime.Now;
                    ResponseData shiftResponse = _shiftService.Get(x => (Convert.ToDateTime(x.StartDate).Hour <= DateTime.Now.Hour && Convert.ToDateTime(x.EndDate).Hour > DateTime.Now.Hour) || (Convert.ToDateTime(x.StartDate).Hour <= DateTime.Now.Hour && Convert.ToDateTime(x.EndDate).Hour < Convert.ToDateTime(x.StartDate).Hour));

                    Shift shift = (Shift)shiftResponse.Data;

                    DateTime ShiftStartDate = Convert.ToDateTime(shift.StartDate);
                    DateTime ShiftEndDate = Convert.ToDateTime(shift.EndDate);
                    int difference = 0;
                    if (ShiftStartDate > ShiftEndDate)
                    {
                        difference = Convert.ToInt32(24 * 60 + (ShiftEndDate - ShiftStartDate).TotalMinutes);
                    }
                    else
                    {
                        difference = Convert.ToInt32((ShiftEndDate - ShiftStartDate).TotalMinutes);
                    }
                    ResponseData workOrderResponse = _workOrderService.Get(x => x.Id == Convert.ToInt32(request.DailyTeamWorkOrder.WorkOrderId), false, false);
                    var workOrder = (WorkOrder)workOrderResponse.Data;

                    request.DailyTeamWorkOrder.WeftDensity = workOrder.WeftDensity;
                    request.DailyTeamWorkOrder.Sample = workOrder.Sample;
                    request.DailyTeamWorkOrder.ProductionContinue = workOrder.ProductionContinue;

                    DailyTeam dailyTeam = new DailyTeam();
                    DailyTeamWorkOrder item = new DailyTeamWorkOrder();
                   
                    if (shift.Id==Convert.ToInt32(request.DailyTeamWorkOrder.ShiftId))
                    {
                        item.DailyTeamId = request.DailyTeamWorkOrder.DailyTeamId;
                    }
                    
                    else if (request.DailyTeamWorkOrder.DailyTeamId == 0)
                    {
                        dailyTeam.TeamId = null;
                        dailyTeam.DeviceId = Convert.ToInt32(request.DailyTeamWorkOrder.DeviceId);
                        dailyTeam.LeadEmployeeId = null;
                        dailyTeam.LeadEmployeeName = null;
                        dailyTeam = (DailyTeam)_dailyTeamService.Add(dailyTeam).Data;
                        item.DailyTeamId = dailyTeam.Id;

                    }
                    else
                    {
                        item.DailyTeamId = request.DailyTeamWorkOrder.DailyTeamId;

                    }
                    if (item.DailyTeamId == 0)
                    {
                        dailyTeam.TeamId = null;
                        dailyTeam.DeviceId = Convert.ToInt32(request.DailyTeamWorkOrder.DeviceId);
                        dailyTeam.LeadEmployeeId = null;
                        dailyTeam.LeadEmployeeName = null;
                        dailyTeam = (DailyTeam)_dailyTeamService.Add(dailyTeam).Data;
                        item.DailyTeamId = dailyTeam.Id;
                    }
                    item.Id = 0;
                    item.WorkOrderId = request.DailyTeamWorkOrder.WorkOrderId;
                    item.ShiftId = shift.Id;
                    item.Status = request.DailyTeamWorkOrder.Status;
                    item.TargetAmount = workOrder.TargetAmount;
                    item.Amount = request.DailyTeamWorkOrder.Amount;
                    item.EndDate = ShiftEndDate;
                    item.StartDate = ShiftStartDate;
                    item.CreatedDate = now;

                    ResponseData response = await _service.Add(item, request.DailyTeamWorkOrder);

                    if (response.IsSucceed)
                    {
                        workOrder.Status = 1;
                        await _workOrderService.Update(workOrder, "", 0);
                        return Ok(response);
                    }

                    return Ok(null);
                }
                else if (request.Type == Enums.RequestTypes.Update)
                {
                    ResponseData response = _service.Get(x => x.Id == request.Id);
                    if (response.IsSucceed)
                    {
                        DailyTeamWorkOrder item = (DailyTeamWorkOrder)response.Data;
                        item.Amount = Convert.ToDecimal(request.DailyTeamWorkOrder.Amount);
                        item.TargetAmount = request.DailyTeamWorkOrder.TargetAmount;
                        item.ShiftId = request.DailyTeamWorkOrder.ShiftId;
                        item.StartDate = request.DailyTeamWorkOrder.StartDate;
                        item.EndDate = request.DailyTeamWorkOrder.EndDate;
                        item.WeftAmount = Convert.ToDecimal(request.DailyTeamWorkOrder.WeftAmount);
                        item.DailyTeamId = request.DailyTeamWorkOrder.DailyTeamId;
                        item.Status = request.DailyTeamWorkOrder.Status;
                        item.WorkOrderId = request.DailyTeamWorkOrder.WorkOrderId;
                        return Ok(_service.Update(item, request.DailyTeamWorkOrder));
                    }
                    else
                        return Ok(response);

                }
                else if (request.Type == Enums.RequestTypes.End)
                {
                    ResponseData response = _service.Get(x => x.Id == request.Id);
                    //ResponseData responseMongo = await _tagService.GetMongo(request.DailyTeamWorkOrder.TagId);
                    //ETag tag = (ETag)responseMongo.Data;
                    if (response.IsSucceed)
                    {
                        DailyTeamWorkOrder item = (DailyTeamWorkOrder)response.Data;
                        //item.Amount = Convert.ToDecimal(tag.TotalShiftMeter);
                        //item.Amount = Convert.ToDecimal(tag.WorkOrderShiftMeter);


                        //item.EndDate = DateTime.Now;
                        //item.TargetAmount = Convert.ToDecimal((item.EndDate - item.StartDate).Value.TotalSeconds / Convert.ToInt32(request.DailyTeamWorkOrder.CycleTime));
                        //item.TargetAmount = Convert.ToDecimal(tag.TargetAmount);
                        item.Status = 0;
                        return Ok(await _service.Update(item, request.DailyTeamWorkOrder));
                    }
                    else
                        return Ok(response);

                }
                else if (request.Type == Enums.RequestTypes.EndShift)
                {
                    ResponseData rMongo = new ResponseData();

                    var mongoTag = request.ETag.ToString();
                    //ResponseData rMongo = await _tagService.GetMongo((int)request.Id);
                    //if (rMongo.IsSucceed)
                    {
                        //ETag mTag = (ETag)rMongo.Data;

                        // Signal api tarafından object türünde gelen MTag string ile json formatında alınır ETag e çevrilmektedir.
                        ETag mTag = JsonConvert.DeserializeObject<ETag>(mongoTag);
                        //ETag mTag = (ETag)mongoTag;

                        int dailyTeamWorkOrderId = Convert.ToInt32(mTag.DailyTeamWorkOrderId);
                        ResponseData response = _service.Get(x => x.Id == dailyTeamWorkOrderId);
                        if (response.IsSucceed)
                        {
                            DailyTeamWorkOrder dailyTeamWorkOrder = (DailyTeamWorkOrder)response.Data;
                            //dailyTeamWorkOrder.Amount = Convert.ToDecimal(mTag.TotalShiftMeter);
                            dailyTeamWorkOrder.Amount = Convert.ToDecimal(mTag.WorkOrderShiftMeter);
                            dailyTeamWorkOrder.WeftAmount = Convert.ToDecimal(mTag.WorkOrderShiftWeftAmount);



                            dailyTeamWorkOrder.EndDate = DateTime.Now;
                            //item.TargetAmount = Convert.ToDecimal((item.EndDate - item.StartDate).Value.TotalSeconds / Convert.ToInt32(request.DailyTeamWorkOrder.CycleTime));
                            dailyTeamWorkOrder.Status = 0;
                            rMongo = await _service.ShiftEnd(dailyTeamWorkOrder, mTag);
                            mTag = (ETag)rMongo.Data;
                            //StartShift
                            dailyTeamWorkOrderId = Convert.ToInt32(mTag.DailyTeamWorkOrderId);
                            if (dailyTeamWorkOrderId == 0)
                            {
                                DailyTeam item = new DailyTeam
                                {
                                    TeamId = null,
                                    DeviceId = Convert.ToInt32(mTag.DeviceId),
                                    LeadEmployeeId = null,
                                    LeadEmployeeName = null
                                };
                                var dailyTeam = _dailyTeamService.Add(item);

                                var now = DateTime.Now;
                                ResponseData shiftResponse = _shiftService.Get(x => (Convert.ToDateTime(x.StartDate).Hour <= DateTime.Now.Hour && Convert.ToDateTime(x.EndDate).Hour > DateTime.Now.Hour) || (Convert.ToDateTime(x.StartDate).Hour <= DateTime.Now.Hour && Convert.ToDateTime(x.EndDate).Hour < Convert.ToDateTime(x.StartDate).Hour));

                                Shift shift = (Shift)shiftResponse.Data;

                                DateTime ShiftStartDate = Convert.ToDateTime(shift.StartDate);
                                DateTime ShiftEndDate = Convert.ToDateTime(shift.EndDate);
                                int difference = 0;
                                if (ShiftStartDate > ShiftEndDate)
                                {
                                    difference = Convert.ToInt32(24 * 60 + (ShiftEndDate - ShiftStartDate).TotalMinutes);

                                }
                                else
                                {
                                    difference = Convert.ToInt32((ShiftEndDate - ShiftStartDate).TotalMinutes);
                                }

                                DailyTeamWorkOrder dailyTeamWorkOrderStart = new DailyTeamWorkOrder();
                                dailyTeamWorkOrderStart.DailyTeamId = dailyTeam.Id;
                                dailyTeamWorkOrderStart.StartDate = DateTime.Now;
                                dailyTeamWorkOrderStart.WorkOrderId = Convert.ToInt32(mTag.WorkOrderId);
                                dailyTeamWorkOrderStart.Status = 1;
                                dailyTeamWorkOrderStart.ShiftId = shift.Id;
                                dailyTeamWorkOrderStart.TargetAmount = Convert.ToDecimal(mTag.TargetAmount);
                                dailyTeamWorkOrderStart.StartupTime = Convert.ToDecimal(mTag.StartupTime);
                                dailyTeamWorkOrderStart.TotalTimeout = 0;
                                mTag.ShiftStartDate = shift.StartDate;
                                mTag.ShiftEndDate = shift.EndDate;

                                return Ok(await _service.ShiftStart(dailyTeamWorkOrderStart, mTag));
                            }
                            else
                            {
                                rMongo.IsSucceed = false;
                                return Ok(rMongo);
                            }

                        }
                        else
                        {
                            response.IsSucceed = false;
                            return Ok(response);
                        }
                    }
                    //else
                    //{
                    //    rMongo.IsSucceed = false;
                    //    return Ok(rMongo);
                    //}
                }
                else if (request.Type == Enums.RequestTypes.StartShift)
                {
                    ResponseData rMongo = new ResponseData();
                    //ResponseData rMongo = await _tagService.GetMongo((int)request.Id);
                    //if (rMongo.IsSucceed)
                    var mongoTag = request.ETag.ToString();
                    {
                        //ETag mTag = (ETag)request.ETag;
                        ETag mTag = JsonConvert.DeserializeObject<ETag>(mongoTag);
                        int dailyTeamWorkOrderId = Convert.ToInt32(mTag.DailyTeamWorkOrderId);
                        if (dailyTeamWorkOrderId == 0)
                        {
                            DailyTeam item = new DailyTeam
                            {
                                TeamId = null,
                                DeviceId = Convert.ToInt32(mTag.DeviceId),
                                LeadEmployeeId = null,
                                LeadEmployeeName = null
                            };
                            var dailyTeam = _dailyTeamService.Add(item);

                            var now = DateTime.Now;
                            ResponseData shiftResponse = _shiftService.Get(x => (Convert.ToDateTime(x.StartDate).Hour <= DateTime.Now.Hour && Convert.ToDateTime(x.EndDate).Hour > DateTime.Now.Hour) || (Convert.ToDateTime(x.StartDate).Hour <= DateTime.Now.Hour && Convert.ToDateTime(x.EndDate).Hour < Convert.ToDateTime(x.StartDate).Hour));

                            Shift shift = (Shift)shiftResponse.Data;

                            DateTime ShiftStartDate = Convert.ToDateTime(shift.StartDate);
                            DateTime ShiftEndDate = Convert.ToDateTime(shift.EndDate);
                            int difference = 0;
                            if (ShiftStartDate > ShiftEndDate)
                            {
                                difference = Convert.ToInt32(24 * 60 + (ShiftEndDate - ShiftStartDate).TotalMinutes);

                            }
                            else
                            {
                                difference = Convert.ToInt32((ShiftEndDate - ShiftStartDate).TotalMinutes);
                            }

                            DailyTeamWorkOrder dailyTeamWorkOrder = new DailyTeamWorkOrder();
                            dailyTeamWorkOrder.DailyTeamId = dailyTeam.Id;
                            dailyTeamWorkOrder.StartDate = DateTime.Now;
                            dailyTeamWorkOrder.WorkOrderId = Convert.ToInt32(mTag.WorkOrderId);
                            dailyTeamWorkOrder.Status = 1;
                            dailyTeamWorkOrder.ShiftId = shift.Id;
                            dailyTeamWorkOrder.TargetAmount = Convert.ToDecimal(mTag.TargetAmount);
                            dailyTeamWorkOrder.StartupTime = Convert.ToDecimal(mTag.StartupTime);
                            dailyTeamWorkOrder.TotalTimeout = 0;
                            mTag.ShiftStartDate = shift.StartDate;
                            mTag.ShiftEndDate = shift.EndDate;

                            return Ok(await _service.ShiftStart(dailyTeamWorkOrder, mTag));
                        }
                        else
                        {
                            rMongo.IsSucceed = false;
                            return Ok(rMongo);
                        }


                    }
                    //else
                    //{
                    //    rMongo.IsSucceed = false;
                    //    return Ok(rMongo);
                    //}
                }
                //----------------------------------------------------------------------------------

                //ResponseData response = _service.Get(x => x.Id == request.Id);
                //ResponseData responseMongo = await _tagService.GetMongo(request.DailyTeamWorkOrder.TagId);
                //ETag tag = (ETag)responseMongo.Data;

                //if (response.IsSucceed)
                //{
                //    if (tag.DailyTeamWorkOrderId == request.DailyTeamWorkOrder.Id.ToString())
                //    {
                //        DailyTeamWorkOrder item = (DailyTeamWorkOrder)response.Data;
                //        item.Amount = Convert.ToDecimal(request.DailyTeamWorkOrder.Amount);
                //        item.EndDate = DateTime.Now;
                //        //item.TargetAmount = Convert.ToDecimal((item.EndDate - item.StartDate).Value.TotalSeconds / Convert.ToInt32(request.DailyTeamWorkOrder.CycleTime));
                //        item.TargetAmount = Convert.ToDecimal(tag.TargetAmount);
                //        item.Status = 0;
                //        return Ok(await _service.ShiftUpdate(item, request.DailyTeamWorkOrder));
                //    }
                //    else
                //    {
                //        response.IsSucceed = false;
                //        return Ok(response);
                //    }
                //}
                //else
                //    return Ok(response);

                else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
                {
                    ResponseData response = _service.Get(x => x.Id == request.Id);
                    if (response.IsSucceed)
                    {
                        DailyTeamWorkOrder item = (DailyTeamWorkOrder)response.Data;
                        return Ok(_service.Delete(item));
                    }
                    else
                        return Ok(response);
                }
                else
                {
                    ResponseData response = new ResponseData
                    {
                        IsSucceed = false,
                        Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                    };
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }

        }



    }
}