﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DailyTeamWorkOrder_ScrapController : ControllerBase
    {
        IDailyTeamWorkOrder_ScrapService _service;
        IShiftService _shiftService;
        IDeviceService _deviceService;

        public DailyTeamWorkOrder_ScrapController(IDailyTeamWorkOrder_ScrapService service,
            IShiftService shiftService,
            IDeviceService deviceService)
        {
            _service = service;
            _shiftService = shiftService;
            _deviceService = deviceService;
        }

        //DailyTeamWorkOrder_Scrap/
        [HttpPost]
        public async Task<IActionResult> DailyTeamWorkOrder_ScrapCrud([FromBody] DailyTeamWorkOrder_ScrapRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_service.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {


                DailyTeamWorkOrder_Scrap item = new DailyTeamWorkOrder_Scrap
                {
                    DailyTeamWorkOrderId = request.DailyTeamWorkOrder_Scrap.DailyTeamWorkOrderId,
                    ScrapId = request.DailyTeamWorkOrder_Scrap.ScrapId,
                    Amount = request.DailyTeamWorkOrder_Scrap.Amount,
                    CreatedDate = DateTime.Now
                };

                ResponseData response = await _service.Add(item, request.DailyTeamWorkOrder_Scrap);
              
                return Ok(response);
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _service.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DailyTeamWorkOrder_Scrap item = (DailyTeamWorkOrder_Scrap)response.Data;
                    item.Amount = request.DailyTeamWorkOrder_Scrap.Amount;
                    item.DailyTeamWorkOrderId = request.DailyTeamWorkOrder_Scrap.DailyTeamWorkOrderId;
                    item.ScrapId = request.DailyTeamWorkOrder_Scrap.ScrapId;
                    item.Amount = request.DailyTeamWorkOrder_Scrap.Amount;
                    return Ok(_service.Update(item));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _service.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DailyTeamWorkOrder_Scrap item = (DailyTeamWorkOrder_Scrap)response.Data;
                    return Ok(_service.Delete(item));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}