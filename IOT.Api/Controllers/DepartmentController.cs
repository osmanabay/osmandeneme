﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        //Departments
        [HttpPost("/api/Departments")]
        public IActionResult GetList([FromBody] DepartmentRequest request)
        {
            return Ok(_departmentService.GetList(null, request.IncludeFactory,request.IncludeDeviceGroups, request.IsDto));
        }

        //Department/
        [HttpPost]
        public IActionResult DepartmentCrud([FromBody] DepartmentRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_departmentService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Department department = new Department
                {
                    Name = request.Department.Name,
                    FactoryId=request.Department.FactoryId
                };
                return Ok(_departmentService.Add(department));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _departmentService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Department department = (Department)response.Data;
                    department.Name = request.Department.Name;
                    department.FactoryId = request.Department.FactoryId;
                    return Ok(_departmentService.Update(department));
                }
                else
                    return Ok(response);
             
            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _departmentService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Department department = (Department)response.Data;
                    return Ok(_departmentService.Delete(department));
                }else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}