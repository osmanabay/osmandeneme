﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        IDeviceService _deviceService;
        IDailyTeamService _dailyTeamService;
        public DeviceController(IDeviceService deviceService,
            IDailyTeamService dailyTeamService)
        {
            _dailyTeamService = dailyTeamService;
            _deviceService = deviceService;
        }

        //Devices
        [HttpPost("/api/Devices")]
        public IActionResult GetList([FromBody] DeviceRequest request)
        {
            if (request.Id==null || request.Id==0)
            {
                    return Ok(_deviceService.GetList(null, request.IncludeTags, request.IsDto));
            }
            else
            {
                return Ok(_deviceService.GetList(x => x.DeviceGroupId == request.Id, request.IncludeTags, request.IsDto));

            }
        }

        //Device/
        [HttpPost]
        public IActionResult DeviceCrud([FromBody] DeviceRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_deviceService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Device device = new Device
                {
                    Name = request.Device.Name,
                    DeviceNo=request.Device.DeviceNo,
                    IsOpened = request.Device.IsOpened,
                    DeviceGroupId = request.Device.DeviceGroupId
                };
                return Ok(_deviceService.Add(device));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _deviceService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Device device = (Device)response.Data;
                    device.Name = request.Device.Name;
                    device.DeviceNo = request.Device.DeviceNo;
                    device.IsOpened = request.Device.IsOpened;
                    device.DeviceGroupId = request.Device.DeviceGroupId;
                    return Ok(_deviceService.Update(device,request.Device));
                }
                else
                    return Ok(response);
            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _deviceService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Device device = (Device)response.Data;
                    return Ok(_deviceService.Delete(device));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }

        //Device/
        [HttpPost("/api/Device/Start")]
        public async Task<IActionResult> StartDevice([FromBody] TeamViewModel model)
        {

            ResponseData getresponse = _deviceService.Get(x => x.Id == model.DeviceDtos.SingleOrDefault(a => a.IsOpened == true).Id);
            if (getresponse.IsSucceed)
            {
                Device device = (Device)getresponse.Data;
                //device.IsOpened = true; 

                ResponseData updateresponse =await _deviceService.Update(device);
                if (updateresponse.IsSucceed)
                {
                    DailyTeam dailyTeam = new DailyTeam
                    {
                        DeviceId = device.Id,
                        TeamId = model.TeamDto.Id,
                        LeadEmployeeId = model.TeamDto.LeadEmployeeId,
                        LeadEmployeeName = model.TeamDto.LeadEmployeeName
                    };

                    // create DailyTeam
                    ResponseData createDailyTeamResponse = _dailyTeamService.Add(dailyTeam);
                    if (createDailyTeamResponse.IsSucceed) // isSucceed
                    {
                        ResponseData isStartDevice = new ResponseData();

                        // ekip liderinide eklemem gerekiyor.
                        EmployeeDto employeeDto = new EmployeeDto
                        {
                            Id = (int)model.TeamDto.LeadEmployeeId,
                            FullName = model.TeamDto.LeadEmployeeName
                        };

                        // employee Ienumerable olduğu için generic list oluşturdum.
                        List<EmployeeDto> employeeDtos = model.EmployeesByTeamDtos.ToList();
                        employeeDtos.Add(employeeDto);

                        foreach (var item in employeeDtos)
                        {
                            DailyTeamEmployee dailyTeamEmployee = new DailyTeamEmployee
                            {
                                DailyTeamId = dailyTeam.Id,
                                EmployeeId = item.Id,
                            };

                            ResponseData createDailyTeamEmployee = _dailyTeamService.AddDailyTeamEmployee(dailyTeamEmployee);
                            // create dailyteamemployee
                            if (createDailyTeamEmployee.IsSucceed) // isSucced
                            {

                                isStartDevice.IsSucceed = true;
                                var data = new { deviceName = device.Name, dailyTeamId = createDailyTeamResponse.Id };
                                isStartDevice.Data = data;

                            }
                        }
                        if (isStartDevice.IsSucceed)
                        {
                            return Ok(isStartDevice);
                        }
                    }
                }
            }

            ResponseData response = new ResponseData
            {
                IsSucceed = false,
                Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

            };
            return Ok(response);
        }


    }
}