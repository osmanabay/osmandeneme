﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceGroupController : ControllerBase
    {
        IDeviceGroupService _deviceGroupService;
        IHttpContextAccessor _accessor;
        public DeviceGroupController(IDeviceGroupService deviceGroupService, IHttpContextAccessor accessor)
        {
            _deviceGroupService = deviceGroupService;
            _accessor = accessor;

        }

        //DeviceGroups
        [HttpPost("/api/DeviceGroups")]
        public IActionResult GetList([FromBody] DeviceGroupRequest request)
        {
            return Ok(_deviceGroupService.GetList(null, request.IncludeDevice, request.IncludeDeviceGroupStop, request.IsDto));
        }

        [HttpGet("/api/GetIpAddress")]
        public IActionResult GetIpAddress()
        {
            ResponseData response = new ResponseData();
            try
            {
                var clientIPAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                response.Data = clientIPAddress;
                response.IsSucceed = true;
                return Ok(response);
            }
            catch (System.Exception ex)
            {
                response.IsSucceed = false;
                response.Message = ex.Message;
                return Ok(response);
            }

        }

        //DeviceGroup/
        [HttpPost]
        public async Task<IActionResult> DeviceGroupCrud([FromBody] DeviceGroupRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_deviceGroupService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                DeviceGroup deviceGroup = new DeviceGroup
                {
                    Name = request.DeviceGroup.Name,
                    DepartmentId = request.DeviceGroup.DepartmentId,
                    IPAddress = request.DeviceGroup.IPAddress
                };
                return Ok(_deviceGroupService.Add(deviceGroup));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _deviceGroupService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DeviceGroup deviceGroup = (DeviceGroup)response.Data;
                    deviceGroup.Name = request.DeviceGroup.Name;
                    deviceGroup.DepartmentId = request.DeviceGroup.DepartmentId;
                    deviceGroup.IPAddress = request.DeviceGroup.IPAddress;

                    return Ok(_deviceGroupService.Update(deviceGroup));
                }
                else
                    return Ok(response);
            }
            else if (request.Type == Enums.RequestTypes.Any)
            {
                ResponseData response =await  _deviceGroupService.Any(request);
                return Ok(response);
            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _deviceGroupService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DeviceGroup deviceGroup = (DeviceGroup)response.Data;
                    return Ok(_deviceGroupService.Delete(deviceGroup));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }
    }
}