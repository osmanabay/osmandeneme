﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceGroupStopController : ControllerBase
    {
        IDeviceGroupStopService _deviceGroupStopService;
        public DeviceGroupStopController(IDeviceGroupStopService deviceGroupStopService)
        {
            _deviceGroupStopService = deviceGroupStopService;
        }

        //DeviceGroupStops
        [HttpPost("/api/DeviceGroupStops")]
        public IActionResult GetList([FromBody] DeviceGroupStopRequest request)
        {
            return Ok(_deviceGroupStopService.GetList(null, request._includeDeviceGroup, request._includeStop, request.IsDto));
        }

        //DeviceGroupStop/
        [HttpPost]
        public IActionResult DeviceGroupStopCrud([FromBody] DeviceGroupStopRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_deviceGroupStopService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                DeviceGroupStop deviceGroupStop = new DeviceGroupStop
                {
                    DeviceGroupId = request.DeviceGroupStop.DeviceGroupId,
                    StopId = request.DeviceGroupStop.StopId,
                    StartDate=request.DeviceGroupStop.StartDate,
                    EndDate=request.DeviceGroupStop.EndDate,
                };
                return Ok(_deviceGroupStopService.Add(deviceGroupStop));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _deviceGroupStopService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DeviceGroupStop item = (DeviceGroupStop)response.Data;
                    item.DeviceGroupId = request.DeviceGroupStop.DeviceGroupId;
                    item.StopId = request.DeviceGroupStop.StopId;
                    item.StartDate = request.DeviceGroupStop.StartDate;
                    item.EndDate = request.DeviceGroupStop.EndDate;
                    return Ok(_deviceGroupStopService.Update(item));
                }
                else
                    return Ok(response);
            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _deviceGroupStopService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    DeviceGroupStop deviceGroupStop = (DeviceGroupStop)response.Data;
                    return Ok(_deviceGroupStopService.Delete(deviceGroupStop));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }
    }
}