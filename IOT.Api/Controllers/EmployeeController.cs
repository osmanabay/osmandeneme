﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        IEmployeeService _employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        //Employees
        [HttpPost("/api/Employees")]
        public IActionResult GetList([FromBody] EmployeeRequest request)
        {
            return Ok(_employeeService.GetList(null, request.IncludeTeams, request.IsDto));
        }

        //Employee/
        [HttpPost]
        public IActionResult EmployeeCrud([FromBody] EmployeeRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_employeeService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Employee employee = new Employee
                {
                    FirstName = request.Employee.FirstName,
                    LastName = request.Employee.LastName,
                    NickName = request.Employee.NickName,
                    Password = request.Employee.Password,
                };
                return Ok(_employeeService.Add(employee));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _employeeService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Employee employee = (Employee)response.Data;
                    employee.FirstName = request.Employee.FirstName;
                    employee.LastName = request.Employee.LastName;
                    employee.Id = request.Employee.Id;
                    employee.Password = request.Employee.Password;
                    return Ok(_employeeService.Update(employee));
                }
                else
                    return Ok(response);
             
            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _employeeService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Employee employee = (Employee)response.Data;
                    return Ok(_employeeService.Delete(employee));
                }else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }
    }
}