﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FactoryController : ControllerBase
    {
        IFactoryService _factoryService;

        public FactoryController(IFactoryService factoryService)
        {
            _factoryService = factoryService;
        }


        [HttpGet("/Get")]
        public IActionResult GetList()
        {
            return Ok(_factoryService.GetList(null));
        }
        //Factorys
        [HttpPost("/api/Factories")]
        public IActionResult GetList([FromBody] FactoryRequest request)
        {
            return Ok(_factoryService.GetList(null, request.IncludeDepartments, request.IsDto));
        }

        //Factory/
        [HttpPost]
        public IActionResult FactoryCrud([FromBody] FactoryRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_factoryService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Factory factory = new Factory
                {
                    Name = request.Factory.Name
                };
                return Ok(_factoryService.Add(factory));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _factoryService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Factory factory = (Factory)response.Data;
                    factory.Name = request.Factory.Name;
                    return Ok(_factoryService.Update(factory));
                }
                else
                    return Ok(response);
             
            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _factoryService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Factory factory = (Factory)response.Data;
                    return Ok(_factoryService.Delete(factory));
                }else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}