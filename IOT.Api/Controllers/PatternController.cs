﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatternController : ControllerBase
    {
        IPatternService _patternService;

        public PatternController(IPatternService patternService)
        {
            _patternService = patternService;
        }

        //Patterns
        [HttpPost("/api/Patterns")]
        public IActionResult GetList([FromBody] PatternRequest request)
        {
            return Ok(_patternService.GetList(null, request.IncludeProducts, request.IsDto));
        }

        //Pattern/
        [HttpPost]
        public IActionResult PatternCrud([FromBody] PatternRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_patternService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Pattern item = new Pattern
                {
                    Name = request.Pattern.Name,
                    No=request.Pattern.No
                };
                return Ok(_patternService.Add(item));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _patternService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Pattern item = (Pattern)response.Data;
                    item.Name = request.Pattern.Name;
                    item.No = request.Pattern.No;
                    return Ok(_patternService.Update(item));
                }
                else
                    return Ok(response);
             
            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _patternService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Pattern item = (Pattern)response.Data;
                    return Ok(_patternService.Delete(item));
                }else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}