﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        IProductService _productService;

        public ProductController(IProductService patternService)
        {
            _productService = patternService;
        }

        //Products
        [HttpPost("/api/Products")]
        public IActionResult GetList([FromBody] ProductRequest request)
        {
            return Ok(_productService.GetList(null, request.IncludePattern, request.IsDto));
        }

        //Product/
        [HttpPost]
        public IActionResult ProductCrud([FromBody] ProductRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_productService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Product item = new Product
                {
                    Name = request.Product.Name,
                    PatternId = request.Product.PatternId,
                    StockCode = request.Product.StockCode,
                    Type = request.Product.Type,
                    WeftDensity=request.Product.WeftDensity
                };
                return Ok(_productService.Add(item));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _productService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Product item = (Product)response.Data;
                    item.Name = request.Product.Name;
                    item.PatternId = request.Product.PatternId;
                    item.StockCode = request.Product.StockCode;
                    item.Type = request.Product.Type;
                    item.WeftDensity = request.Product.WeftDensity;

                    return Ok(_productService.Update(item));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _productService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Product item = (Product)response.Data;
                    return Ok(_productService.Delete(item));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}