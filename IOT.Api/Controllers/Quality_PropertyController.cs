﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Quality_PropertyController : ControllerBase
    {
        IQuality_PropertyService _patternService;

        public Quality_PropertyController(IQuality_PropertyService patternService)
        {
            _patternService = patternService;
        }

        //Quality_Propertys
        [HttpPost("/api/Quality_Propertys")]
        public IActionResult GetList([FromBody] Quality_PropertyRequest request)
        {
            return Ok(_patternService.GetList(null, request.IncludeProduct,request.IncludeQualitys, request.IsDto));
        }

        //Quality_Property/
        [HttpPost]
        public IActionResult Quality_PropertyCrud([FromBody] Quality_PropertyRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_patternService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Quality_Property item = new Quality_Property
                {
                    Name = request.Quality_Property.Name,
                    ProductId = request.Quality_Property.ProductId,
                    Value = request.Quality_Property.Value,
                    
                };
                return Ok(_patternService.Add(item));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _patternService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Quality_Property item = (Quality_Property)response.Data;
                    item.Name = request.Quality_Property.Name;
                    item.ProductId = request.Quality_Property.ProductId;
                    item.Value = request.Quality_Property.Value;
                    return Ok(_patternService.Update(item));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _patternService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Quality_Property item = (Quality_Property)response.Data;
                    return Ok(_patternService.Delete(item));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}