﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScrapController : ControllerBase
    {
        IScrapService _ScrapService;

        public ScrapController(IScrapService ScrapService)
        {
            _ScrapService = ScrapService;
        }


        [HttpGet("/Get")]
        public IActionResult GetList()
        {
            return Ok(_ScrapService.GetList(null));
        }
        //Scraps
        [HttpPost("/api/Scraps")]
        public IActionResult GetList([FromBody] ScrapRequest request)
        {
            return Ok(_ScrapService.GetList(null,  request.IsDto));
        }

        //Scrap/
        [HttpPost]
        public IActionResult ScrapCrud([FromBody] ScrapRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_ScrapService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Scrap Scrap = new Scrap
                {
                    Name = request.Scrap.Name
                };
                return Ok(_ScrapService.Add(Scrap));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _ScrapService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Scrap Scrap = (Scrap)response.Data;
                    Scrap.Name = request.Scrap.Name;
                    return Ok(_ScrapService.Update(Scrap));
                }
                else
                    return Ok(response);
             
            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _ScrapService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Scrap Scrap = (Scrap)response.Data;
                    return Ok(_ScrapService.Delete(Scrap));
                }else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}