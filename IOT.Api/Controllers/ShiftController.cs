﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShiftController : ControllerBase
    {
        IShiftService _shiftService;
        public ShiftController(IShiftService shiftService)
        {
            _shiftService = shiftService;
        }

        //Shifts
        [HttpPost("/api/Shifts")]
        public IActionResult GetList([FromBody] ShiftRequest request)
        {
            return Ok(_shiftService.GetList(null, request.IsDto));
        }

        //Shift/
        [HttpPost]
        public IActionResult ShiftCrud([FromBody] ShiftRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_shiftService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Shift shift = new Shift
                {
                    Name = request.Shift.Name,
                    StartDate=request.Shift.StartDate,
                    EndDate=request.Shift.EndDate
                };
                return Ok(_shiftService.Add(shift));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _shiftService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Shift shift = (Shift)response.Data;
                    shift.Name = request.Shift.Name;
                    shift.StartDate = request.Shift.StartDate;
                    shift.EndDate = request.Shift.EndDate;
                    return Ok(_shiftService.Update(shift));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _shiftService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Shift shift = (Shift)response.Data;
                    return Ok(_shiftService.Delete(shift));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}