﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StopController : ControllerBase
    {
        IStopService _stopService;
        public StopController(IStopService stopService)
        {
            _stopService = stopService;
        }

        //Stops
        [HttpPost("/api/Stops")]
        public IActionResult GetList([FromBody] StopRequest request)
        {
            return Ok(_stopService.GetList(null, request.includeStopType, request.IsDto));
        }

        //Stop/
        [HttpPost]
        public IActionResult StopCrud([FromBody] StopRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_stopService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Stop stop = new Stop
                {
                    Name = request.Stop.Name,
                    StopTypeId=request.Stop.StopTypeId,
                    Duration=request.Stop.Duration,
                };
                return Ok(_stopService.Add(stop));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _stopService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Stop stop = (Stop)response.Data;
                    stop.Name = request.Stop.Name;
                    stop.StopTypeId = request.Stop.StopTypeId;
                    stop.Duration = request.Stop.Duration;
                    return Ok(_stopService.Update(stop));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _stopService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Stop stop = (Stop)response.Data;
                    return Ok(_stopService.Delete(stop));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}