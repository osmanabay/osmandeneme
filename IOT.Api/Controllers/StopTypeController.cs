﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StopTypeController : ControllerBase
    {
        IStopTypeService _stopTypeService;
        public StopTypeController(IStopTypeService stopTypeService)
        {
            _stopTypeService = stopTypeService;
        }

        //StopTypes
        [HttpPost("/api/StopTypes")]
        public IActionResult GetList([FromBody] StopTypeRequest request)
        {
            return Ok(_stopTypeService.GetList(null,request.IncludeStops,request.IsDto));
        }

        //StopType/
        [HttpPost]
        public IActionResult StopTypeCrud([FromBody] StopTypeRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_stopTypeService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                StopType stopType = new StopType
                {
                    Name = request.StopType.Name
                };
                return Ok(_stopTypeService.Add(stopType));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _stopTypeService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    StopType stopType = (StopType)response.Data;
                    stopType.Name = request.StopType.Name;
                    return Ok(_stopTypeService.Update(stopType));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _stopTypeService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    StopType stopType = (StopType)response.Data;
                    return Ok(_stopTypeService.Delete(stopType));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}