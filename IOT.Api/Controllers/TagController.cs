﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagController : ControllerBase
    {
        ITagService _tagService;
        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }

        //Tags
        [HttpPost("/api/Tags")]
        public IActionResult GetList([FromBody] TagRequest request)
        {
            return Ok(_tagService.GetList(null, request.includeTagType, request.includeDevice, request.IsDto));
        }

        //Tag/
        [HttpPost]
        public async Task<IActionResult> TagCrud([FromBody] TagRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_tagService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.ReadMongo && request.Id > 0)
            {
                var asd = await _tagService.GetMongo((int)request.Id);
                return Ok(asd);
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Tag tag = new Tag
                {
                    Name = request.Tag.Name,
                    TypeId=request.Tag.TypeId,
                    DeviceId=request.Tag.DeviceId,
                    Value=0,
                    SignalFromWhere=request.Tag.SignalFromWhere,
                    CycleTime=request.Tag.CycleTime,
                    StartupTime= request.Tag.StartupTime,
                    TimeOut= request.Tag.TimeOut,
                };
                return Ok(await _tagService.Add(tag));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _tagService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Tag tag = (Tag)response.Data;
                    tag.Id = request.Tag.Id;
                    tag.Name = request.Tag.Name;
                    tag.TypeId = request.Tag.TypeId;
                    tag.DeviceId = request.Tag.DeviceId;
                    tag.SignalFromWhere = request.Tag.SignalFromWhere;
                    tag.CycleTime = 0;
                    tag.StartupTime = request.Tag.StartupTime;
                    tag.TimeOut = request.Tag.TimeOut;

                    //ETag tag = (ETag)response.Data;
                    //tag.Name = request.Tag.Name;
                    //tag.TypeId = request.Tag.TypeId.ToString();
                    //tag.DeviceId = request.Tag.DeviceId.ToString();
                    //tag.SignalFromWhere = request.Tag.SignalFromWhere.ToString();
                    //tag.CycleTime = request.Tag.CycleTime.ToString();
                    //tag.StartupTime = request.Tag.StartupTime.ToString();
                    //tag.TimeOut = request.Tag.TimeOut.ToString();
                    return Ok(_tagService.Update(tag));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _tagService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Tag tag = (Tag)response.Data;
                    return Ok(_tagService.Delete(tag));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}