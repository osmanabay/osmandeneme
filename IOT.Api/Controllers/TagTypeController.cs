﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagTypeController : ControllerBase
    {
        ITagTypeService _tagTypeService;
        public TagTypeController(ITagTypeService tagTypeService)
        {
            _tagTypeService = tagTypeService;
        }

        //TagTypes
        [HttpPost("/api/TagTypes")]
        public IActionResult GetList([FromBody] TagTypeRequest request)
        {
            return Ok(_tagTypeService.GetList(null, request.IsDto));
        }

        //TagType/
        [HttpPost]
        public IActionResult TagTypeCrud([FromBody] TagTypeRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_tagTypeService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                TagType tagType = new TagType
                {
                    Name = request.TagType.Name
                };
                return Ok(_tagTypeService.Add(tagType));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _tagTypeService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    TagType tagType = (TagType)response.Data;
                    tagType.Name = request.TagType.Name;
                    return Ok(_tagTypeService.Update(tagType));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _tagTypeService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    TagType tagType = (TagType)response.Data;
                    return Ok(_tagTypeService.Delete(tagType));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}