﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        ITeamService _teamService;
        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        //Teams
        [HttpPost("/api/Teams")]
        public IActionResult GetList([FromBody] TeamRequest request)
        {
            return Ok(_teamService.GetList(null,request.IncludeEmployee, request.IsDto));
        }


        

        //Team/
        [HttpPost]
        public IActionResult TeamCrud([FromBody] TeamRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_teamService.Get(x => x.Id == request.Id));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                Team team = new Team
                {
                    Name = request.Team.Name,
                    LeadEmployeeName=request.Team.LeadEmployeeName,
                    LeadEmployeeId=request.Team.LeadEmployeeId
                };
                return Ok(_teamService.Add(team));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _teamService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Team team = (Team)response.Data;
                    team.Name = request.Team.Name;
                    team.LeadEmployeeName = request.Team.LeadEmployeeName;
                    team.LeadEmployeeId = request.Team.LeadEmployeeId;
                    return Ok(_teamService.Update(team));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _teamService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    Team Team = (Team)response.Data;
                    return Ok(_teamService.Delete(Team));
                }
                else
                    return Ok(response);
            }
            else if (request.Type == Enums.RequestTypes.Any)
            {

                return Ok(_teamService.Any(request));
            }
            else if (request.IsAddTeamEmployee)
            {
                return Ok(_teamService.AddTeamEmployee(request.TeamEmployee));
            }
            else if (request.IsDeleteTeamEmployee)
            {
                return Ok(_teamService.DeleteTeamEmployee(request.TeamEmployee));
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}