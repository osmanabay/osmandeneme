﻿using IOT.Business.Abstract;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using IOT.Entities.Concrete;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WarpStockController : ControllerBase
    {
        readonly IWarpStockService _warpStockService;

        public WarpStockController(IWarpStockService warpStockService)
        {
            _warpStockService = warpStockService;
        }


        [HttpGet("/GetList")]
        public IActionResult GetList()
        {
            return Ok(_warpStockService.GetList(null));
        }
        //WarpStocks
        [HttpPost("/api/WarpStocks")]
        public IActionResult GetList([FromBody] WarpStockRequest request)
        {
            return Ok(_warpStockService.GetList(x=>x.WarpStockNo==request.WarpStock.WarpStockNo && x.Status!=3));
        }

        //WarpStock/
        [HttpPost]
        public async Task<IActionResult> WarpStockCrud([FromBody] WarpStockRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read)
            {
                return Ok(_warpStockService.Get(x=>x.WarpStockNo==request.WarpStock.WarpStockNo));
            }
            else if (request.Type == Enums.RequestTypes.Create)
            {
                WarpStock warpStock = new WarpStock
                {
                    WarpStockMeter = request.WarpStock.WarpStockMeter,
                    WarpStockNo = request.WarpStock.WarpStockNo,
                    LeftMeter = request.WarpStock.LeftMeter,
                    WorkOrderId=request.WarpStock.WorkOrderId,
                    UsedMeter = 0,
                    Status = 1

                };
                return Ok(await _warpStockService.Add(warpStock, request.TagId));
            }
            else if (request.Type == Enums.RequestTypes.Update)
            {
                ResponseData response = _warpStockService.Get(x => x.WarpStockNo == request.WarpStock.WarpStockNo);
                if (response.IsSucceed)
                {
                    WarpStock warpStock = (WarpStock)response.Data;
                    if (warpStock!=null)
                    {
                        if (request.WarpStock.Status==1)
                        {
                            warpStock.Status = request.WarpStock.Status;
                            warpStock.WarpStockMeter = warpStock.LeftMeter;
                            warpStock.UsedMeter = 0;
                        }
                        else
                        {
                            //warpStock.WarpStockMeter = request.WarpStock.WarpStockMeter;
                            warpStock.LeftMeter = request.WarpStock.LeftMeter;
                            warpStock.Status = request.WarpStock.Status;
                            warpStock.CutWarpDate = DateTime.Now.AddHours(1);
                            warpStock.UsedMeter = request.WarpStock.UsedMeter;
                        }
                        
                    }
                    else
                    {
                        response.IsSucceed = false;
                        response.Data = null;
                        return Ok(response);
                    }
                    var responseWarpStock =await _warpStockService.Update(warpStock, request.TagId);
                    return Ok(responseWarpStock);
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _warpStockService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    WarpStock warpStock = (WarpStock)response.Data;
                    return Ok(_warpStockService.Delete(warpStock));
                }
                else
                    return Ok(response);
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}