﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkOrderController : ControllerBase
    {
        IWorkOrderService _itemService;
        IDailyTeamWorkOrderService _dailyTeamWorkOrderService;
        public WorkOrderController(IWorkOrderService itemService,
            IDailyTeamWorkOrderService dailyTeamWorkOrderService)
        {
            _itemService = itemService;
            _dailyTeamWorkOrderService = dailyTeamWorkOrderService;
        }

        //WorkOrders
        [HttpPost("/api/WorkOrders")]
        public IActionResult GetList([FromBody] WorkOrderRequest request)
        {
            if (request.IsOpened==false)
            {
                return Ok(_itemService.GetList(null, request.IsDto,request.includeWarpStock,request.includeDevice));
            }
            else
            {
                return Ok(_itemService.GetList(x => x.IsOpened == request.IsOpened && x.DeviceId==request.deviceId, request.IsDto, request.includeWarpStock));

            }
        }

        //WorkOrder/
        [HttpPost]
        public async Task<IActionResult> WorkOrderCrud([FromBody] WorkOrderRequest request)
        {
            if (request.Type == Enums.RequestTypes.Read && request.Id > 0)
            {
                return Ok(_itemService.Get(x => x.Id == request.Id, request.includeProduct, request.IsDto));
            }
            else if (request.Type == Enums.RequestTypes.Create && request.WorkOrder != null)
            {
               
                WorkOrder item = new WorkOrder
                {
                    TargetAmount = request.WorkOrder.TargetAmount,
                    ProductId = request.WorkOrder.ProductId,
                    DeviceId=request.WorkOrder.DeviceId,
                    Amount = request.WorkOrder.Amount,
                    WeftAmount=0,
                    IsOpened = true,
                    No = request.WorkOrder.No,
                    WarpNo=request.WorkOrder.WarpNo,
                    CreatedDate = DateTime.Now,
                    Sample=request.WorkOrder.Sample,
                    ProductionContinue=request.WorkOrder.ProductionContinue
                };
                if (request.WorkOrder.Sample)
                {
                    item.No = "#"+DateTime.Now.ToString().Replace(".","").Replace(" ","").Replace(":","");
                }
                return Ok(_itemService.Add(item));
            }
            else if (request.Type == Enums.RequestTypes.Update && request.WorkOrder != null)
            {
                ResponseData response = _itemService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    WorkOrder item = (WorkOrder)response.Data;
                    item.No = request.WorkOrder.No;
                    item.Id = request.WorkOrder.Id;
                    item.IsOpened = request.WorkOrder.IsOpened;
                    item.ProductId = request.WorkOrder.ProductId;
                    item.TargetAmount = request.WorkOrder.TargetAmount;
                    item.Amount = request.WorkOrder.Amount;
                    item.WarpNo= request.WorkOrder.WarpNo;
                    item.ModifiedDate = DateTime.Now;
                    return Ok(_itemService.Update(item,"",0));
                }
                else
                    return Ok(response);

            }
            else if (request.Type == Enums.RequestTypes.Delete && request.Id > 0)
            {
                ResponseData response = _itemService.Get(x => x.Id == request.Id);
                if (response.IsSucceed)
                {
                    WorkOrder WorkOrder = (WorkOrder)response.Data;
                    WorkOrder.ModifiedDate = DateTime.Now;
                    return Ok(_itemService.Delete(WorkOrder));
                }
                else
                    return Ok(response);
            }
            else if (request.Type == Enums.RequestTypes.End && request.Id>0)
            {
                ResponseData response = _itemService.Get(x => x.Id == request.Id);
                WorkOrder w = (WorkOrder)response.Data;
                if (response.IsSucceed)
                {
                    ResponseData responseDailyTeamWorkOrders = _dailyTeamWorkOrderService.GetList(x => x.WorkOrderId == request.Id);
                    if (responseDailyTeamWorkOrders.IsSucceed)
                    {
                        List<DailyTeamWorkOrder> dtwoList = (List<DailyTeamWorkOrder>)responseDailyTeamWorkOrders.Data;

                      WorkOrder item = (WorkOrder)response.Data;
                        item.IsOpened = false;
                        item.TargetAmount = w.TargetAmount;
                        item.Amount = dtwoList.Where(y=>y.Amount!=null).Sum(x => x.Amount);
                        item.StartDate = dtwoList.OrderBy(x => x.StartDate).First().StartDate;
                        item.EndDate = dtwoList.OrderBy(x => x.EndDate).Last().EndDate;
                        item.ModifiedDate = DateTime.Now;
                        return Ok(await _itemService.Update(item, request.deviceName,Convert.ToInt32(request.tagId)));
                    }
                    else
                        return Ok(responseDailyTeamWorkOrders);


                }
                else
                    return Ok(response);

            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}