﻿using IOT.Business.Abstract;
using IOT.Business.Concrete;
using IOT.DataAccess.Abstract;
using IOT.DataAccess.Concrete.EntityFramework;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace IOT.Api
{
    public class Startup
    {
        private IConfigurationRoot _appSettings;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _appSettings = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json")
          .Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        public void ConfigureServices(IServiceCollection services)
        {
            DBIOTContext.ConnectionString = _appSettings.GetConnectionString("EfContext");
            DBIOTContext.SignalUrl = _appSettings.GetSection("SignalUrl").Value;
            ScopedItems(services);
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder => {
                builder
                .SetIsOriginAllowed(isOriginAllowed: _ => true)

                .AllowAnyMethod()
                .AllowAnyHeader()
                //.AllowAnyOrigin()
                .AllowCredentials();
                //.WithOrigins("http://localhost:4200");
            }));

            services.AddMvc();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                             .AddJsonOptions(opt => opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("CorsPolicy");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

        }

        public void ScopedItems(IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddHttpContextAccessor();

            // Mssql Database
            services.AddScoped<DBIOTContext>();

            //Scrap
            services.AddScoped<IScrapService, ScrapManager>();
            services.AddScoped<IScrapDal, EfScrapDal>();

            //Bolt
            services.AddScoped<IBoltService, BoltManager>();
            services.AddScoped<IBoltDal, EfBoltDal>();

            //WarpStock
            services.AddScoped<IWarpStockService, WarpStockManager>();
            services.AddScoped<IWarpStockDal, EfWarpStockDal>();

            //DailyTeamWorkOrder_scrap
            services.AddScoped<IDailyTeamWorkOrder_ScrapService, DailyTeamWorkOrder_ScrapManager>();
            services.AddScoped<IDailyTeamWorkOrder_ScrapDal, EfDailyTeamWorkOrder_ScrapDal>();

            //Device
            services.AddScoped<IDeviceService, DeviceManager>();
            services.AddScoped<IDeviceDal, EfDeviceDal>();

            //Team
            services.AddScoped<ITeamService, TeamManager>();
            services.AddScoped<ITeamDal, EfTeamDal>();

            //DailyTeam
            services.AddScoped<IDailyTeamService, DailyTeamManager>();
            services.AddScoped<IDailyTeamDal, EfDailyTeamDal>();

            //Factory
            services.AddScoped<IFactoryService, FactoryManager>();
            services.AddScoped<IFactoryDal, EfFactoryDal>();

            //Department
            services.AddScoped<IDepartmentService, DepartmentManager>();
            services.AddScoped<IDepartmentDal, EfDepartmentDal>();

            //Shift
            services.AddScoped<IShiftService, ShiftManager>();
            services.AddScoped<IShiftDal, EfShiftDal>();


            //DailyTeamStop
            services.AddScoped<IDailyTeamStopService, DailyTeamStopManager>();
            services.AddScoped<IDailyTeamStopDal, EfDailyTeamStopDal>();


            //DailyTeamWorkOrder
            services.AddScoped<IDailyTeamWorkOrderService, DailyTeamWorkOrderManager>();
            services.AddScoped<IDailyTeamWorkOrderDal, EfDailyTeamWorkOrderDal>();




            //DeviceGroup
            services.AddScoped<IDeviceGroupService, DeviceGroupManager>();
            services.AddScoped<IDeviceGroupDal, EfDeviceGroupDal>();

            //DeviceGroupStop
            services.AddScoped<IDeviceGroupStopService, DeviceGroupStopManager>();
            services.AddScoped<IDeviceGroupStopDal, EfDeviceGroupStopDal>();

            //WorkOrder1
            services.AddScoped<IWorkOrder1Service, WorkOrder1Manager>();
            services.AddScoped<IWorkOrder1Dal, EfWorkOrder1Dal>();


            //WorkOrder
            services.AddScoped<IWorkOrderService, WorkOrderManager>();
            services.AddScoped<IWorkOrderDal, EfWorkOrderDal>();

            //TagType
            services.AddScoped<ITagTypeService, TagTypeManager>();
            services.AddScoped<ITagTypeDal, EfTagTypeDal>();

            //Tag
            services.AddScoped<ITagService, TagManager>();
            services.AddScoped<ITagDal, EfTagDal>();


            //Stop
            services.AddScoped<IStopService, StopManager>();
            services.AddScoped<IStopDal, EfStopDal>();


            //StopType
            services.AddScoped<IStopTypeService, StopTypeManager>();
            services.AddScoped<IStopTypeDal, EfStopTypeDal>();


            //Product
            services.AddScoped<IProductService, ProductManager>();
            services.AddScoped<IProductDal, EfProductDal>();

            //Quality
            services.AddScoped<IQuality_PropertyService, Quality_PropertyManager>();
            services.AddScoped<IQuality_PropertyDal, EfQuality_PropertyDal>();

            //Pattern
            services.AddScoped<IPatternService, PatternManager>();
            services.AddScoped<IPatternDal, EfPatternDal>();

            //Employee
            services.AddScoped<IEmployeeService, EmployeeManager>();
            services.AddScoped<IEmployeeDal, EfEmployeeDal>();


        }
    }
}
