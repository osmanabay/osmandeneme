﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Abstract
{
    public interface IBoltService
    {
        ResponseData GetList(Expression<Func<Bolt, bool>> expression = null, bool _isDto = false);
        ResponseData Get(Expression<Func<Bolt, bool>> expression);
        Task<ResponseData> Add(Bolt item,int tagId);
        ResponseData Update(Bolt item);
        ResponseData Delete(Bolt item);



    }
}
