﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IDailyTeamService
    {
        ResponseData Get(Expression<Func<DailyTeam, bool>> expression);
        ResponseData Add(DailyTeam item);
        ResponseData Update(DailyTeam item);
        ResponseData Delete(DailyTeam item);

        ResponseData AddDailyTeamEmployee(DailyTeamEmployee item);
        ResponseData DeleteDailyTeamEmployee(DailyTeamEmployee item);


    }
}
