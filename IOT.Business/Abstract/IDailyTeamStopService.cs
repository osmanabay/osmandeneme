﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Abstract
{
    public interface IDailyTeamStopService
    {
        ResponseData GetList(Expression<Func<DailyTeamStop, bool>> expression = null);
        ResponseData Get(Expression<Func<DailyTeamStop, bool>> expression);
        Task<ResponseData> AddAsync(DailyTeamStopDto dto);
        Task<ResponseData> UpdateAsync(DailyTeamStopDto dto);
        ResponseData Delete(DailyTeamStop item);
    }
}
