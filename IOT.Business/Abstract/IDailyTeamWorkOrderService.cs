﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Abstract
{
    public interface IDailyTeamWorkOrderService
    {
        ResponseData Get(Expression<Func<DailyTeamWorkOrder, bool>> expression);
        ResponseData GetList(Expression<Func<DailyTeamWorkOrder, bool>> expression);
        Task<ResponseData> Add(DailyTeamWorkOrder item,DailyTeamWorkOrderDto dto);
        Task<ResponseData> Update(DailyTeamWorkOrder item, DailyTeamWorkOrderDto dto);
        //Task<ResponseData> ShiftUpdate(DailyTeamWorkOrder item, DailyTeamWorkOrderDto dto);
        Task<ResponseData> ShiftEnd(DailyTeamWorkOrder item, ETag eTag);
        Task<ResponseData> ShiftStart(DailyTeamWorkOrder item, ETag eTag);

        ResponseData Delete(DailyTeamWorkOrder item);
    }
}
