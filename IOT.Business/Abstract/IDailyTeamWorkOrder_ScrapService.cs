﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Abstract
{
    public interface IDailyTeamWorkOrder_ScrapService
    {
        ResponseData Get(Expression<Func<DailyTeamWorkOrder_Scrap, bool>> expression);
        ResponseData GetList(Expression<Func<DailyTeamWorkOrder_Scrap, bool>> expression);
        Task<ResponseData> Add(DailyTeamWorkOrder_Scrap item,DailyTeamWorkOrder_ScrapDto dto);
        ResponseData Update(DailyTeamWorkOrder_Scrap item);
        ResponseData Delete(DailyTeamWorkOrder_Scrap item);
    }
}
