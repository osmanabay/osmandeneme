﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IDepartmentService
    {
        ResponseData GetList(Expression<Func<Department, bool>> expression = null,
                                bool _includeFactory=false,
                                bool _includeDeviceGroups=false,
                                bool _isDto=false);
        ResponseData Get(Expression<Func<Department, bool>> expression);
        ResponseData Add(Department item);
        ResponseData Update(Department item);
        ResponseData Delete(Department item);



    }
}
