﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IOT.Business.Abstract
{
    public interface IDeviceGroupService
    {
        ResponseData GetList(Expression<Func<DeviceGroup, bool>> expression = null,
                             bool _includeDevices=false,
                             bool _includeDeviceGroupStops=false,
                             bool _isDto=false);
        ResponseData Get(Expression<Func<DeviceGroup, bool>> expression);
        ResponseData Add(DeviceGroup DeviceGroup);
        ResponseData Update(DeviceGroup DeviceGroup);
        Task<ResponseData> Any(DeviceGroupRequest request);
        ResponseData Delete(DeviceGroup DeviceGroup);



    }
}
