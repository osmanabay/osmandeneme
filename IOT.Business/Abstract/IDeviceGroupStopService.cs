﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Linq.Expressions;

namespace IOT.Business.Abstract
{
    public interface IDeviceGroupStopService
    {
        ResponseData GetList(Expression<Func<DeviceGroupStop, bool>> expression = null,
                             bool _includeDeviceGroup=false,
                             bool _includeStop=false,
                             bool _isDto=false);
        ResponseData Get(Expression<Func<DeviceGroupStop, bool>> expression);
        ResponseData Add(DeviceGroupStop item);
        ResponseData Update(DeviceGroupStop item);
        ResponseData Delete(DeviceGroupStop item);



    }
}
