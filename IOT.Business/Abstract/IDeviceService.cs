﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Abstract
{
    public interface IDeviceService
    {
        ResponseData GetList(Expression<Func<Device, bool>> expression = null,bool _includeTags=false,bool _isDto=false);
        ResponseData Get(Expression<Func<Device, bool>> expression);
        ResponseData Add(Device device);
        Task<ResponseData> Update(Device device, DeviceDto dto=null);
        ResponseData Delete(Device device);
        Task<ResponseData> Reset(string deviceName,int tagId);
    }
}
