﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IEmployeeService
    {
        ResponseData GetList(Expression<Func<Employee, bool>> expression = null,
                                                       bool _includeTeams=false,
                                                       bool _isDto=false);
        ResponseData Get(Expression<Func<Employee, bool>> expression);
        ResponseData Add(Employee item);
        ResponseData Update(Employee item);
        ResponseData Delete(Employee item);



    }
}
