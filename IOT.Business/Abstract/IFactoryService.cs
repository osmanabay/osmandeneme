﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IFactoryService
    {
        ResponseData GetList(Expression<Func<Factory, bool>> expression = null,bool _includeTags=false,bool _isDto=false);
        ResponseData Get(Expression<Func<Factory, bool>> expression);
        ResponseData Add(Factory item);
        ResponseData Update(Factory item);
        ResponseData Delete(Factory item);



    }
}
