﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IPatternService
    {
        ResponseData GetList(Expression<Func<Pattern, bool>> expression = null,bool _includeTags=false,bool _isDto=false);
        ResponseData Get(Expression<Func<Pattern, bool>> expression);
        ResponseData Add(Pattern pattern);
        ResponseData Update(Pattern pattern);
        ResponseData Delete(Pattern pattern);



    }
}
