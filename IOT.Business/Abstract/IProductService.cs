﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IProductService
    {
        ResponseData GetList(Expression<Func<Product, bool>> expression = null,
                                            bool _includePattern=false,
                                            bool _isDto=false);
        ResponseData Get(Expression<Func<Product, bool>> expression);
        ResponseData Add(Product product);
        ResponseData Update(Product product);
        ResponseData Delete(Product product);



    }
}
