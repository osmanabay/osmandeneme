﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IQuality_PropertyService
    {
        ResponseData GetList(Expression<Func<Quality_Property, bool>> expression = null,
                                            bool _includeProduct=false,
                                            bool _includeQualitys=false,
                                            bool _isDto=false);
        ResponseData Get(Expression<Func<Quality_Property, bool>> expression);
        ResponseData Add(Quality_Property Quality_Property);
        ResponseData Update(Quality_Property Quality_Property);
        ResponseData Delete(Quality_Property Quality_Property);



    }
}
