﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IScrapService
    {
        ResponseData GetList(Expression<Func<Scrap, bool>> expression = null,bool _isDto=false);
        ResponseData Get(Expression<Func<Scrap, bool>> expression);
        ResponseData Add(Scrap item);
        ResponseData Update(Scrap item);
        ResponseData Delete(Scrap item);



    }
}
