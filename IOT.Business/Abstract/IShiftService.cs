﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IShiftService
    {
        ResponseData GetList(Expression<Func<Shift, bool>> expression = null, 
                                                    bool _isDto = false);
        ResponseData Get(Expression<Func<Shift, bool>> expression);
        ResponseData Add(Shift item);
        ResponseData Update(Shift item);
        ResponseData Delete(Shift item);



    }
}
