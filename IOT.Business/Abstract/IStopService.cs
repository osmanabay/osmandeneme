﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IStopService
    {
        ResponseData GetList(Expression<Func<Stop, bool>> expression = null, 
                                                    bool _includeStopType=false,
                                                    bool _isDto = false);
        ResponseData Get(Expression<Func<Stop, bool>> expression);
        ResponseData Add(Stop item);
        ResponseData Update(Stop item);
        ResponseData Delete(Stop item);



    }
}
