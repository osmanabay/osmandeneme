﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface IStopTypeService
    {
        ResponseData GetList(Expression<Func<StopType, bool>> expression = null, bool _includeStops = false,
                                                    bool _isDto = false);
        ResponseData Get(Expression<Func<StopType, bool>> expression);
        ResponseData Add(StopType item);
        ResponseData Update(StopType item);
        ResponseData Delete(StopType item);



    }
}
