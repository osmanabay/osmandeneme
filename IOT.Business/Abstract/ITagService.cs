﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Abstract
{
    public interface ITagService
    {
        ResponseData GetList(Expression<Func<Tag, bool>> expression = null, 
                                                    bool _includeTagType=false,
                                                    bool _includeDevice=false,
                                                    bool _isDto = false);
        ResponseData Get(Expression<Func<Tag, bool>> expression);
        Task<ResponseData> Add(Tag item);
        Task<ResponseData> Update(Tag item);
        Task<ResponseData> Delete(Tag item);

        Task<ResponseData> GetMongo(int id);





    }
}
