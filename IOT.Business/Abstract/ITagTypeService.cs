﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface ITagTypeService
    {
        ResponseData GetList(Expression<Func<TagType, bool>> expression = null, 
                                                    bool _isDto = false);
        ResponseData Get(Expression<Func<TagType, bool>> expression);
        ResponseData Add(TagType item);
        ResponseData Update(TagType item);
        ResponseData Delete(TagType item);



    }
}
