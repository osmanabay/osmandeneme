﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Abstract
{
    public interface ITeamService
    {
        ResponseData GetList(Expression<Func<Team, bool>> expression = null,bool includeEmployee=false,bool _isDto=false);
        ResponseData Get(Expression<Func<Team, bool>> expression);
        ResponseData Add(Team Team);
        ResponseData Update(Team Team);
        ResponseData Delete(Team Team);
        ResponseData Any(TeamRequest request);
        ResponseData AddTeamEmployee(TeamEmployee item);
        ResponseData DeleteTeamEmployee(TeamEmployee item);

    }
}
