﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using IOT.Entities.Concrete;

namespace IOT.Business.Abstract
{
    public interface IWarpStockService
    {
        ResponseData GetList(Expression<Func<WarpStock, bool>> expression = null,
                                                        bool _isDto=false);
        ResponseData Get(Expression<Func<WarpStock, bool>> expression,bool _isDto=false);
        Task<ResponseData> Add(WarpStock item, int tagId);
        Task<ResponseData> Update(WarpStock item,int tagId);
        ResponseData Delete(WarpStock item);



    }
}
