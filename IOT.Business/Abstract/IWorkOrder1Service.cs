﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Abstract
{
    public interface IWorkOrder1Service
    {
        ResponseData GetList(Expression<Func<WorkOrder1, bool>> expression = null,
                                                        bool _isDto=false);
        ResponseData Get(Expression<Func<WorkOrder1, bool>> expression,bool _includeProduct=false,bool _isDto=false);
        ResponseData Add(WorkOrder1 item);
        Task<ResponseData> Update(WorkOrder1 item,string deviceName,int tagId);
        ResponseData Delete(WorkOrder1 item);



    }
}
