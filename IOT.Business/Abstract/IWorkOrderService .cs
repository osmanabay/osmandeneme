﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Abstract
{
    public interface IWorkOrderService
    {
        ResponseData GetList(Expression<Func<WorkOrder, bool>> expression = null,
                                                        bool _isDto=false,bool _includeWarpStock=false,bool _includeDevice=false);
        ResponseData Get(Expression<Func<WorkOrder, bool>> expression,bool _includeProduct=false,bool _isDto=false,bool _includeWarpStock=false);
        ResponseData Add(WorkOrder item);
        Task<ResponseData> Update(WorkOrder item,string deviceName,int tagId);
        ResponseData Delete(WorkOrder item);



    }
}
