﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Concrete
{
    public class BoltManager : IBoltService
    {
        private IBoltDal _BoltDal;
        private ITagDal _ItemTagDal;

        public BoltManager(IBoltDal BoltDal, ITagDal ItemTagDal)
        {
            _BoltDal = BoltDal;
            _ItemTagDal = ItemTagDal;
    }

        public async Task<ResponseData> Add(Bolt item,int tagId)
        {

            ResponseData data = new ResponseData();
            try
            {
                _BoltDal.Add(item);
                ResponseData responseGetMongo = await _ItemTagDal.GetMongo(tagId);
                if (responseGetMongo.IsSucceed)
                {
                    ETag mTag = (ETag)responseGetMongo.Data;
                    mTag.TotalBoltMeter = (Convert.ToDouble(mTag.TotalBoltMeter) + Convert.ToDouble(item.Meter)).ToString();
                    mTag.TotalBoltWeftAmount= (Convert.ToDouble(mTag.TotalBoltWeftAmount) + Convert.ToDouble(item.WeftAmount)).ToString();
                    mTag.TotalBoltTargetMeter = (Convert.ToDouble(mTag.TotalBoltTargetMeter) + Convert.ToDouble(item.TargetAmount)).ToString();
                    mTag.LastBoltMeterDate = item.CreatedDate.ToString();
                    mTag.TotalBoltProgressBar = "0";

                    ResponseData responseUpdateMongo = await _ItemTagDal.UpdateForWorkOrder(mTag);
                    if (responseUpdateMongo.IsSucceed)
                    {
                        data.Id = item.Id;
                        data.Data = item;
                        data.IsSucceed = true;
                        return data;
                    }

                }
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }


        public ResponseData Delete(Bolt item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _BoltDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Bolt, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _BoltDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Bolt, bool>> expression, 
                                                    bool _isDto=false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _BoltDal.GetList(expression,_isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
             
        }

        public ResponseData Update(Bolt item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _BoltDal.Update(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }
    }
}
