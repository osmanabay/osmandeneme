﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Concrete
{
    public class DailyTeamManager : IDailyTeamService
    {
        private IDailyTeamDal _itemDal;

        public DailyTeamManager(IDailyTeamDal itemDal)
        {
            _itemDal = itemDal;
        }

        public ResponseData Add(DailyTeam item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _itemDal.Add(item);
                data.Data = item;
                data.Id = item.Id;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData AddDailyTeamEmployee(DailyTeamEmployee item)
        {
            ResponseData data = new ResponseData();
            try
            {

                _itemDal.AddDailyTeamEmployee(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(DailyTeam item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData DeleteDailyTeamEmployee(DailyTeamEmployee item)
        {
            ResponseData data = new ResponseData();
            try
            {

                _itemDal.DeleteDailyTeamEmployee(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null
                             ? ex.Message
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<DailyTeam, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression);
                if (data.Data==null)
                {
                    data.IsSucceed = false;
                    return data;
                }
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }



        public ResponseData Update(DailyTeam item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Update(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
