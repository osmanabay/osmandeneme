﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Concrete
{
    public class DailyTeamStopManager : IDailyTeamStopService
    {
        private IDailyTeamStopDal _itemDal;
        private ITagDal _tagDal;

        public DailyTeamStopManager(IDailyTeamStopDal itemDal, ITagDal tagDal)
        {
            _itemDal = itemDal;
            _tagDal = tagDal;
        }

        public async Task<ResponseData> AddAsync(DailyTeamStopDto dto)
        {

            ResponseData data = new ResponseData();
            try
            {
                DailyTeamStop dailyTeamStop = new DailyTeamStop
                {
                    DailyTeamId= dto.DailyTeamId,
                    StopId=dto.StopId,
                    StartDate=dto.StartDate,
                    EndDate=dto.EndDate,
                    Duration=dto.Duration
                };
                _itemDal.Add(dailyTeamStop);

                //Manuel duruşlarda mongoya yaz 
                if (dto.IsManuel)
                {
                    ResponseData mongoData = await _tagDal.GetMongo(dto.TagId);
                    if (mongoData.IsSucceed)
                    {
                        ETag tag = (ETag)mongoData.Data;
                        tag.StopContinue = true;
                        tag.LastSignalDate = DateTime.Now.ToString();
                        tag.Status = dto.StopId.ToString();
                        tag.DaliyTeamStopId = dailyTeamStop.Id.ToString();
                        tag.TotalStopAmount = (Convert.ToInt32(tag.TotalStopAmount) +1).ToString();
                        ResponseData updateMongoResponse = await _tagDal.UpdateToMongo(tag);
                        if (updateMongoResponse.IsSucceed)
                        {
                            data.Data = dailyTeamStop;
                            data.IsSucceed = true;
                            return data;
                        }
                    }
                }
                else
                {
                    data.Data = dailyTeamStop;
                    data.IsSucceed = true;
                    return data;
                }

                data.IsSucceed = false;
                return data;

            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(DailyTeamStop item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<DailyTeamStop, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<DailyTeamStop, bool>> expression = null)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.GetList(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }


        public async Task<ResponseData> UpdateAsync(DailyTeamStopDto dto)
        {
            ResponseData data = new ResponseData();
            try
            {
                DailyTeamStop dailyTeamStop = new DailyTeamStop
                {
                    Id=dto.Id,
                    DailyTeamId = dto.DailyTeamId,
                    StopId = dto.StopId,
                    StartDate = dto.StartDate,
                    EndDate = dto.EndDate,
                    Duration = dto.Duration
                };
                _itemDal.Update(dailyTeamStop);

                if (dto.IsManuel)
                {
                    ResponseData mongoData = await _tagDal.GetMongo(dto.TagId);
                    if (mongoData.IsSucceed)
                    {
                        ETag tag = (ETag)mongoData.Data;
                        tag.StopContinue = false;
                        if (tag.DailyTeamWorkOrderId=="0")
                        {
                            tag.Status = "0";
                        }
                        else
                        {
                            tag.Status = "1";
                            tag.LastSignalDate = DateTime.Now.ToString();
                        }
                        //tag.LastSignalDate = null;
                        tag.PlannedStopDuration = tag.RealTimeTotalPlannedStop;
                        tag.UnPlannedStopDuration = tag.RealTimeTotalUnPlannedStop;

                        //tag.DaliyTeamStopId = dailyTeamStop.Id.ToString();
                        ResponseData updateMongoResponse = await _tagDal.UpdateToMongo(tag);
                        if (updateMongoResponse.IsSucceed)
                        {
                            data.Data = dailyTeamStop;
                            data.IsSucceed = true;
                            return data;
                        }
                    }
                }
                else
                {
                    data.Data = dailyTeamStop;
                    data.IsSucceed = true;
                    return data;
                }

                data.IsSucceed = false;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
