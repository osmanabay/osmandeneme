﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IOT.Business.Concrete
{
    public class DailyTeamWorkOrderManager : IDailyTeamWorkOrderService
    {
        private IDailyTeamWorkOrderDal _itemDal;
        private ITagDal _itemTagDal;
        private IDailyTeamStopDal _dailyTeamStopDal;
        private IBoltDal _boltDal;
        private IWorkOrderDal _workOrderDal;



        public DailyTeamWorkOrderManager(IDailyTeamWorkOrderDal itemDal,
              ITagDal itemTagDal, IDailyTeamStopDal dailyTeamStopDal, IBoltDal boltDal, IWorkOrderDal workOrderDal)
        {
            _itemDal = itemDal;
            _itemTagDal = itemTagDal;
            _dailyTeamStopDal = dailyTeamStopDal;
            _boltDal = boltDal;
            _workOrderDal = workOrderDal;
        }

        public async Task<ResponseData> Add(DailyTeamWorkOrder item, DailyTeamWorkOrderDto dto)
        {

            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Add(item);

                ResponseData responseGetMongo = await _itemTagDal.GetMongo(dto.TagId);
                if (responseGetMongo.IsSucceed)
                {
                    //_itemDal.Add(item);
                    ETag mTag = (ETag)responseGetMongo.Data;
                    //if (mTag.DailyTeamWorkOrderId == "0")
                    //{
                    //if (mTag.WorkOrderId == dto.WorkOrderId.ToString())
                    //{
                    //    mTag.TotalShiftWeftAmount = "0";
                    //}
                    //else
                    //{
                    //    mTag.Value = "0";
                    //    mTag.TotalBoltMeter = "0";
                    //    mTag.TotalBoltWeftAmount = "0";
                    //    mTag.TotalBoltTargetMeter = "0";
                    //}
                    mTag.WarpUsedMeter = "0";
                    mTag.WarpUsedWeftAmount = "0";

                    mTag.DeviceId = dto.DeviceId.ToString();
                    mTag.WorkOrderId = dto.WorkOrderId.ToString();
                    mTag.WorkOrderNo = dto.WorkOrderNo;
                    mTag.ShiftId = item.ShiftId.ToString();
                    //if (mTag.WorkOrderId != item.WorkOrderId.ToString())
                    //{
                    //    mTag.WeftAmount = "0";
                    //}
                    mTag.WeftAmount = "0";
                    //mTag.TotalShiftWeftAmount = "0";
                    mTag.Value = "0";
                    mTag.TotalBoltMeter = "0";
                    mTag.TotalBoltWeftAmount = "0";
                    mTag.TotalBoltTargetMeter = "0";
                    mTag.WorkOrderShiftWeftAmount = "0";
                    mTag.WorkOrderShiftMeter = "0";


                    mTag.TargetAmount = item.TargetAmount.ToString();
                    mTag.Status = "1";
                    mTag.LastSignalDate = null;
                    mTag.IsStartUp = true;
                    mTag.Sample = dto.Sample;
                    mTag.ProductionContinue = dto.ProductionContinue;
                    mTag.WeftDensity = dto.WeftDensity.ToString();
                    mTag.DailyTeamWorkOrderId = item.Id.ToString();
                    mTag.DailyTeamId = item.DailyTeamId.ToString();
                    mTag.StartDate = DateTime.Now.ToString();
                    mTag.CycleTime = dto.CycleTime;
                    mTag.ShiftEndDate = item.EndDate.Value.ToString();
                    mTag.ShiftStartDate = item.StartDate.Value.ToString();

                    ResponseData responseUpdateMongo = await _itemTagDal.UpdateForWorkOrder(mTag);
                    if (responseUpdateMongo.IsSucceed)
                    {
                        data.Id = item.Id;
                        data.Data = item;
                        data.IsSucceed = true;
                        return data;
                    }
                    //}
                }

                data.Message = "Mongo'ya dailyteamworkorder update edilemedi.";
                data.IsSucceed = false;
                return data;

            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(DailyTeamWorkOrder item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<DailyTeamWorkOrder, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<DailyTeamWorkOrder, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.GetList(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }


        // Update sadece iş emri kapatılacağı zaman Update çalıştırılmaktadır
        public async Task<ResponseData> Update(DailyTeamWorkOrder item, DailyTeamWorkOrderDto dto)
        {
            ResponseData data = new ResponseData();
            try
            {
                ResponseData getResponseMongo = await _itemTagDal.GetMongo(dto.TagId);
                if (getResponseMongo.IsSucceed)
                {
                    // İş emri kapatılacağı zaman topu kes
                    ETag eTag = (ETag)getResponseMongo.Data;
                    Bolt bolt = new Bolt
                    {
                        DailyTeamWorkOrderId = dto.Id,
                        Meter = eTag.ProducedMeterByBolt,
                        WeftAmount = eTag.WeftAmountByBolt,
                        TargetAmount = eTag.BoltTargetMeter

                    };

                    _boltDal.Add(bolt);
                    eTag.WarpUsedMeter = "0";
                    eTag.WarpUsedWeftAmount = "0";
                    eTag.WeftAmountByBolt = "0";
                    eTag.TotalBoltWeftAmount = "0";
                    eTag.ProductionContinue = false;
                    eTag.Sample = false;

                    var newStartupTime = Convert.ToDecimal(eTag.NewStartupTime);
                    var startupTime = Convert.ToDecimal(eTag.StartupTime);

                    item.StartupTime = Math.Round((eTag.NewStartupTime != null || eTag.NewStartupTime != "0") ? newStartupTime : startupTime, 2);
                    item.TotalTimeout = Convert.ToDecimal(eTag.TotalTimeout);

                    item.Amount = Convert.ToDecimal(eTag.WorkOrderShiftMeter);
                    item.WeftAmount = Convert.ToDecimal(eTag.WorkOrderShiftWeftAmount);



                    item.EndDate = DateTime.Now.AddHours(1);
                    //item.TargetAmount = Convert.ToDecimal((item.EndDate - item.StartDate).Value.TotalSeconds / Convert.ToInt32(request.DailyTeamWorkOrder.CycleTime));
                    item.TargetAmount = Convert.ToDecimal(eTag.TargetAmount);

                    // İş emrini kapat
                    _itemDal.Update(item);
                    eTag.DailyTeamWorkOrderId = "0";
                    eTag.Status = "0";
                    eTag.ShiftId = "0";
                  

                    // Vardiyadaki iş emrinde toplanan verilere göre ana iş emrini güncelle
                    WorkOrder workOrder = (WorkOrder)_workOrderDal.Get(x => x.Id == dto.WorkOrderId).Data;
                    
                    var dailyTeamWorkOrders = _itemDal.GetList(x => x.WorkOrderId == dto.WorkOrderId);
                    workOrder.IsOpened = false;
                    workOrder.TargetAmount = dailyTeamWorkOrders[0].TargetAmount;
                    workOrder.Amount = dailyTeamWorkOrders.Sum(x => x.Amount);
                    workOrder.WeftAmount = dailyTeamWorkOrders.Sum(x => x.WeftAmount);
                    workOrder.StartDate = dailyTeamWorkOrders.OrderBy(x => x.StartDate).First().StartDate;
                    workOrder.EndDate = dailyTeamWorkOrders.OrderBy(x => x.EndDate).Last().EndDate;
                    workOrder.ModifiedDate = DateTime.Now;
                    workOrder.Status = 2;

                    _workOrderDal.Update(workOrder);
                    eTag.WorkOrderId = "0";
                    eTag.WeftAmount = "0";


                    //eTag.ShiftTargetMeter = "0";
                    //eTag.TotalShiftMeter = "0";
                    //eTag.TotalShiftWeftAmount = "0";

                    eTag.StartDate = eTag.ShiftEndDate;
                    // Duruş varsa duruşu sonlandır.
                    if (eTag.StopContinue)
                    {
                        DailyTeamStop dailyTeamStop = _dailyTeamStopDal.Get(x => x.Id == Convert.ToInt32(eTag.DaliyTeamStopId));
                        dailyTeamStop.EndDate = DateTime.Now;
                        dailyTeamStop.Duration = Convert.ToInt32((dailyTeamStop.EndDate - dailyTeamStop.StartDate).Value.TotalSeconds);
                        _dailyTeamStopDal.Update(dailyTeamStop);
                    }
                    eTag.RealTimeTotalPlannedStop = "0";
                    eTag.RealTimeTotalUnPlannedStop = "0";
                    
                    // Yapılan işlemleri etkileyen alanları mongo db de güncelle 
                    ResponseData postResponseMongo = await _itemTagDal.UpdateToMongo(eTag);
                    if (postResponseMongo.IsSucceed)
                    {
                        data.IsSucceed = true;
                        data.Data = item;
                        return data;
                    }
                }

                data.Message = "If something went wrong please try again later.";
                data.IsSucceed = false;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                               ex.Message :
                               ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public async Task<ResponseData> ShiftEnd(DailyTeamWorkOrder item, ETag eTag)
        {

            /*
             * Vardiya kapat yapıldıktan sonra burada vardiya başlatta yapılmaktadır.
             * */
            ResponseData data = new ResponseData();
            try
            {
                var newStartupTime = Convert.ToDecimal(eTag.NewStartupTime);
                var startupTime = Convert.ToDecimal(eTag.StartupTime);
                item.StartupTime = Math.Round((eTag.NewStartupTime != null || eTag.NewStartupTime != "0") ? newStartupTime : startupTime, 2);
                item.TotalTimeout = Convert.ToDecimal(eTag.TotalTimeout);

                _itemDal.Update(item);

                // Vardiyalık iş emri kapatıldığında mongodb den sıfırlanacak alanlar
                eTag.Status = "0";
                eTag.ShiftTargetMeter = "0";
                eTag.TotalShiftMeter = "0";
                eTag.TotalShiftWeftAmount = "0";
                eTag.DailyTeamWorkOrderId = "0";
                eTag.RealTimeTotalPlannedStop = "0";
                eTag.RealTimeTotalUnPlannedStop = "0";
                eTag.StartDate = eTag.ShiftEndDate;

                //ResponseData postResponseMongo = await _itemTagDal.UpdateToMongo(eTag);
                //if (postResponseMongo.IsSucceed)
                //{
                //    data.IsSucceed = true;
                //    data.Data = postResponseMongo.Data;
                //    return postResponseMongo;
                //}

                data.IsSucceed = true;
                data.Data = eTag;
                return data;

                //data.Message = "If something went wrong please try again later.";
                //data.IsSucceed = false;
                //return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                               ex.Message :
                               ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public async Task<ResponseData> ShiftStart(DailyTeamWorkOrder item, ETag eTag)
        {
            ResponseData data = new ResponseData();

            try
            {
                _itemDal.Add(item);
                eTag.StartDate = item.StartDate.ToString();
                eTag.DailyTeamId = item.DailyTeamId.ToString();
                eTag.DailyTeamWorkOrderId = item.Id.ToString();
                eTag.Status = "1";
                eTag.ShiftId = item.ShiftId.ToString();
                eTag.WorkOrderShiftMeter = "0";
                eTag.WorkOrderShiftWeftAmount = "0";
                eTag.TotalStopAmount = "0";

                //ResponseData postResponseMongo = await _itemTagDal.UpdateToMongo(eTag);
                //if (postResponseMongo.IsSucceed)
                //{
                //    data.IsSucceed = true;
                //    data.Data = postResponseMongo.Data;
                //    return postResponseMongo;
                //}

                data.IsSucceed = true;
                data.Data = eTag;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                                              ex.Message :
                                              ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        //public async Task<ResponseData> ShiftUpdate(DailyTeamWorkOrder item, DailyTeamWorkOrderDto dto)
        //{
        //    ResponseData data = new ResponseData();
        //    try
        //    {
        //        ResponseData getResponseMongo = await _itemTagDal.GetMongo(dto.TagId);
        //        if (getResponseMongo.IsSucceed)
        //        {
        //            ETag eTag = (ETag)getResponseMongo.Data;
        //            var newStartupTime = Convert.ToDecimal(eTag.NewStartupTime);
        //            var startupTime = Convert.ToDecimal(eTag.StartupTime);
        //            _itemDal.Update(item);
        //            item.StartupTime = Math.Round((eTag.NewStartupTime != null || eTag.NewStartupTime != "0") ? newStartupTime : startupTime, 2);
        //            item.TotalTimeout = Convert.ToDecimal(eTag.TotalTimeout);

        //            eTag.Status = "0";
        //            eTag.ShiftTargetMeter = "0";
        //            eTag.TotalShiftMeter = "0";
        //            eTag.TotalShiftWeftAmount = "0";
        //            eTag.DailyTeamWorkOrderId = "0";
        //            eTag.RealTimeTotalPlannedStop = "0";
        //            eTag.RealTimeTotalUnPlannedStop = "0";
        //            eTag.StartDate = eTag.ShiftEndDate;


        //            ResponseData postResponseMongo = await _itemTagDal.UpdateToMongo(eTag);
        //            if (postResponseMongo.IsSucceed)
        //            {
        //                data.IsSucceed = true;
        //                data.Data = item;
        //                return postResponseMongo;
        //            }
        //        }

        //        data.Message = "If something went wrong please try again later.";
        //        data.IsSucceed = false;
        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        data.Message = ex.InnerException == null ?
        //                       ex.Message :
        //                       ex.InnerException.Message;
        //        data.IsSucceed = false;
        //        return data;
        //    }
        //}

    }
}
