﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IOT.Business.Concrete
{
    public class DailyTeamWorkOrder_ScrapManager : IDailyTeamWorkOrder_ScrapService
    {
        private IDailyTeamWorkOrder_ScrapDal _itemDal;
        private ITagDal _itemTagDal;

        public DailyTeamWorkOrder_ScrapManager(IDailyTeamWorkOrder_ScrapDal itemDal,
              ITagDal itemTagDal)
        {
            _itemDal = itemDal;
            _itemTagDal = itemTagDal;
        }

        public async Task<ResponseData> Add(DailyTeamWorkOrder_Scrap item, DailyTeamWorkOrder_ScrapDto dto)
        {

            ResponseData data = new ResponseData();
            try
            {

                _itemDal.Add(item);

                ResponseData responseGetMongo = await _itemTagDal.GetMongo(dto.TagId);
                if (responseGetMongo.IsSucceed)
                {
                    ETag mTag = (ETag)responseGetMongo.Data;
                    mTag.TotalScrap = (Convert.ToInt32(mTag.TotalScrap) + dto.Amount).ToString();

                    ResponseData responseUpdateMongo = await _itemTagDal.UpdateForWorkOrder(mTag);
                    if (responseUpdateMongo.IsSucceed)
                    {
                        data.Id = item.Id;
                        data.Data = item;
                        data.IsSucceed = true;
                        return data;
                    }
                   
                }

                data.Message = "Mongo'ya DailyTeamWorkOrder_Scrap update edilemedi.";
                data.IsSucceed = false;
                return data;

            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(DailyTeamWorkOrder_Scrap item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<DailyTeamWorkOrder_Scrap, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<DailyTeamWorkOrder_Scrap, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.GetList(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Update(DailyTeamWorkOrder_Scrap item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Update(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }


    }
}
