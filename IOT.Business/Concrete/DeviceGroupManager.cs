﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Concrete
{
    public class DeviceGroupManager : IDeviceGroupService
    {
        private IDeviceGroupDal _deviceGroupDal;
        private IDeviceDal _deviceDal;
        private IDailyTeamDal _dailyTeamDal;

        private ITagDal _tagDal;

        private ITeamDal _teamDal;

        public DeviceGroupManager(IDeviceGroupDal deviceGroupDal, ITeamDal teamDal, IDeviceDal deviceDal, ITagDal tagDal, IDailyTeamDal dailyTeamDal)
        {
            _deviceGroupDal = deviceGroupDal;
            _teamDal = teamDal;
            _deviceDal = deviceDal;
            _tagDal = tagDal;
            _dailyTeamDal = dailyTeamDal;
        }

        public ResponseData Add(DeviceGroup deviceGroup)
        {

            ResponseData data = new ResponseData();
            try
            {

                _deviceGroupDal.Add(deviceGroup);
                data.Data = deviceGroup;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(DeviceGroup deviceGroup)
        {
            ResponseData data = new ResponseData();
            try
            {
                _deviceGroupDal.Delete(deviceGroup);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<DeviceGroup, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _deviceGroupDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<DeviceGroup, bool>> expression,
                                    bool _includeDevices = false,
                                    bool _includeDeviceGroupStops = false,
                                    bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _deviceGroupDal.GetList(expression, _includeDevices, _includeDeviceGroupStops, _isDto).Data;
                //data.ClientIpAddress= this.GetIPAddress();
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }

        }



        public ResponseData Update(DeviceGroup deviceGroup)
        {
            ResponseData data = new ResponseData();
            try
            {
                _deviceGroupDal.Update(deviceGroup);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }


        public string GetIPAddress()
        {
            try
            {
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public async Task<ResponseData> Any(DeviceGroupRequest request)
        {
            ResponseData response = new ResponseData();
            try
            {
                ResponseData teamResponse = _teamDal.Get(x => x.Id == request.Team.LeadEmployeeId);
                Team team = (Team)teamResponse.Data;
                ResponseData deviceResponse = _deviceDal.GetList(x => x.DeviceGroupId == request.DeviceGroup.Id && x.IsOpened != true, true);
                List<Device> devices = (List<Device>)deviceResponse.Data;
                foreach (Device item in devices)
                {
                    int tagId = item.Tags.FirstOrDefault().Id;
                    ResponseData responseMongo = await _tagDal.GetMongo(tagId);
                    ETag eTag = (ETag)responseMongo.Data;
                    if (eTag.DailyTeamId != "0")
                    {
                        DailyTeam dailyTeam = _dailyTeamDal.Get(x => x.Id == Convert.ToInt32(eTag.DailyTeamId));
                        dailyTeam.LeadEmployeeName = team.LeadEmployeeName;
                        dailyTeam.TeamId = team.Id;
                        dailyTeam.LeadEmployeeId = team.LeadEmployeeId;
                        _dailyTeamDal.Update(dailyTeam);
                    }
                }

                response.IsSucceed = true;
                return response;
            }
            catch (Exception ex)
            {
                response.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                response.IsSucceed = false;
                return response;
            }
        }
    }
}
