﻿using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Linq.Expressions;

namespace IOT.Business.Concrete
{
    public class DeviceGroupStopManager : IDeviceGroupStopService
    {
        private IDeviceGroupStopDal _deviceGroupStopDal;

        public DeviceGroupStopManager(IDeviceGroupStopDal deviceGroupStopDal)
        {
            _deviceGroupStopDal = deviceGroupStopDal;
        }

        public ResponseData Add(DeviceGroupStop item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _deviceGroupStopDal.Add(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(DeviceGroupStop item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _deviceGroupStopDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<DeviceGroupStop, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _deviceGroupStopDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<DeviceGroupStop, bool>> expression,
                                    bool _includeDeviceGroup = false,
                                    bool _includeStop = false,
                                    bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _deviceGroupStopDal.GetList(expression, _includeDeviceGroup,_includeStop, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }

        }



        public ResponseData Update(DeviceGroupStop item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _deviceGroupStopDal.Update(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
