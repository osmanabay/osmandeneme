﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Concrete
{
    public class DeviceManager : IDeviceService
    {
        private IDeviceDal _deviceDal;
        private ITagDal _tagDal;


        public DeviceManager(IDeviceDal deviceDal, ITagDal tagDal)
        {
            _deviceDal = deviceDal;
            _tagDal = tagDal;
        }

        public ResponseData Add(Device Device)
        {

            ResponseData data = new ResponseData();
            try
            {

                _deviceDal.Add(Device);
                data.Data = Device;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(Device Device)
        {
            ResponseData data = new ResponseData();
            try
            {
                _deviceDal.Delete(Device);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Device, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _deviceDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Device, bool>> expression, bool _includeTags = false, bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _deviceDal.GetList(expression, _includeTags, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }

        }

        public async Task<ResponseData> Update(Device device, DeviceDto dto = null)
        {
            ResponseData data = new ResponseData();
            try
            {
                _deviceDal.Update(device);
                if (dto != null)
                {
                    var responseMongo = await _tagDal.GetMongo(Convert.ToInt32(dto.TagId));
                    if (responseMongo.IsSucceed && responseMongo.Data != null)
                    {
                        var eTag = (ETag)responseMongo.Data;
                        int index = eTag.Name.IndexOf("_")+1;
                        string subName = eTag.Name.Substring(index, eTag.Name.Length-index);
                        eTag.Name = device.Name +"_"+ subName;
                        await _tagDal.UpdateToMongo(eTag);
                    }


                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public async Task<ResponseData> Reset(string deviceName, int tagId)
        {
            ResponseData data = new ResponseData();

            try
            {
                return await _deviceDal.ResetAsync(deviceName, tagId);

            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }


        }
    }
}
