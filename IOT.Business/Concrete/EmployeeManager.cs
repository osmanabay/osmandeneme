﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Concrete
{
    public class EmployeeManager : IEmployeeService
    {
        private IEmployeeDal _employeeDal;

        public EmployeeManager(IEmployeeDal employeeDal)
        {
            _employeeDal = employeeDal;
        }

        public ResponseData Add(Employee item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _employeeDal.Add(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null
                             ? ex.Message
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(Employee item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _employeeDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Employee, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _employeeDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null
                             ? ex.Message
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Employee, bool>> expression,
                                                    bool _includeFactories = false,
                                                    bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _employeeDal.GetList(expression, _includeFactories, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null
                             ? ex.Message
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }

        }

        public ResponseData Update(Employee item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _employeeDal.Update(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null
                             ? ex.Message
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }
    }
}
