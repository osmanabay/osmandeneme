﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Concrete
{
    public class FactoryManager : IFactoryService
    {
        private IFactoryDal _factoryDal;

        public FactoryManager(IFactoryDal factoryDal)
        {
            _factoryDal = factoryDal;
        }

        public ResponseData Add(Factory item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _factoryDal.Add(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(Factory item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _factoryDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Factory, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _factoryDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Factory, bool>> expression,
                                                    bool _includeFactories = false, 
                                                    bool _isDto=false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _factoryDal.GetList(expression,_includeFactories,_isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
             
        }

        public ResponseData Update(Factory item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _factoryDal.Update(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }
    }
}
