﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Concrete
{
    public class PatternManager : IPatternService
    {
        private IPatternDal _patternDal;

        public PatternManager(IPatternDal patternDal)
        {
            _patternDal = patternDal;
        }

        public ResponseData Add(Pattern pattern)
        {

            ResponseData data = new ResponseData();
            try
            {

                _patternDal.Add(pattern);
                data.Data = pattern;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(Pattern pattern)
        {
            ResponseData data = new ResponseData();
            try
            {
                _patternDal.Delete(pattern);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Pattern, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _patternDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Pattern, bool>> expression,bool _includeFactories = false, bool _isDto=false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _patternDal.GetList(expression,_includeFactories,_isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
             
        }

        public ResponseData Update(Pattern pattern)
        {
            ResponseData data = new ResponseData();
            try
            {
                _patternDal.Update(pattern);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }
    }
}
