﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Concrete
{
    public class ProductManager : IProductService
    {
        private IProductDal _productDal;


        public ProductManager(IProductDal productDal)
        {
            _productDal = productDal;
        }

        public ResponseData Add(Product item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _productDal.Add(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(Product item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _productDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Product, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _productDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Product, bool>> expression,
                                                             bool _includePattern = false,
                                                             bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _productDal.GetList(expression, _includePattern, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }

        }


        public ResponseData Update(Product Product)
        {
            ResponseData data = new ResponseData();
            try
            {
                _productDal.Update(Product);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
