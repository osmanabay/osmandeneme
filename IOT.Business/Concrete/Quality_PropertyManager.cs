﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Concrete
{
    public class Quality_PropertyManager : IQuality_PropertyService
    {
        private readonly IQuality_PropertyDal _Quality_PropertyDal;

        public Quality_PropertyManager(IQuality_PropertyDal Quality_PropertyDal)
        {
            _Quality_PropertyDal = Quality_PropertyDal;
        }

        public ResponseData Add(Quality_Property item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _Quality_PropertyDal.Add(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(Quality_Property item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _Quality_PropertyDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Quality_Property, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _Quality_PropertyDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Quality_Property, bool>> expression,
                                                             bool _includeProduct = false, 
                                                             bool _includeQualitys = false,
                                                             bool _isDto=false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _Quality_PropertyDal.GetList(expression, _includeProduct,_includeQualitys, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
             
        }


        public ResponseData Update(Quality_Property Quality_Property)
        {
            ResponseData data = new ResponseData();
            try
            {
                _Quality_PropertyDal.Update(Quality_Property);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
