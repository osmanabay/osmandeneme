﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Concrete
{
    public class ScrapManager : IScrapService
    {
        private IScrapDal _ScrapDal;

        public ScrapManager(IScrapDal ScrapDal)
        {
            _ScrapDal = ScrapDal;
        }

        public ResponseData Add(Scrap item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _ScrapDal.Add(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(Scrap item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _ScrapDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Scrap, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _ScrapDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Scrap, bool>> expression, 
                                                    bool _isDto=false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _ScrapDal.GetList(expression,_isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
             
        }

        public ResponseData Update(Scrap item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _ScrapDal.Update(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }
    }
}
