﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Concrete
{
    public class StopManager : IStopService
    {
        private IStopDal _itemDal;

        public StopManager(IStopDal itemDal)
        {
            _itemDal = itemDal;
        }

        public ResponseData Add(Stop item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _itemDal.Add(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                               ex.Message : 
                               ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(Stop item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Stop, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Stop, bool>> expression, 
                                            bool _includeStopType=false,
                                            bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.GetList(expression,_includeStopType, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }

        }

        public ResponseData Update(Stop item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Update(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
