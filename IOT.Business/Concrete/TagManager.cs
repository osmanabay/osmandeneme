﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Concrete
{
    public class TagManager : ITagService
    {
        private ITagDal _itemDal;

        public TagManager(ITagDal itemDal)
        {
            _itemDal = itemDal;
        }

        public async Task<ResponseData> Add(Tag item)
        {

            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Add(item);
                ResponseData mongoResponse = await _itemDal.AddToMongo(item);
                if (mongoResponse.IsSucceed)
                {
                    data.Data = item;
                    data.IsSucceed = true;
                    return data;
                }
                else
                {
                    _itemDal.Delete(item);
                    data.Data = null;
                    data.IsSucceed = false;
                    return data;
                }

            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                               ex.Message :
                               ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public async Task<ResponseData> Delete(Tag item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(item);
                await _itemDal.DeleteToMongo(item.Id);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                               ex.Message :
                               ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Tag, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Tag, bool>> expression,
                                            bool _includeTagType = false,
                                            bool _includeDevice = false,
                                            bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.GetList(expression, _includeTagType, _includeDevice, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }

        }

        public async Task<ResponseData> GetMongo(int id)
        {
            ResponseData data = new ResponseData();
            try
            {
                data =await _itemDal.GetMongo(id);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public async Task<ResponseData> Update(Tag item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Update(item);
                ResponseData responseMongo = await _itemDal.GetMongo(item.Id);
                if (responseMongo.IsSucceed)
                {
                    ETag eTag = (ETag)responseMongo.Data;
                    eTag.Name = item.Name;
                    eTag.TypeId = item.TypeId.ToString();
                    eTag.DeviceId = item.DeviceId.ToString();
                    eTag.SignalFromWhere = item.SignalFromWhere.ToString();
                    eTag.StartupTime = item.StartupTime.ToString();
                    eTag.TimeOut = item.TimeOut.ToString();
                    data.IsSucceed = true;
                    return data;
                }
                data.IsSucceed = false;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                               ex.Message :
                               ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }


    }
}
