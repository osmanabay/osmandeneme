﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Business.Concrete
{
    public class TeamManager : ITeamService
    {
        private ITeamDal _itemDal;
        private ITagDal _tagDal;

        public TeamManager(ITeamDal itemDal, ITagDal tagDal)
        {
            _itemDal = itemDal;
            _tagDal = tagDal;
        }

        public ResponseData Add(Team Team)
        {

            ResponseData data = new ResponseData();
            try
            {

                _itemDal.Add(Team);
                data.Data = Team;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData AddTeamEmployee(TeamEmployee item)
        {
            ResponseData data = new ResponseData();
            try
            {

                _itemDal.AddTeamEmployee(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Any(TeamRequest request)
        {
            ResponseData data = new ResponseData();
            try
            {
                data = _itemDal.Any(request);
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(Team Team)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(Team);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData DeleteTeamEmployee(TeamEmployee item)
        {
            ResponseData data = new ResponseData();
            try
            {

                _itemDal.DeleteTeamEmployee(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null
                             ? ex.Message
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Team, bool>> expression)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression);
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<Team, bool>> expression, bool _includeEmployee=false, bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.GetList(expression,_includeEmployee,_isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }

        }

        public ResponseData Update(Team Team)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Update(Team);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
