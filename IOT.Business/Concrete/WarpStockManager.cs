﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using IOT.Entities.Concrete;

namespace IOT.Business.Concrete
{
    public class WarpStockManager : IWarpStockService
    {
        private readonly IWarpStockDal _itemDal;
        private readonly ITagDal _itemTagDal;

        public WarpStockManager(IWarpStockDal itemDal, ITagDal itemTagDal)
        {
            _itemDal = itemDal;
            _itemTagDal = itemTagDal;
        }

        public async Task<ResponseData> Add(WarpStock item,int tagId)
        {

            ResponseData data = new ResponseData();
            try
            {

                data.Data = _itemDal.Get(x => x.WarpStockNo == item.WarpStockNo).Data;

                var warpStock = (WarpStock)data.Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                ResponseData responseGetMongo = await _itemTagDal.GetMongo(tagId);
                if (responseGetMongo.IsSucceed)
                {
                    ETag mTag = (ETag)responseGetMongo.Data;

                    mTag.WarpMeter = warpStock.WarpStockMeter.ToString(CultureInfo.InvariantCulture);
                    mTag.LeftWarpMeter = warpStock.LeftMeter.ToString(CultureInfo.InvariantCulture);
                    mTag.DisplayLeftWarpMeter= warpStock.LeftMeter.ToString(CultureInfo.InvariantCulture);
                    mTag.WarpNo = warpStock.WarpStockNo;
                    mTag.WarpWorkOrderId = warpStock.WorkOrderId.ToString();

                    var responseUpdateMongo = await _itemTagDal.UpdateForWorkOrder(mTag);

                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                               ex.Message : 
                               ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(WarpStock item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<WarpStock, bool>> expression, bool isDto=false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression, isDto).Data;

                var warpStock =(WarpStock)data.Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<WarpStock, bool>> expression, bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.GetList(expression, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }

        }

        public async Task<ResponseData> Update(WarpStock item,int tagId)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Update(item);

                ResponseData responseGetMongo = await _itemTagDal.GetMongo(tagId);
                if (responseGetMongo.IsSucceed)
                {
                    ETag mTag = (ETag)responseGetMongo.Data;

                    if (item.Status==1)
                    {
                        mTag.DisplayLeftWarpMeter = item.LeftMeter.ToString();

                        mTag.WarpMeter = item.WarpStockMeter.ToString();
                        mTag.LeftWarpMeter = item.LeftMeter.ToString();
                        mTag.WarpNo = item.WarpStockNo.ToString();
                        mTag.WarpWorkOrderId = item.WorkOrderId.ToString();
                    }
                    else
                    {
                        mTag.DisplayLeftWarpMeter = "0";

                        mTag.WarpMeter = "0";
                        mTag.LeftWarpMeter = "0";
                        mTag.WarpNo = "0";
                        mTag.WarpWorkOrderId = "0";
                        mTag.WarpUsedWeftAmount = "0";
                        mTag.WarpUsedMeter = "0";

                    }

                    ResponseData responseUpdateMongo = await _itemTagDal.UpdateForWorkOrder(mTag);

                }

                data.Data = item;
                data.IsSucceed = true;
                return data;


            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
