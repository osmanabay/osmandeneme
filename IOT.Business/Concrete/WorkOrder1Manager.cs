﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Concrete
{
    public class WorkOrder1Manager : IWorkOrder1Service
    {
        private IWorkOrder1Dal _itemDal;
        private IDeviceService _deviceService;

        public WorkOrder1Manager(IWorkOrder1Dal itemDal,IDeviceService deviceService)
        {
            _itemDal = itemDal;
            _deviceService = deviceService;
        }

        public ResponseData Add(WorkOrder1 item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _itemDal.Add(item);
                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                               ex.Message : 
                               ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(WorkOrder1 item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<WorkOrder1, bool>> expression,bool _includeProduct=false, bool _isDto=false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression, _includeProduct, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<WorkOrder1, bool>> expression, bool _isDto = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.GetList(expression, _isDto).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }

        }

        public async Task<ResponseData> Update(WorkOrder1 item,string deviceName,int tagId)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Update(item);
                data.IsSucceed = true;
                if (deviceName=="" && tagId==0)
                {
                    
                }
                else
                {
                    ResponseData responseResetDevice = await _deviceService.Reset(deviceName, tagId);
                    data.Data = responseResetDevice.Data;
                }
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
