﻿
using IOT.Business.Abstract;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Business.Concrete
{
    public class WorkOrderManager : IWorkOrderService
    {
        private IWorkOrderDal _itemDal;
        private IWarpStockDal _warpStockDal;


        public WorkOrderManager(IWorkOrderDal itemDal, IWarpStockDal warpStockDal)
        {
            _itemDal = itemDal;
            _warpStockDal = warpStockDal;
        }

        public ResponseData Add(WorkOrder item)
        {

            ResponseData data = new ResponseData();
            try
            {

                _itemDal.Add(item);
                if (item.WarpStocks!=null)
                {
                    foreach (var warpStock in item.WarpStocks)
                    {
                        warpStock.WorkOrderId = item.Id;
                        warpStock.Id = 0;
                        warpStock.Status = 0;
                        warpStock.LeftMeter = warpStock.WarpStockMeter;
                        _warpStockDal.Add(warpStock);
                    }
                }
                



                data.Data = item;
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null ?
                               ex.Message : 
                               ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Delete(WorkOrder item)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Delete(item);
                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData Get(Expression<Func<WorkOrder, bool>> expression,bool includeProduct=false, bool isDto=false, bool includeWarpStock = false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.Get(expression, includeProduct, isDto, includeWarpStock).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }
        }

        public ResponseData GetList(Expression<Func<WorkOrder, bool>> expression, bool isDto = false, bool includeWarpStock=false,bool includeDevice=false)
        {
            ResponseData data = new ResponseData();
            try
            {
                data.Data = _itemDal.GetList(expression, isDto, includeWarpStock,includeDevice).Data;
                if (data.Data == null)
                {
                    data.IsSucceed = false;
                    return data;
                }

                data.IsSucceed = true;
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;

                data.IsSucceed = false;
                return data;
            }

        }

        public async Task<ResponseData> Update(WorkOrder item,string deviceName,int tagId)
        {
            ResponseData data = new ResponseData();
            try
            {
                _itemDal.Update(item);
                data.IsSucceed = true;
                if (deviceName=="" && tagId==0)
                {
                    
                }
                return data;
            }
            catch (Exception ex)
            {
                data.Message = ex.InnerException == null 
                             ? ex.Message 
                             : ex.InnerException.Message;
                data.IsSucceed = false;
                return data;
            }
        }
    }
}
