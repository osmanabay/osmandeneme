﻿using IOT.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace IOT.Core.DataAccess.EntityFramework
{
    public class EfEntityRepositoryBase<TEntity, TContext> : IEntityRepository<TEntity>
        where TEntity : class,IEntity, new()
        where TContext : DbContext, new()
    {
        public void Add(TEntity entity)
        {
            using (var context = new TContext())
            {
                var addedEntity = context.Entry(entity);
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
            }
        }

        public void Delete(TEntity entity)
        {
            using (var context = new TContext())
            {

                entity.IsDeleted = true;
                entity.ModifiedDate = DateTime.Now;
                var DeletedEntity = context.Entry(entity);
                DeletedEntity.State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public TEntity Get(Expression<Func<TEntity, bool>> expression)
        {
            using (var context = new TContext())
            {
                var getvalue = context.Set<TEntity>().SingleOrDefault(expression);
                return getvalue;
            }
        }

        public List<TEntity> GetList(Expression<Func<TEntity, bool>> expression = null)
        {
            using (var context = new TContext())
            {
                return expression == null
                    ? context.Set<TEntity>().Where(x => x.IsDeleted != true).ToList()
                    : context.Set<TEntity>().Where(x => x.IsDeleted != true)
                                            .Where(expression)
                                            .ToList();
            }
        }

        public void Update(TEntity entity)
        {
            using (var context = new TContext())
            {
                entity.ModifiedDate = DateTime.Now;
                var updatedEntity = context.Entry(entity);
                updatedEntity.State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
