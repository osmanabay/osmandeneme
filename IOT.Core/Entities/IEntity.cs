﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Core.Entities
{
    public interface IEntity:IBaseEntity
    {
        DateTime? CreatedDate { get; set; }
        DateTime? ModifiedDate { get; set; }
        bool? IsDeleted { get; set; }

    }
}
