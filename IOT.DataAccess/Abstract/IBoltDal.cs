﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IBoltDal:IEntityRepository<Bolt>
    {
        ResponseData GetList(Expression<Func<Bolt, bool>> expression = null,bool isDto=false);

    }

   
}
