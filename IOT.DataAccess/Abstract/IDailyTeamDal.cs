﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;

namespace IOT.DataAccess.Abstract
{
    public interface IDailyTeamDal : IEntityRepository<DailyTeam>
    {
        ResponseData AddDailyTeamEmployee(DailyTeamEmployee dailyTeamEmployee);
        ResponseData DeleteDailyTeamEmployee(DailyTeamEmployee dailyTeamEmployee);
    }
}
