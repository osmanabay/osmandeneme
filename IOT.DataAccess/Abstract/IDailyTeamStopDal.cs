﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IDailyTeamStopDal : IEntityRepository<DailyTeamStop>
    {

    }
}
