﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;

namespace IOT.DataAccess.Abstract
{
    public interface IDailyTeamWorkOrder_ScrapDal : IEntityRepository<DailyTeamWorkOrder_Scrap>
    {
    }
}
