﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;

namespace IOT.DataAccess.Abstract
{
    public interface IDailyTeamWorkOrderDal : IEntityRepository<DailyTeamWorkOrder>
    {
    }
}
