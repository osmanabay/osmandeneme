﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IDepartmentDal:IEntityRepository<Department>
    {
        ResponseData GetList(Expression<Func<Department, bool>> expression = null, 
                                bool _includeFactory=false, 
                                bool _includeDeviceGroups=false,
                                bool isDto=false);

    }

   
}
