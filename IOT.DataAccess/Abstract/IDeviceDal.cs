﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IOT.DataAccess.Abstract
{
    public interface IDeviceDal:IEntityRepository<Device>
    {
        ResponseData GetList(Expression<Func<Device, bool>> expression = null, 
                             bool _includeTag=false, 
                             bool _includeDeviceGroup=false,
                             bool isDto=false);
        Task<ResponseData> ResetAsync(string deviceName,int tagId);
    }

   
}
