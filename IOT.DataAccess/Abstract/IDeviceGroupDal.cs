﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IDeviceGroupDal : IEntityRepository<DeviceGroup>
    {
        ResponseData GetList(Expression<Func<DeviceGroup, bool>> expression = null, 
                             bool _includeDevice = false, 
                             bool _includeDeviceGroupStops = false,
                             bool isDto = false);

    }


}
