﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IDeviceGroupStopDal : IEntityRepository<DeviceGroupStop>
    {
        ResponseData GetList(Expression<Func<DeviceGroupStop, bool>> expression = null,
                                    bool _includeDeviceGroup = false,
                                    bool _includeStop = false,
                                    bool isDto = false);

    }


}
