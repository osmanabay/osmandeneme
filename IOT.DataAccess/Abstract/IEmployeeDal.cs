﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IEmployeeDal:IEntityRepository<Employee>
    {
        ResponseData GetList(Expression<Func<Employee, bool>> expression = null,
                             bool includeTeams=false,
                             bool isDto=false);
    }

   
}
