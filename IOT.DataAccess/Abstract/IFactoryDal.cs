﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IFactoryDal:IEntityRepository<Factory>
    {
        ResponseData GetList(Expression<Func<Factory, bool>> expression = null, bool _includeFactories=false, bool isDto=false);

    }

   
}
