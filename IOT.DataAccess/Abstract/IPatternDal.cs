﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IPatternDal:IEntityRepository<Pattern>
    {
        ResponseData GetList(Expression<Func<Pattern, bool>> expression = null,
                             bool includeProduct=false,
                             bool isDto=false);
    }

   
}
