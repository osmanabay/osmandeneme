﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IProductDal:IEntityRepository<Product>
    {
        ResponseData GetList(Expression<Func<Product, bool>> expression = null, 
                                                bool _includePattern=false, 
                                                bool isDto=false);
    }
}
