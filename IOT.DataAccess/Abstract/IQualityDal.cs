﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IQualityDal:IEntityRepository<Quality>
    {
        ResponseData GetList(Expression<Func<Quality, bool>> expression = null, 
                                                bool _includeProduct=false, 
                                                bool _includeProperty=false,
                                                bool isDto=false);
    }
}
