﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IQuality_PropertyDal:IEntityRepository<Quality_Property>
    {
        ResponseData GetList(Expression<Func<Quality_Property, bool>> expression = null, 
                                                bool _includeProduct = false, 
                                                bool _includeQualitys = false,
                                                bool isDto=false);
    }
}
