﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IScrapDal:IEntityRepository<Scrap>
    {
        ResponseData GetList(Expression<Func<Scrap, bool>> expression = null,bool isDto=false);

    }

   
}
