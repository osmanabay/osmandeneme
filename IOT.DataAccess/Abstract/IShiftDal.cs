﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IShiftDal:IEntityRepository<Shift>
    {
        ResponseData GetList(Expression<Func<Shift, bool>> expression = null,
                             bool isDto=false);
    }

   
}
