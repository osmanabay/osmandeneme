﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IStopDal : IEntityRepository<Stop>
    {
        ResponseData GetList(Expression<Func<Stop, bool>> expression = null,
                             bool _includeStopType = false,
                             bool _isDto = false);
    }


}
