﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IStopTypeDal:IEntityRepository<StopType>
    {
        ResponseData GetList(Expression<Func<StopType, bool>> expression = null, bool _includeStops = false,
                             bool isDto=false);
    }

   
}
