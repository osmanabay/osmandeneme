﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IOT.DataAccess.Abstract
{
    public interface ITagDal : IEntityRepository<Tag>
    {
        ResponseData GetList(Expression<Func<Tag, bool>> expression = null,
                             bool includeTagType = false,
                             bool isDevice = false,
                             bool isDto = false
            );
        Task<ResponseData> GetMongo(int id);
        Task<ResponseData> AddToMongo(Tag item);
        Task<ResponseData> DeleteToMongo(int id);
        Task<ResponseData> UpdateToMongo(ETag item);
        Task<ResponseData> UpdateForWorkOrder(ETag item);
    }


}
