﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface ITeamDal:IEntityRepository<Team>
    {
        ResponseData GetList(Expression<Func<Team, bool>> expression = null,bool includeEmployee=false, bool isDto = false);
        ResponseData Any(IRequest request);
        ResponseData AddTeamEmployee(TeamEmployee TeamEmployee);
        ResponseData Get(Expression<Func<Team, bool>> expression = null);

        ResponseData DeleteTeamEmployee(TeamEmployee TeamEmployee);

    }

   
}
