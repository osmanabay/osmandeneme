﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using IOT.Entities.Concrete;

namespace IOT.DataAccess.Abstract
{
    public interface IWarpStockDal:IEntityRepository<WarpStock>
    {
        ResponseData GetList(Expression<Func<WarpStock, bool>> expression = null, bool isDto = false);

        ResponseData Get(Expression<Func<WarpStock, bool>> expression = null, bool isDto = false);

    }

   
}
