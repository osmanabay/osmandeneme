﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IWorkOrder1Dal:IEntityRepository<WorkOrder1>
    {
        ResponseData GetList(Expression<Func<WorkOrder1, bool>> expression = null, bool isDto = false);

        ResponseData Get(Expression<Func<WorkOrder1, bool>> expression = null, bool includeProduct=false, bool isDto = false);

    }

   
}
