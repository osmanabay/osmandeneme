﻿using IOT.Core.DataAccess;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IOT.DataAccess.Abstract
{
    public interface IWorkOrderDal:IEntityRepository<WorkOrder>
    {
        ResponseData GetList(Expression<Func<WorkOrder, bool>> expression = null, bool isDto = false, bool includeWarpStock = false,bool includeDevice=false);

        ResponseData Get(Expression<Func<WorkOrder, bool>> expression = null, bool includeProduct=false, bool isDto = false,bool includeWarpStock=false);

    }

   
}
