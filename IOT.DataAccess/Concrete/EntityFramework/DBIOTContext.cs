﻿using IOT.Entities;
using IOT.Entities.Concrete;
using Microsoft.EntityFrameworkCore;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public partial class DBIOTContext : DbContext
    {
        public static string ConnectionString { get; set; }
        public static string SignalUrl { get; set; }
        public DBIOTContext()
        {
        }

        public DBIOTContext(DbContextOptions<DBIOTContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DailyTeam> DailyTeam { get; set; }
        public virtual DbSet<DailyTeamEmployee> DailyTeamEmployee { get; set; }
        public virtual DbSet<Device> Device { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<Team> Team { get; set; }
        public virtual DbSet<TeamEmployee> TeamEmployee { get; set; }
        public virtual DbSet<Factory> Factory { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<DailyTeamStop> DailyTeamStop { get; set; }
        public virtual DbSet<DailyTeamWorkOrder> DailyTeamWorkOrder { get; set; }
        public virtual DbSet<DeviceGroup> DeviceGroup { get; set; }
        public virtual DbSet<DeviceGroupStop> DeviceGroupStop { get; set; }
        public virtual DbSet<Pattern> Pattern { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Shift> Shift { get; set; }
        public virtual DbSet<Stop> Stop { get; set; }
        public virtual DbSet<StopType> StopType { get; set; }
        public virtual DbSet<TagType> TagType { get; set; }
        public virtual DbSet<WorkOrder> WorkOrder { get; set; }
        public virtual DbSet<DailyTeamWorkOrder_Scrap> DailyTeamWorkOrder_Scrap { get; set; }
        public virtual DbSet<Scrap> Scrap { get; set; }
        public virtual DbSet<Bolt> Bolt { get; set; }

        public virtual DbSet<WarpStock> WarpStock { get; set; }


        public virtual DbSet<Quality> Quality { get; set; }
        public virtual DbSet<Quality_Property> Quality_Property { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<DailyTeamEmployee>(entity =>
            {
                entity.HasKey(e => new { e.DailyTeamId, e.EmployeeId });
            });

            modelBuilder.Entity<TeamEmployee>(entity =>
            {
                entity.HasKey(e => new { e.EmployeeId, e.TeamId});
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}