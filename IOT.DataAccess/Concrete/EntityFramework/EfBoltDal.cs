﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfBoltDal : EfEntityRepositoryBase<Bolt, DBIOTContext>, IBoltDal
    {
        public ResponseData GetList(Expression<Func<Bolt, bool>> expression = null, bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Bolt>().Where(x => x.IsDeleted != true)
                   : context.Set<Bolt>().Where(x => x.IsDeleted != true)
                                          .Where(expression);





                if (isDto)
                {
                    var BoltDto = items.Select(x => new BoltDto { Id = x.Id, Meter = x.Meter,DailyTeamWorkOrderId=x.DailyTeamWorkOrderId,WeftAmount=x.WeftAmount }).ToList();
                    data.Data = BoltDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
