﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.Core.Entities;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfDailyTeamDal : EfBaseEntityRepositoryBase<DailyTeam, DBIOTContext>, IDailyTeamDal
    {
        ResponseData IDailyTeamDal.AddDailyTeamEmployee(DailyTeamEmployee item)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var addedEntity = context.Entry(item);
                addedEntity.State = EntityState.Added;
                if (context.SaveChanges()>0)
                {
                    data.IsSucceed = true;
                }
                else
                {
                    data.IsSucceed = false;
                }
                return data;

            }
        }

        ResponseData IDailyTeamDal.DeleteDailyTeamEmployee(DailyTeamEmployee item)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var addedEntity = context.Remove(item);
                addedEntity.State = EntityState.Deleted;
                if (context.SaveChanges() > 0)
                {
                    data.IsSucceed = true;
                }
                else
                {
                    data.IsSucceed = false;
                }
                return data;

            }
        }
    }
}
