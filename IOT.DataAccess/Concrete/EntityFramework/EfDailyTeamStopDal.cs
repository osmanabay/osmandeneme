﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.Core.Entities;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfDailyTeamStopDal : EfBaseEntityRepositoryBase<DailyTeamStop, DBIOTContext>, IDailyTeamStopDal
    {
       
    }
}
