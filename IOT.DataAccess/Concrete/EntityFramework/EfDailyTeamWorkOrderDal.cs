﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfDailyTeamWorkOrderDal : EfBaseEntityRepositoryBase<DailyTeamWorkOrder, DBIOTContext>, IDailyTeamWorkOrderDal
    {
        
    }
}
