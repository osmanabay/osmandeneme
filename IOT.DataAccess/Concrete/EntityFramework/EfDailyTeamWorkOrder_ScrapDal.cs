﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfDailyTeamWorkOrder_ScrapDal : EfBaseEntityRepositoryBase<DailyTeamWorkOrder_Scrap, DBIOTContext>, IDailyTeamWorkOrder_ScrapDal
    {
        
    }
}
