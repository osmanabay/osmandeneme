﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfDepartmentDal : EfEntityRepositoryBase<Department, DBIOTContext>, IDepartmentDal
    {
        public ResponseData GetList(Expression<Func<Department, bool>> expression = null,
                                        bool _includeFactory = false,
                                        bool _includeDeviceGroups = false,
                                        bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Department>().Where(x => x.IsDeleted != true)
                   : context.Set<Department>().Where(x => x.IsDeleted != true)
                                          .Where(expression);


                if (_includeDeviceGroups)
                {
                    items = items.Include(x => x.DeviceGroups);
                }

                if (_includeFactory)
                {
                    items = items.Include(x => x.Factory);
                }


                if (isDto)
                {
                    var itemsDto = items.Select(x => new DepartmentDto
                    {
                        Id = x.Id,
                        FactoryId=x.FactoryId,
                        Name = x.Name,
                        Factory = _includeFactory ? x.Factory : null,
                        DeviceGroups=_includeDeviceGroups?x.DeviceGroups:null
                    }).ToList();
                    data.Data = itemsDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
