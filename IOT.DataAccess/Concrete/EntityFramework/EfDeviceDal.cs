﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfDeviceDal : EfEntityRepositoryBase<Device, DBIOTContext>, IDeviceDal
    {
        public ResponseData GetList(Expression<Func<Device, bool>> expression = null, bool _includeTag = false,
                                    bool _includeDeviceGroup = false,
                                    bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var devices = expression == null
                   ? context.Set<Device>().Where(x => x.IsDeleted != true)
                   : context.Set<Device>().Where(x => x.IsDeleted != true)
                                          .Where(expression);

                if (_includeDeviceGroup)
                {
                    devices = devices.Include(x => x.DeviceGroup);
                }

                if (_includeTag)
                {
                    devices = devices.Include(x => x.Tags).Where(z => z.Tags.Any(b => b.IsDeleted != true)).Select(
                        x => new Device
                        {
                            Id = x.Id,
                            Name = x.Name,
                            DeviceNo=x.DeviceNo,
                            DeviceGroupId = x.DeviceGroupId,
                            Tags = x.Tags.Where(b => b.IsDeleted != true).ToList()
                        });
                }

                if (isDto)
                {
                    var deviceDto = devices.Select(x => new DeviceDto { Id = x.Id, Name = x.Name,DeviceNo=x.DeviceNo, Tags = _includeTag ? x.Tags : null, DeviceGroup = _includeDeviceGroup ? x.DeviceGroup : null }).ToList();
                    data.Data = deviceDto;
                    return data;
                }

                //data.Data = devices.Where(x=> x.Tags.Any(a=> a.IsDeleted!=true)).ToList();
                data.Data = devices.Where(x => x.IsDeleted != true).ToList();
                return data;
            }
        }

        public async Task<ResponseData> ResetAsync(string DeviceName, int tagId)
        {
            ResponseData data = new ResponseData();
            using (var client = new HttpClient())
            {
                var httpResponse = await client.PostAsync(DBIOTContext.SignalUrl + "api/signal/reset/" + DeviceName + "/" + tagId, null);
                var jsonString = await httpResponse.Content.ReadAsStringAsync();
                data.Data = JsonConvert.DeserializeObject<bool>(jsonString);
                data.IsSucceed = httpResponse.IsSuccessStatusCode;
                data.Message = httpResponse.RequestMessage.ToString();
            }
            return data;
        }
    }
}
