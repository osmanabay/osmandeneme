﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfDeviceGroupDal : EfEntityRepositoryBase<DeviceGroup, DBIOTContext>, IDeviceGroupDal
    {
        public ResponseData GetList(Expression<Func<DeviceGroup, bool>> expression = null,                     bool _includeDevice = false,
                                    bool _includeDeviceGroupStops = false,
                                    bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var DeviceGroups = expression == null
                   ? context.Set<DeviceGroup>().Where(x => x.IsDeleted != true)
                   : context.Set<DeviceGroup>().Where(x => x.IsDeleted != true)
                                          .Where(expression);


                if (_includeDevice)
                {
                    DeviceGroups = DeviceGroups.Include(x => x.Devices);
                }

                if (_includeDeviceGroupStops)
                {
                    DeviceGroups = DeviceGroups.Include(x => x.DeviceGroupStops);
                }



                if (isDto)
                {
                    var DeviceGroupDto = DeviceGroups.Select(x => new DeviceGroupDto { Id = x.Id, Name = x.Name, Devices = _includeDevice ? x.Devices : null,DeviceGroupStops=_includeDeviceGroupStops?x.DeviceGroupStops:null }).ToList();
                    data.Data = DeviceGroupDto;
                    return data;
                }

                data.Data = DeviceGroups.ToList();
                return data;
            }
        }

    }
}
