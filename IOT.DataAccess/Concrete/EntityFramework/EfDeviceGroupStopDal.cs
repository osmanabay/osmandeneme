﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfDeviceGroupStopDal : EfBaseEntityRepositoryBase<DeviceGroupStop, DBIOTContext>, IDeviceGroupStopDal
    {
        public ResponseData GetList(Expression<Func<DeviceGroupStop, bool>> expression = null,
                                    bool _includeDeviceGroup = false,
                                    bool _includeStop = false,
                                    bool _isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<DeviceGroupStop>()
                   : context.Set<DeviceGroupStop>()
                            .Where(expression);

                if (_includeDeviceGroup)
                {
                    items = items.Include(x => x.DeviceGroup);
                }

                if (_includeStop)
                {
                    items = items.Include(x => x.Stop);
                }

                if (_isDto)
                {
                    var itemsDto = items.Select(x => new DeviceGroupStopDto
                    {
                        Id = x.Id,
                        DeviceGroupName = x.DeviceGroup.Name,
                        StopName = x.Stop.Name,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        DeviceGroup=_includeDeviceGroup?x.DeviceGroup:null,
                        Stop = _includeStop?x.Stop:null
                    })
                                       .ToList();
                    data.Data = itemsDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
