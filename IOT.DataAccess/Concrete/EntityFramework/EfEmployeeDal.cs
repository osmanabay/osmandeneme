﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfEmployeeDal : EfEntityRepositoryBase<Employee, DBIOTContext>, IEmployeeDal
    {
        public ResponseData GetList(Expression<Func<Employee, bool>> expression = null,
                                    bool includeTeams = false,
                                    bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Employee>().Where(x => x.IsDeleted != true)
                   : context.Set<Employee>().Where(x => x.IsDeleted != true)
                                          .Where(expression);

                if (includeTeams)
                {
                    items = items.Include(x => x.Teams);
                }

                if (isDto)
                {
                    var itemDto = items.Select(x => new EmployeeDto
                    {
                        Id = x.Id,
                        FullName = x.FirstName + " " + x.LastName,
                        NickName = x.NickName,
                        Password = x.Password,
                        Teams = includeTeams ? x.Teams : null
                    }).ToList();
                    data.Data = itemDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
