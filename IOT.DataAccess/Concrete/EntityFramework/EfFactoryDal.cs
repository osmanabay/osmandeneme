﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfFactoryDal : EfEntityRepositoryBase<Factory, DBIOTContext>, IFactoryDal
    {
        public ResponseData GetList(Expression<Func<Factory, bool>> expression = null, bool _includeDepartment = false, bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Factory>().Where(x => x.IsDeleted != true)
                   : context.Set<Factory>().Where(x => x.IsDeleted != true)
                                          .Where(expression);


                if (_includeDepartment)
                {
                    items = items.Include(x => x.Departments);
                }

                

                if (isDto)
                {
                    var FactoryDto = items.Select(x => new FactoryDto { Id = x.Id, Name = x.Name, Departments = _includeDepartment ? x.Departments : null }).ToList();
                    data.Data = FactoryDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
