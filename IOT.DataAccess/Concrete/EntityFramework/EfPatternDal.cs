﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfPatternDal : EfEntityRepositoryBase<Pattern, DBIOTContext>, IPatternDal
    {
        public ResponseData GetList(Expression<Func<Pattern, bool>> expression = null, bool _includeProduct = false, bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var Patterns = expression == null
                   ? context.Set<Pattern>().Where(x => x.IsDeleted != true)
                   : context.Set<Pattern>().Where(x => x.IsDeleted != true)
                                          .Where(expression);


                if (_includeProduct)
                {
                    Patterns = Patterns.Include(x => x.Products);
                }

                

                if (isDto)
                {
                    var PatternDto = Patterns.Select(x => new PatternDto { Id = x.Id, Name = x.Name, Products = _includeProduct 
                                                ? x.Products 
                                                : null })
                                             .ToList();
                    data.Data = PatternDto;
                    return data;
                }

                data.Data = Patterns.ToList();
                return data;
            }
        }

    }
}
