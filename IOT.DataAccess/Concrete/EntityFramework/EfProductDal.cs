﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfProductDal : EfEntityRepositoryBase<Product, DBIOTContext>, IProductDal
    {
        public ResponseData GetList(Expression<Func<Product, bool>> expression = null, 
            bool _includePattern = false, 
            bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Product>().Where(x => x.IsDeleted != true)
                   : context.Set<Product>().Where(x => x.IsDeleted != true)
                                          .Where(expression);


                if (_includePattern)
                {
                    items = items.Include(x => x.Pattern);
                }

                if (isDto)
                {
                    var itemDto = items.Select(x => new ProductDto { Id = x.Id, Name = x.Name, PatternName=x.Pattern.Name, Pattern = _includePattern 
                                                ? x.Pattern 
                                                : null })
                                           .ToList();

                    data.Data = itemDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
