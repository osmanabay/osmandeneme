﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfQualityDal : EfBaseEntityRepositoryBase<Quality, DBIOTContext>, IQualityDal
    {
        public ResponseData GetList(Expression<Func<Quality, bool>> expression = null,
            bool _includeProduct = false,
            bool _includeProperty = false,
            bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Quality>()
                   : context.Set<Quality>()
                                          .Where(expression);


                if (_includeProduct)
                {
                    items = items.Include(x => x.Product);
                }

                if (_includeProperty)
                {
                    items = items.Include(x => x.Quality_Property);
                }

                if (isDto)
                {
                    var itemDto = items.Select(x => new QualityDto
                    {
                        Id = x.Id,
                        ProductName = x.Product.Name,
                        PropertyName = x.Quality_Property.Name,
                        Product = _includeProduct
                                                ? x.Product
                                                : null,
                        Quality_Property = _includeProperty
                                                ? x.Quality_Property
                                                : null,
                        Value=x.Value,
                        CreatedDate=x.CreatedDate
                    })
                                           .ToList();

                    data.Data = itemDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
