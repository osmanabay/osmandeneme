﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfQuality_PropertyDal : EfEntityRepositoryBase<Quality_Property, DBIOTContext>, IQuality_PropertyDal
    {
        public ResponseData GetList(Expression<Func<Quality_Property, bool>> expression = null,
            bool _includeProduct = false,
            bool _includeQualitys = false,
            bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Quality_Property>().Where(x => x.IsDeleted != true)
                   : context.Set<Quality_Property>().Where(x => x.IsDeleted != true)
                                          .Where(expression);


                if (_includeProduct)
                {
                    items = items.Include(x => x.Product);
                }

                if (_includeQualitys)
                {
                    items = items.Include(x => x.Qualitys);
                }

                if (isDto)
                {
                    var itemDto = items.Select(x => new Quality_PropertyDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Value = x.Value,
                        ProductName = x.Product.Name,
                        Qualitys = _includeQualitys
                                                ? x.Qualitys
                                                : null,
                        Product = _includeProduct
                                                ? x.Product
                                                : null,
                    })
                                           .ToList();

                    data.Data = itemDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
