﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfScrapDal : EfEntityRepositoryBase<Scrap, DBIOTContext>, IScrapDal
    {
        public ResponseData GetList(Expression<Func<Scrap, bool>> expression = null, bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Scrap>().Where(x => x.IsDeleted != true)
                   : context.Set<Scrap>().Where(x => x.IsDeleted != true)
                                          .Where(expression);





                if (isDto)
                {
                    var ScrapDto = items.Select(x => new ScrapDto { Id = x.Id, Name = x.Name }).ToList();
                    data.Data = ScrapDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
