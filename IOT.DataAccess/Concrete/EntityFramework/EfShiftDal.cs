﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfShiftDal : EfEntityRepositoryBase<Shift, DBIOTContext>, IShiftDal
    {
        public ResponseData GetList(Expression<Func<Shift, bool>> expression = null,
                                    bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Shift>().Where(x => x.IsDeleted != true)
                   : context.Set<Shift>().Where(x => x.IsDeleted != true)
                                          .Where(expression);

                if (isDto)
                {
                    var itemDto = items.Select(x => new ShiftDto { Id = x.Id, Name = x.Name, EndDate = x.EndDate, StartDate = x.StartDate }).ToList();
                    data.Data = itemDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
