﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfStopDal : EfEntityRepositoryBase<Stop, DBIOTContext>, IStopDal
    {
        public ResponseData GetList(Expression<Func<Stop, bool>> expression = null,
                                    bool _includeStopType = false,
                                    bool _isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Stop>().Where(x => x.IsDeleted != true)
                   : context.Set<Stop>().Where(x => x.IsDeleted != true)
                                          .Where(expression);



              

                if (_includeStopType)
                {
                    items = items.Include(x => x.StopType);
                }

                if (_isDto)
                {
                    var itemDto = items.Select(x => new StopDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Duration=x.Duration,
                        StopTypeId = x.StopTypeId
                    }).ToList();
                    data.Data = itemDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
