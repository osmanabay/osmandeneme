﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfStopTypeDal : EfEntityRepositoryBase<StopType, DBIOTContext>, IStopTypeDal
    {
        public ResponseData GetList(Expression<Func<StopType, bool>> expression = null, bool _includeStops = false, bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<StopType>().Where(x => x.IsDeleted != true)
                   : context.Set<StopType>().Where(x => x.IsDeleted != true)
                                          .Where(expression);
                if (_includeStops)
                {
                    items = items.Include(x => x.Stops).Where(a => a.Stops.Any(b => b.IsDeleted != true));
                }

                if (isDto)
                {
                    var itemDto=context.Set<StopType>().Where(x => x.IsDeleted != true)
                                       .Select(x => new StopTypeDto { Id = x.Id, Name = x.Name, Stops = x.Stops.Where(y => y.IsDeleted != true).ToList() }).ToList();
                    //var itemDto = items.Select(x => new StopTypeDto
                    //{
                    //    Id = x.Id,
                    //    Name = x.Name
                    //}).ToList();
                    data.Data = itemDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
