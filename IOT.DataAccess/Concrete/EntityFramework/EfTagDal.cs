﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfTagDal : EfEntityRepositoryBase<Tag, DBIOTContext>, ITagDal
    {


        public async Task<ResponseData> AddToMongo(Tag item)
        {
            ResponseData data = new ResponseData();

            using (var context = new DBIOTContext())
            {
                Device d = context.Device.Include(x=>x.DeviceGroup).SingleOrDefault(x => x.Id == item.DeviceId);
                if (d != null)
                {
                    item.Device = d;
                    var device = new ETag
                    {
                        Id = item.Id.ToString(),
                        Name = item.Device.Name + "_" + item.Name,
                        TypeId = item.TypeId.ToString(),
                        Value = item.Value.ToString(),
                        CycleTime = item.CycleTime.ToString(),
                        SignalFromWhere = item.SignalFromWhere.ToString(),
                        DeviceId = item.DeviceId.ToString(),
                        StartupTime = item.StartupTime.ToString(),
                        TotalScrap = "0",
                        TimeOut = item.TimeOut.ToString(),
                        TotalTimeout = "0",
                        Status="0",
                        DeviceGroupId=d.DeviceGroupId.ToString(),
                        DeviceGroupName=d.DeviceGroup.Name

                    };
                    string jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(device);
                    using (var client = new HttpClient())
                    {

                        var httpResponse = await client.PostAsync(DBIOTContext.SignalUrl + "api/signal/addDevice", new StringContent(jsonBody,
                                        Encoding.UTF8,
                                        "application/json"));
                        data.IsSucceed = httpResponse.IsSuccessStatusCode;
                        data.Message = httpResponse.RequestMessage.ToString();
                    }

                }
                else
                {
                    data.IsSucceed = false;
                    data.Message = "";
                }

            }

            return data;
        }

        public ResponseData GetList(Expression<Func<Tag, bool>> expression = null,
                                    bool includeTagType = false,
                                    bool includeDevice = false,
                                    bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Tag>().Where(x => x.IsDeleted != true)
                   : context.Set<Tag>().Where(x => x.IsDeleted != true)
                                          .Where(expression);

                if (includeDevice)
                {
                    items = items.Include(x => x.Device);
                }

                if (includeTagType)
                {
                    items = items.Include(x => x.Type);
                }

                if (isDto)
                {
                    var itemDto = items.Select(x => new TagDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        DeviceId=x.DeviceId,
                        DeviceName = x.Device==null?"":x.Device.Name,
                        SignalFromWhere = (int)x.SignalFromWhere,
                        TypeName = x.Type==null?"":x.Type.Name,
                        TypeId=x.Type.Id,
                        StartupTime=x.StartupTime,
                        TimeOut=x.TimeOut
                    }).ToList();
                    data.Data = itemDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

        public async Task<ResponseData> DeleteToMongo(int id)
        {
            ResponseData data = new ResponseData();

            string jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(id);
            using (var client = new HttpClient())
            {
                var httpResponse = await client.PostAsync(DBIOTContext.SignalUrl + "api/signal/deleteDevice", new StringContent(jsonBody,
                                Encoding.UTF8,
                                "application/json"));
                data.IsSucceed = httpResponse.IsSuccessStatusCode;
                data.Message = httpResponse.RequestMessage.ToString();
            }

            return data;
        }

        public async Task<ResponseData> UpdateToMongo(ETag item)
        {
            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                int deviceId = Convert.ToInt32(item.DeviceId);
                Device d = context.Device.SingleOrDefault(x => x.Id == deviceId);

                if (d != null)
                {
                    item.Name = item.Name.Contains(d.Name) ? item.Name : d.Name + "_" + item.Name;
                    string jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(item);
                    using (var client = new HttpClient())
                    {
                        var httpResponse = await client.PostAsync(DBIOTContext.SignalUrl + "api/signal/updateDevice", new StringContent(jsonBody,
                                        Encoding.UTF8,
                                        "application/json"));
                        data.IsSucceed = httpResponse.IsSuccessStatusCode;
                        data.Message = httpResponse.RequestMessage.ToString();
                        return data;
                    }

                }
                data.IsSucceed = false;
                data.Message = "";
            }
            return data;
        }

        public async Task<ResponseData> GetMongo(int id)
        {
            ResponseData data = new ResponseData();

            using (var client = new HttpClient())
            {
                var httpResponse = await client.GetAsync(DBIOTContext.SignalUrl + "api/signal/getDevice/" + id.ToString());
                var jsonString = await httpResponse.Content.ReadAsStringAsync();
                data.Data = JsonConvert.DeserializeObject<ETag>(jsonString);
                data.IsSucceed = httpResponse.IsSuccessStatusCode;
                data.Message = httpResponse.RequestMessage.ToString();
            }
            return data;
        }

        public async Task<ResponseData> UpdateForWorkOrder(ETag item)
        {
            ResponseData data = new ResponseData();

            string jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(item);
            using (var client = new HttpClient())
            {
                var httpResponse = await client.PostAsync(DBIOTContext.SignalUrl + "api/signal/updateDevice", new StringContent(jsonBody,
                                Encoding.UTF8,
                                "application/json"));
                data.IsSucceed = httpResponse.IsSuccessStatusCode;
                data.Message = httpResponse.RequestMessage.ToString();
            }

            return data;
        }


    }
}
