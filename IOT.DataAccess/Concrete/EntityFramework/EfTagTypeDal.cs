﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfTagTypeDal : EfEntityRepositoryBase<TagType, DBIOTContext>, ITagTypeDal
    {
        public ResponseData GetList(Expression<Func<TagType, bool>> expression = null,
                                    bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<TagType>().Where(x => x.IsDeleted != true)
                   : context.Set<TagType>().Where(x => x.IsDeleted != true)
                                          .Where(expression);

                if (isDto)
                {
                    var itemDto = items.Select(x => new TagTypeDto
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToList();
                    data.Data = itemDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

    }
}
