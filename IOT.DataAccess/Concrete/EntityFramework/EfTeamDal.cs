﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfTeamDal : EfEntityRepositoryBase<Team, DBIOTContext>, ITeamDal
    {
        public ResponseData AddTeamEmployee(TeamEmployee item)
        {
            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var addedEntity = context.Entry(item);
                addedEntity.State = EntityState.Added;
                if (context.SaveChanges() > 0)
                {
                    data.IsSucceed = true;
                }
                else
                {
                    data.IsSucceed = false;
                }
                return data;

            }
        }

        public ResponseData Any(IRequest request)
        {
            TeamRequest trequest = (TeamRequest)request;
            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {

                var employee = context.Set<Employee>().SingleOrDefault(x => x.NickName == trequest.Username && x.Password == trequest.Password);
                if (employee == null)
                {
                    data.IsSucceed = false;
                    data.Message = "Kullanıcı bulunamadı";
                    return data;

                }

                data.IsSucceed = context.Set<Team>().Any(x => x.LeadEmployeeId == employee.Id);

                if (!data.IsSucceed)
                {
                    data.IsSucceed = false;
                    data.Message = "Kullanıcı bulunamadı";
                    return data;
                }

                var team = context.Set<Team>().SingleOrDefault(x => x.LeadEmployeeId == employee.Id);



                var teamDto = new TeamDto
                {
                    Id = team.Id,
                    Name = team.Name,
                    LeadEmployeeName = team.LeadEmployeeName,
                    LeadEmployeeId = team.LeadEmployeeId
                };

                //var employeesByTeamDto = context.Set<Employee>().Where(x => x.TeamEmployees.Any(a=> a.TeamId==team.Id)&&x.Id!=team.LeadEmployeeId).Select(a=> new EmployeeDto {Id=a.Id,FullName=a.FirstName+" "+a.LastName}).ToList();


                //var employeesOtherDto = context.Set<Employee>().Where(x=> !employeesByTeamDto.Any(b=> b.Id==x.Id) && x.Id != team.LeadEmployeeId).Select(a => new EmployeeDto { Id = a.Id, FullName = a.FirstName + " " + a.LastName }).ToList();

                //var devices = context.Set<Device>().Where(x => x.IsOpened != true && x.IsDeleted!=true).Select(x => new DeviceDto { Id = x.Id, Name = x.Name,Tags=x.Tags.Where(y => y.IsDeleted != true).ToList() }).ToList();

                //var stopTypes = context.Set<StopType>().Where(x => x.IsDeleted != true)
                //                       .Select(x => new StopTypeDto { Id = x.Id, Name = x.Name, Stops= x.Stops.Where(y=> y.IsDeleted!=true).ToList() }).ToList();

                TeamViewModel teamViewModel = new TeamViewModel
                {
                    TeamDto=teamDto
                };
                data.Data = teamViewModel;
                return data;
            }
        }

        public ResponseData DeleteTeamEmployee(TeamEmployee item)
        {
            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var addedEntity = context.Remove(item);
                addedEntity.State = EntityState.Deleted;
                if (context.SaveChanges() > 0)
                {
                    data.IsSucceed = true;
                }
                else
                {
                    data.IsSucceed = false;
                }
                return data;

            }
        }

        public ResponseData GetList(Expression<Func<Team, bool>> expression = null,bool includeEmployee=false, bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Team>().Where(x => x.IsDeleted != true)
                   : context.Set<Team>().Where(x => x.IsDeleted != true)
                                          .Where(expression);

                if (includeEmployee)
                {
                    items = items.Include(x => x.TeamEmployees);
                }


                if (isDto)
                {
                    var itemsDto = items.Select(x => new TeamDto { Id = x.Id, Name = x.Name, LeadEmployeeName = x.LeadEmployeeName }).ToList();
                    data.Data = itemsDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

        public ResponseData Get(Expression<Func<Team, bool>> expression = null)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<Team>().Where(x => x.IsDeleted != true)
                   : context.Set<Team>().Where(x => x.IsDeleted != true)
                                          .Where(expression);
                data.Data = items.SingleOrDefault();
                return data;
            }
        }
    }
}
