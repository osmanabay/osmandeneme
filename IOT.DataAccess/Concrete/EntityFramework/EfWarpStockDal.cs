﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IOT.Entities.Concrete;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfWarpStockDal : EfBaseEntityRepositoryBase<WarpStock, DBIOTContext>, IWarpStockDal
    {

        public ResponseData GetList(Expression<Func<WarpStock, bool>> expression = null, 
                                                               bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<WarpStock>().Where(x => x.Status != 3)
                   : context.Set<WarpStock>().Where(x => x.Status != 3)
                                          .Where(expression);


                data.Data = items.ToList();
                return data;
            }
        }

        public ResponseData Get(Expression<Func<WarpStock, bool>> expression = null, bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var warpStock = expression == null
                   ? context.Set<WarpStock>().Where(x => x.Status != 3)
                   : context.Set<WarpStock>().Where(x => x.Status != 3)
                                          .Where(expression);

                data.Data = warpStock.FirstOrDefault();
                return data;
            }
        }



    }
}
