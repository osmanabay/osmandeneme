﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfWorkOrder1Dal : EfEntityRepositoryBase<WorkOrder1, DBIOTContext>, IWorkOrder1Dal
    {

        public ResponseData GetList(Expression<Func<WorkOrder1, bool>> expression = null, 
                                                               bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<WorkOrder1>().Where(x => x.IsDeleted != true)
                   : context.Set<WorkOrder1>().Where(x => x.IsDeleted != true)
                                          .Where(expression);



                if (isDto)
                {
                    var itemsDto = items.Select(x => new WorkOrder1Dto { Id = x.Id,Amount=x.Amount,IsOpened=x.IsOpened,No=x.No,ProductId=x.ProductId,ProductName=x.Product.Name,TargetAmount=x.TargetAmount,TargetMeter=x.TargetAmount.ToString().Replace(".",","),WarpNo=x.WarpNo}).ToList();
                    data.Data = itemsDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

        public ResponseData Get(Expression<Func<WorkOrder1, bool>> expression = null, bool _includeProduct = false, bool isDto = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var workOrder = expression == null
                   ? context.Set<WorkOrder1>().Where(x => x.IsDeleted != true)
                   : context.Set<WorkOrder1>().Where(x => x.IsDeleted != true)
                                          .Where(expression);

                if (_includeProduct)
                {
                    workOrder = workOrder.Include(x => x.Product);
                }

                if (isDto)
                {
                    var workOrderDto = workOrder.Select(x => new WorkOrder1Dto { Id = x.Id,  No= x.No, ProductName = _includeProduct ? x.Product.Name : null,TargetAmount=x.TargetAmount}).ToList();
                    data.Data = workOrderDto;
                    return data;
                }

                data.Data = workOrder.SingleOrDefault();
                return data;
            }
        }



    }
}
