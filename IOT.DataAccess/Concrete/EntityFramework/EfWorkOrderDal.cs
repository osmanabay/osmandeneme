﻿using IOT.Core.DataAccess.EntityFramework;
using IOT.DataAccess.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Entities.ComplexTypes.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IOT.Entities.ComplexTypes.DTO;
using IOT.Entities.Concrete;

namespace IOT.DataAccess.Concrete.EntityFramework
{
    public class EfWorkOrderDal : EfEntityRepositoryBase<WorkOrder, DBIOTContext>, IWorkOrderDal
    {

        public ResponseData GetList(Expression<Func<WorkOrder, bool>> expression = null, 
                                                               bool isDto = false,bool includeWarpStock = false, bool includeDevice = false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var items = expression == null
                   ? context.Set<WorkOrder>().Where(x => x.IsDeleted != true)
                   : context.Set<WorkOrder>().Where(x => x.IsDeleted != true)
                                          .Where(expression);

                if (includeWarpStock)
                {
                    items = items.Include(x=>x.WarpStocks);
                }

                if (includeDevice)
                {
                    items = items.Include(x => x.Device);
                }

                if (isDto)
                {
                    var itemsDto = items.Select(x => new WorkOrderDto { Id = x.Id,Amount=x.Amount,IsOpened=x.IsOpened,No=x.No,ProductId=x.ProductId,ProductName=x.ProductName,TargetAmount=x.TargetAmount,WarpNo=x.WarpNo,WarpStocks=x.WarpStocks,ProductionContinue =x.ProductionContinue,Sample = x.Sample,StartDate = x.StartDate, EndDate = x.EndDate,Device =x.Device}).ToList();
                    data.Data = itemsDto;
                    return data;
                }

                data.Data = items.ToList();
                return data;
            }
        }

        public ResponseData Get(Expression<Func<WorkOrder, bool>> expression = null, bool includeProduct = false, bool isDto = false,bool includeWarpStock=false)
        {

            ResponseData data = new ResponseData();
            using (var context = new DBIOTContext())
            {
                var workOrder = expression == null
                   ? context.Set<WorkOrder>().Where(x => x.IsDeleted != true)
                   : context.Set<WorkOrder>().Where(x => x.IsDeleted != true)
                                          .Where(expression);


                if (includeWarpStock)
                {
                    workOrder = workOrder.Include(x=>x.WarpStocks);
                }
               
                if (isDto)
                {
                    var workOrderDto = workOrder.Select(x => new WorkOrderDto { Id = x.Id,  No= x.No, ProductName = x.ProductName ,TargetAmount=x.TargetAmount,WarpStocks=x.WarpStocks}).ToList();
                    data.Data = workOrderDto;
                    return data;
                }

                data.Data = workOrder.SingleOrDefault();
                return data;
            }
        }



    }
}
