﻿using IOT.Business.Abstract;
using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.ERPApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkOrderController : ControllerBase
    {
        readonly IWorkOrderService _itemService;
        readonly IDeviceService _deviceService;

        public WorkOrderController(IWorkOrderService itemService, IDeviceService deviceService)
        {
            _itemService = itemService;
            _deviceService = deviceService;
        }

        //WorkOrders
        //[HttpPost("/api/WorkOrders")]
        //public IActionResult GetList([FromBody] WorkOrderRequest request)
        //{
        //    if (request.IsOpened==false)
        //    {
        //        return Ok(_itemService.GetList(null, request.IsDto));
        //    }
        //    else
        //    {
        //        return Ok(_itemService.GetList(x => x.IsOpened == request.IsOpened, request.IsDto));

        //    }
        //}

        //WorkOrder/
        [HttpPost]
        public async Task<IActionResult> WorkOrderCrud([FromBody]  WorkOrderRequestModel request)
        {

            if (request.RequestType == Enums.RequestTypes.Read && request.WorkOrderNo != null)
            {
                var response = _itemService.Get(x => x.No == request.WorkOrderNo,false,false,true);
                WorkOrder wo = (WorkOrder)response.Data;
                var response1 = _deviceService.Get(x => x.Id == wo.DeviceId);
                Device device = (Device)response1.Data;

                WorkOrderRequestModel workOrder = new WorkOrderRequestModel
                {
                    ProductId = wo.ProductId,
                    DeviceId = device.DeviceNo,
                    EndDate=Convert.ToDateTime(wo.EndDate),
                    ProductName=wo.ProductName,
                    StartDate= Convert.ToDateTime(wo.StartDate),
                    TargetMeter=(decimal)wo.TargetAmount,
                    Meter = (decimal)wo.Amount,
                    WorkOrderNo=wo.No,
                    Status=wo.Status,
                    WarpStocks= wo.WarpStocks,
                    WeftDensity = wo.WeftDensity
                };

                return Ok(workOrder);
            }
            else if (request.RequestType == Enums.RequestTypes.Create)
            {


                var response =_deviceService.Get(x => x.DeviceNo == request.DeviceId);
                Device device = (Device)response.Data;
                if (device==null)
                {
                    response.IsSucceed = false;
                    return Ok(response);
                }
                int deviceId = device.Id;
                WorkOrder item = new WorkOrder
                {
                    DeviceId = deviceId,
                    ProductId = request.ProductId,
                    ProductName = request.ProductName,
                    No = request.WorkOrderNo,
                    IsOpened = true,
                    Status = 0,
                    TargetAmount = request.TargetMeter,
                    Amount = 0,
                    //WarpNo=request.WarpNo,
                    WeftDensity = request.WeftDensity,
                    Sample = false,
                    ProductionContinue = false,
                    IsDeleted = false,
                    StartDate = request.StartDate,
                    EndDate = request.EndDate,
                    CreatedDate = DateTime.Now,
                    WarpStocks = request.WarpStocks
                };

                return Ok(_itemService.Add(item));
            }
            else
            {
                ResponseData response = new ResponseData
                {
                    IsSucceed = false,
                    Message = "Talebiniz gerçekleştiremedik. Lütfen sistem yöneticiniz ile irtibata geçiniz.",

                };
                return Ok(response);
            }
        }



    }
}