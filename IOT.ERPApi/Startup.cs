﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IOT.Business.Abstract;
using IOT.Business.Concrete;
using IOT.DataAccess.Abstract;
using IOT.DataAccess.Concrete.EntityFramework;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace IOT.ERPApi
{
    public class Startup
    {
        private IConfigurationRoot _appSettings;
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _appSettings = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json")
          .Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            DBIOTContext.ConnectionString = _appSettings.GetConnectionString("EfContext");
            ScopedItems(services);
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder => {
                builder
                .SetIsOriginAllowed(isOriginAllowed: _ => true)

                .AllowAnyMethod()
                .AllowAnyHeader()
                //.AllowAnyOrigin()
                .AllowCredentials();
                //.WithOrigins("http://localhost:4200");
            }));

            services.AddMvc();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                             .AddJsonOptions(opt => opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("CorsPolicy");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

        }

        public void ScopedItems(IServiceCollection services)
        {
            services.AddScoped<DBIOTContext>();

            //WorkOrder1
            services.AddScoped<IWorkOrder1Service, WorkOrder1Manager>();
            services.AddScoped<IWorkOrder1Dal, EfWorkOrder1Dal>();

            //WorkOrder
            services.AddScoped<IWorkOrderService, WorkOrderManager>();
            services.AddScoped<IWorkOrderDal, EfWorkOrderDal>();

            //Device
            services.AddScoped<IDeviceService, DeviceManager>();
            services.AddScoped<IDeviceDal, EfDeviceDal>();

            //Tag
            services.AddScoped<ITagService, TagManager>();
            services.AddScoped<ITagDal, EfTagDal>();

            //WarpStock
            services.AddScoped<IWarpStockService, WarpStockManager>();
            services.AddScoped<IWarpStockDal, EfWarpStockDal>();
        }
    }
}
