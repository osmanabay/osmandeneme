﻿using IOT.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class BaseDto:IDto
    {
        public int Id { get; set; }
    }
}
