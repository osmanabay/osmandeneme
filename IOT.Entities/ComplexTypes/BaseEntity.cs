﻿using IOT.Core.Entities;
using System;

namespace IOT.Entities
{
    public class BaseEntity : IEntity
    {
        public int Id { get; set; }

        public BaseEntity()
        {
            CreatedDate = DateTime.Now;
        }
        public int? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; } = false;
    }
}
