﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class BoltDto:BaseDto
    {
        public int DailyTeamWorkOrderId { get; set; }

        public string Meter { get; set; }
        public string WeftAmount { get; set; }
        public string TargetAmount { get; set; }



        public DailyTeamWorkOrder DailyTeamWorkOrder { get; set; }
    }
}
