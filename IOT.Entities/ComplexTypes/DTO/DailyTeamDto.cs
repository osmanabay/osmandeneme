﻿

using System.Collections.Generic;

namespace IOT.Entities.ComplexTypes
{
    public partial class DailyTeamDto:BaseDto
    {
        public string LeadEmployeeName { get; set; }
        public string TeamName { get; set; }
        public string DeviceName { get; set; }

        public int? TeamId { get; set; }
        public int? DeviceId { get; set; }
        public int? LeadEmployeeId { get; set; }
    }
}