﻿using System;

namespace IOT.Entities.ComplexTypes
{
    public partial class DailyTeamStopDto:BaseDto
    {

        public string StopName { get; set; }

        public int DailyTeamId { get; set; }
        public int StopId { get; set; }
        public int? Duration { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DailyTeam DailyTeam { get; set; }
        public Stop Stop { get; set; }

        public int TagId { get; set; }

        public bool IsManuel { get; set; } = false;

    }
}