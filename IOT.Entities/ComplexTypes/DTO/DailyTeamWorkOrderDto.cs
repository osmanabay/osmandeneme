﻿

using System;
using System.Collections.Generic;

namespace IOT.Entities.ComplexTypes
{
    public partial class DailyTeamWorkOrderDto:BaseDto
    {
        public string TeamName { get; set; }
        public string ShiftName { get; set; }
        public string WorkOrderNo { get; set; }

        public decimal WeftDensity { get; set; }
        public decimal? WeftAmount { get; set; }
        public bool Sample { get; set; }
        public bool ProductionContinue { get; set; }


        public int DailyTeamId { get; set; }
        public int WorkOrderId { get; set; }
        public string CycleTime { get; set; }
        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public int TagId { get; set; }
        public int ShiftId { get; set; }
        public int? Status { get; set; }
        public decimal? TargetAmount { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreatedDate { get; set; }

        public decimal StartupTime { get; set; }
        public decimal TotalTimeout { get; set; }
        public WorkOrder1 WorkOrder { get; set; }
        public Shift Shift { get; set; }
        public DailyTeam DailyTeam { get; set; }
    }
}