﻿

using System;
using System.Collections.Generic;

namespace IOT.Entities.ComplexTypes
{
    public partial class DailyTeamWorkOrder_ScrapDto:BaseDto
    {
        public int DailyTeamWorkOrderId { get; set; }
        public int ScrapId { get; set; }
        public int TagId { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? CreatedDate { get; set; }

        public DailyTeamWorkOrder DailyTeamWorkOrder { get; set; }
        public Scrap Scrap { get; set; }
    }
}