﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class DepartmentDto:BaseDto
    {
        public string Name { get; set; }
        public int FactoryId { get; set; }

        public ICollection<DeviceGroup> DeviceGroups { get; set; }
        public Factory Factory { get; set; }
    }
}
