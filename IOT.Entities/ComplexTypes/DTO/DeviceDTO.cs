﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class DeviceDto:BaseDto
    {
        public string Name { get; set; }
        public string DeviceNo { get; set; }
        public bool IsOpened { get; set; }
        public int? DeviceGroupId { get; set; }
        public string TagId { get; set; }

        public ICollection<Tag> Tags { get; set; }
        public ICollection<DailyTeam> DailyTeams { get; set; }
        public DeviceGroup DeviceGroup { get; set; }
    }
}
