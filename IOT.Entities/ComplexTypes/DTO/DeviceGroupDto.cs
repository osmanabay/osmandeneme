﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class DeviceGroupDto:BaseDto
    {
        public string Name { get; set; }
        public string IPAddress { get; set; }

        public int DepartmentId { get; set; }

        public ICollection<Device> Devices { get; set; }
        public ICollection<DeviceGroupStop> DeviceGroupStops { get; set; }
        public Department Department { get; set; }
    }
}
