﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class DeviceGroupStopDto:BaseDto
    {
        public int? DeviceGroupId { get; set; }
        public int? StopId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public string DeviceGroupName { get; set; }
        public string StopName { get; set; }

        public DeviceGroup DeviceGroup { get; set; }
        public Stop Stop { get; set; }
    }
}
