﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class EmployeeDto : BaseDto
    {
        public EmployeeDto()
        {
            FullName = FirstName + " " + LastName;
        }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Team> Teams { get; set; }
    }
}
