﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class FactoryDto:BaseDto
    {
        public string Name { get; set; }
        public ICollection<Department> Departments { get; set; }
    }
}
