﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class PatternDto:BaseDto
    {
        public string Name { get; set; }
        public string No { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
