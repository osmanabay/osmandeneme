﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class QualityDto:BaseDto
    {
        public int ProductId { get; set; }
        public int Quality_PropertyId { get; set; }
        public string Value { get; set; }
        public DateTime? CreatedDate { get; set; }

        public string ProductName { get; set; }
        public string PropertyName { get; set; }

        public Product Product { get; set; }
        public Quality_Property Quality_Property { get; set; }
    }
}
