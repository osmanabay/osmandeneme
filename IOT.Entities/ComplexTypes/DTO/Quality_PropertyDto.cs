﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class Quality_PropertyDto : BaseDto
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public string ProductName { get; set; }

        public Product Product { get; set; }
        public IEnumerable<Quality> Qualitys { get; set; }
    }
}
