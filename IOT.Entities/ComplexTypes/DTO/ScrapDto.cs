﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class ScrapDto:BaseDto
    {
        public string Name { get; set; }
        public ICollection<DailyTeamWorkOrder_Scrap> DailyTeamWorkOrder_Scraps { get; set; }
    }
}
