﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class StopTypeDto:BaseDto
    {
        public string Name { get; set; }

        public ICollection<Stop> Stops { get; set; }
    }
}
