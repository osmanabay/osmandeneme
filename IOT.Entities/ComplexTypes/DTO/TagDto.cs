﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class TagDto:BaseDto
    {
        public int DeviceId { get; set; }
        public int? TypeId { get; set; }
        public string Name { get; set; }
        public string TypeName { get; set; }
        public string DeviceName { get; set; }
        public int CycleTime { get; set; }
        public int SignalFromWhere { get; set; }
        public int StartupTime { get; set; }
        public int TimeOut { get; set; }

        public Device Device { get; set; }
        public TagType Type { get; set; }
    }
}
