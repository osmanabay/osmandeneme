﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class TagTypeDto:BaseDto
    {
        public string Name { get; set; }

        public ICollection<Tag> Tags { get; set; }
    }
}
