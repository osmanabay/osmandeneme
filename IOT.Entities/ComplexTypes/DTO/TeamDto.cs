﻿using System.Collections.Generic;

namespace IOT.Entities.ComplexTypes
{
    public class TeamDto:BaseDto
    {
       
        public string LeadEmployeeName { get; set; }
        public int? LeadEmployeeId { get; set; }
        public string Name { get; set; }

        public virtual Employee LeadEmployee { get; set; }
        public virtual ICollection<DailyTeam> DailyTeams { get; set; }
        public virtual ICollection<TeamEmployee> TeamEmployees { get; set; }
    }
}
