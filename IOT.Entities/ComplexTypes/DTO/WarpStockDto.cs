﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.DTO
{
    public class WarpStockDto
    {
        public int Id { get; set; }
        public int WorkOrderId { get; set; }
        public string WarpStockNo { get; set; }
        public decimal WarpStockMeter { get; set; }
        public decimal LeftMeter { get; set; }
        public decimal UsedMeter { get; set; }

        public short Status { get; set; }
    }
}
