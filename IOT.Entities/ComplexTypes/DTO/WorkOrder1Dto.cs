﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class WorkOrder1Dto:BaseDto
    {
        public int ProductId { get; set; }
        public string No { get; set; }
        public bool? IsOpened { get; set; }
        public decimal? TargetAmount { get; set; }
        public string TargetMeter { get; set; }

        public decimal? Amount { get; set; }
        public string WarpNo { get; set; }
        public bool Sample { get; set; } = false;
        public string ProductName { get; set; }
        public bool ProductionContinue { get; set; } = false;

        public ICollection<DailyTeamWorkOrder> DailyTeamWorkOrders { get; set; }
        public Product Product { get; set; }
    }
}
