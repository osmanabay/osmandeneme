﻿using System;

namespace IOT.Entities.ComplexTypes
{
    public class ETag
    {
        public string Id { get; set; }
        public string DeviceId { get; set; }
        public string TypeId { get; set; }
        public string ShiftId { get; set; }

        public string Name { get; set; }
        public string DeviceGroupId { get; set; }
        public string DeviceGroupName { get; set; }
        public string TeamId { get; set; } = null;
        public string LeadEmployeeId { get; set; } = null;
        public string LeadEmployeeName { get; set; } = null;
        public string Value { get; set; } = "0";
        public string WorkOrderNo { get; set; }
        public string WorkOrderId { get; set; }
        public string DailyTeamWorkOrderId { get; set; } = "0";
        public string Status { get; set; }
        public string DailyTeamId { get; set; }
        public string StartDate { get; set; }
        public string CycleTime { get; set; }

        private string realTimeProductionProgressBar;
        public string RealTimeProductionProgressBar
        {
            get { return realTimeProductionProgressBar; }
            set
            {
                realTimeProductionProgressBar = Convert.ToDecimal(value) > 100 ? "100" :
                             Convert.ToDecimal(value) < 0 ? "0" : value;

            }
        }

        private string productionProgressBar;
        public string ProductionProgressBar
        {
            get => productionProgressBar;
            set =>
                productionProgressBar = Convert.ToDecimal(value) > 100 ? "100" :
                    Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string realTimeTargetAmount;
        public string RealTimeTargetAmount
        {
            get => realTimeTargetAmount;
            set => realTimeTargetAmount = Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string targetAmount;
        public string TargetAmount
        {
            get => targetAmount;
            set => targetAmount = Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string productivity;
        public string Productivity
        {
            get => productivity;
            set =>
                productivity = Convert.ToDecimal(value) > 100 ? "100" :
                    Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string efficiency;
        public string Efficiency
        {
            get => efficiency;
            set =>
                efficiency = Convert.ToDecimal(value) > 100 ? "100" :
                    Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string availability;
        public string Availability
        {
            get => availability;
            set =>
                availability = Convert.ToDecimal(value) > 100 ? "100" :
                    Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string oee;
        public string OEE
        {
            get => oee;
            set =>
                oee = Convert.ToDecimal(value) > 100 ? "100" :
                    Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        public string ShiftEndDate { get; set; } = null;
        public string ShiftStartDate { get; set; } = null;

        private string totalShiftMeter;
        public string TotalShiftMeter
        {
            get => totalShiftMeter;
            set => totalShiftMeter = Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string totalShiftWeftAmount;
        public string TotalShiftWeftAmount
        {
            get => totalShiftWeftAmount;
            set => totalShiftWeftAmount = Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string shiftTargetMeter;
        public string ShiftTargetMeter
        {
            get => shiftTargetMeter;
            set => shiftTargetMeter = Convert.ToDecimal(value) < 0 ? "0" : value;
        }


        public string WorkOrderShiftWeftAmount { get; set; } = "0";

        private string workOrderShiftMeter;
        public string WorkOrderShiftMeter
        {
            get => workOrderShiftMeter;
            set => workOrderShiftMeter = Convert.ToDecimal(value) < 0 ? "0" : value;
        }




        public string SignalFromWhere { get; set; }
        public bool StopContinue { get; set; } = false;
        public string DaliyTeamStopId { get; set; }
        public string TimeOut { get; set; }

        public string StartupTime { get; set; }
        public string NewStartupTime { get; set; }
        public string TotalTimeout { get; set; }
        public string LastSignalDate { get; set; } = null;
        public string LastAmount { get; set; }
        public bool IsStartUp { get; set; } = true;
        public string Cycle { get; set; }
        public string TotalScrap { get; set; }
        public string WeftAmount { get; set; }

        private string warpAmount;
        public string WarpAmount
        {
            get => warpAmount;
            set => warpAmount = Convert.ToDecimal(value) < 0 ? "0" : value;
        }
        public string Rpm { get; set; }
        public string ProducedMeter { get; set; }
        public string DisplayProducedMeter { get; set; }
        public string DisplayRealTimeTargetAmount { get; set; }



        public string WeftDensity { get; set; }

        public string TotalStopDuration { get; set; }
        public string PlannedStopDuration { get; set; }
        public string UnPlannedStopDuration { get; set; }
        public string RealTimeTotalPlannedStop { get; set; }
        public string RealTimeTotalUnPlannedStop { get; set; }

        public string WarpNo { get; set; }
        public string WarpMeter { get; set; }

        public string WarpId { get; set; }
        public string WarpWorkOrderId { get; set; }

        public string WarpUsedMeter { get; set; }
        public string WarpUsedWeftAmount { get; set; }


        private string leftWarpMeter;
        public string LeftWarpMeter
        {
            get => leftWarpMeter;
            set => leftWarpMeter = Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string displayLeftWarpMeter;
        public string DisplayLeftWarpMeter
        {
            get => displayLeftWarpMeter;
            set => displayLeftWarpMeter = Convert.ToDecimal(value) < 0 ? "0" : value;
        }

        private string warpProgressBar;
        public string WarpProgressBar
        {
            get => warpProgressBar;
            set =>
                warpProgressBar = Convert.ToDecimal(value) > 100 ? "100" :
                    Convert.ToDecimal(value) < 0 ? "0" : value;
        }


        public string TotalBoltMeter { get; set; }
        public string TotalBoltWeftAmount { get; set; }

        private string totalBoltProgressBar;
        public string TotalBoltProgressBar
        {
            get => totalBoltProgressBar;
            set =>
                totalBoltProgressBar = Convert.ToDecimal(value) > 100 ? "100" :
                    Convert.ToDecimal(value) < 0 ? "0" : value;
        }
        public string LastBoltMeterDate { get; set; } = null;
        public string BoltTargetMeter { get; set; }
        public string TotalBoltTargetMeter { get; set; }

        public string BoltCutDurationTime { get; set; }




        public string WeftAmountByBolt { get; set; }
        public string ProducedMeterByBolt { get; set; }

        public string Value2 { get; set; }

        public string TotalStopAmount { get; set; }

        public bool Sample { get; set; } = false;

        public bool ProductionContinue { get; set; } = false;


        public string NotWarpWeftAmount { get; set; } = "0";


    }
}
