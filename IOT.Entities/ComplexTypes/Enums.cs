﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class Enums
    {
        public enum RequestTypes
        {
            [Description("Create")]
            Create = 1,
            [Description("Read")]
            Read = 2,
            [Description("Update")]
            Update = 3,
            [Description("Delete")]
            Delete = 4,
            [Description("Any")]
            Any = 5,
            [Description("End")]
            End = 6,
            [Description("ReadMongo")]
            ReadMongo = 7,
            [Description("Shift")]
            EndShift = 8,
            [Description("Shift")]
            StartShift = 9
        }

        public enum Status
        {
            [Description("Passive")]
            Passive = 0,
            [Description("Active")]
            Active = 1
        }

       
    }
}
