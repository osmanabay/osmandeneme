﻿using IOT.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using static IOT.Entities.ComplexTypes.Enums;

namespace IOT.Entities.ComplexTypes.Request
{
    public class BaseRequest:IRequest
    {
        public int? Id { get; set; }
        public RequestTypes Type { get; set; }
        public bool IsDto { get; set; }

    }
}
