﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class BoltRequest : BaseRequest
    {
        public BoltDto Bolt { get; set; }
        public int TagId { get; set; }
        public bool IncludeDailyTeamWorkOrder { get; set; }
    }
}
