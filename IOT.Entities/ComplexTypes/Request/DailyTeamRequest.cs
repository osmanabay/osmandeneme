﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class DailyTeamRequest : BaseRequest
    {
        public DailyTeamDto DailyTeam { get; set; }
        public DailyTeamEmployee DailyTeamEmployee { get; set; }
        public bool IsDeleteDailyTeamEmployee { get; set; }
        public bool IsAddDailyTeamEmployee { get; set; }
    }
}
