﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class DailyTeamStopRequest : BaseRequest
    {
        public DailyTeamStopDto DailyTeamStop { get; set; }
    }
}
