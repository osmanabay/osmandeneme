﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class DailyTeamWorkOrderRequest : BaseRequest
    {
        public DailyTeamWorkOrderDto DailyTeamWorkOrder { get; set; }
        public object ETag { get; set; }
    }
}
