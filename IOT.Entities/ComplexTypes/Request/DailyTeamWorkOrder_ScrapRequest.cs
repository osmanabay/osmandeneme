﻿namespace IOT.Entities.ComplexTypes.Request
{
   
    public class DailyTeamWorkOrder_ScrapRequest : BaseRequest
    {
        public DailyTeamWorkOrder_ScrapDto DailyTeamWorkOrder_Scrap { get; set; }
    }
}
