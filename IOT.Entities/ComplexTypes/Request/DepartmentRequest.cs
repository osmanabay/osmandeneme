﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class DepartmentRequest : BaseRequest
    {
        public DepartmentDto Department { get; set; }
        public bool IncludeFactory { get; set; }
        public bool IncludeDeviceGroups { get; set; }
    }
}
