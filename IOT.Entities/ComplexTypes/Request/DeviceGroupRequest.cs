﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class DeviceGroupRequest : BaseRequest
    {
        public DeviceGroupDto DeviceGroup { get; set; }
        public bool IncludeDevice { get; set; }
        public bool IncludeDeviceGroupStop { get; set; }
        public Team Team { get; set; }

    }
}
