﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class DeviceGroupStopRequest : BaseRequest
    {
        public DeviceGroupStopDto DeviceGroupStop { get; set; }
        public bool _includeDeviceGroup { get; set; }
        public bool _includeStop { get; set; }
    }
}
