﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class DeviceRequest : BaseRequest
    {
        public DeviceDto Device { get; set; }
        public bool IncludeTags { get; set; }
        public bool IncludeDeviceGroup { get; set; }
    }
}
