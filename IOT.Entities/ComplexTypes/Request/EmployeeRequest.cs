﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class EmployeeRequest : BaseRequest
    {
        public EmployeeDto Employee { get; set; }
        public bool IncludeTeams { get; set; }
    }
}
