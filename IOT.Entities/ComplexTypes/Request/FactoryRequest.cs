﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class FactoryRequest : BaseRequest
    {
        public FactoryDto Factory { get; set; }
        public bool IncludeDepartments { get; set; }
    }
}
