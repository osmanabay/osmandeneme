﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class PatternRequest : BaseRequest
    {
        public PatternDto Pattern { get; set; }
        public bool IncludeProducts { get; set; }

    }
}
