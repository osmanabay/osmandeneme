﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class ProductRequest : BaseRequest
    {
        public ProductDto Product { get; set; }
        public bool IncludePattern { get; set; }
    }
}
