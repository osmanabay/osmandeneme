﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class QualityRequest : BaseRequest
    {
        public QualityDto Quality { get; set; }
        public bool IncludeProduct { get; set; }
        public bool IncludeProperty { get; set; }

    }
}
