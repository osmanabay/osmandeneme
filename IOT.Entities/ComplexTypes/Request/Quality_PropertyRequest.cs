﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class Quality_PropertyRequest : BaseRequest
    {
        public Quality_PropertyDto Quality_Property { get; set; }
        public bool IncludeProduct { get; set; }
        public bool IncludeQualitys { get; set; }

    }
}
