﻿namespace IOT.Entities.ComplexTypes.Request
{
    public class ScrapRequest : BaseRequest
    {
        public ScrapDto Scrap { get; set; }
        public bool IncludePattern { get; set; }
    }
}
