﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class ShiftRequest : BaseRequest
    {
        public ShiftDto Shift { get; set; }
    }
}
