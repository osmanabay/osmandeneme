﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class StopRequest : BaseRequest
    {
        public StopDto Stop { get; set; }
        public bool includeStopType { get; set; }
    }
}
