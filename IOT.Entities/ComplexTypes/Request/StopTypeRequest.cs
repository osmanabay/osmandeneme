﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class StopTypeRequest : BaseRequest
    {
        public StopTypeDto StopType { get; set; }
        public bool IncludeStops { get; set; }
    }
}
