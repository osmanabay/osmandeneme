﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class TagRequest : BaseRequest
    {
        public TagDto Tag { get; set; }
        public bool includeDevice { get; set; }
        public bool includeTagType { get; set; }
    }
}
