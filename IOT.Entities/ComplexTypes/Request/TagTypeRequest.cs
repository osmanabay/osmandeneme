﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class TagTypeRequest : BaseRequest
    {
        public TagTypeDto TagType { get; set; }
    }
}
