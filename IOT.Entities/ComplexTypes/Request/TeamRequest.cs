﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class TeamRequest : BaseRequest
    {
        public TeamDto Team { get; set; }
        public TeamEmployee TeamEmployee { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsDeleteTeamEmployee { get; set; }
        public bool IsAddTeamEmployee { get; set; }
        public bool IncludeEmployee { get; set; }
    }
}
