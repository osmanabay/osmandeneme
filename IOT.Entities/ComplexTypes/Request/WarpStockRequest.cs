﻿using IOT.Entities.ComplexTypes.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class WarpStockRequest : BaseRequest
    {
        public WarpStockDto WarpStock { get; set; }
        public int TagId { get; set; }
    }
}
