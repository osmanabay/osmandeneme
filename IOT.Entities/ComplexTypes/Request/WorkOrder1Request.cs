﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.Request
{
    public class WorkOrder1Request : BaseRequest
    {
        public WorkOrder1Dto WorkOrder { get; set; }
        public string deviceName { get; set; }
        public string tagId { get; set; }
        public bool includeProduct { get; set; }
        public bool IsOpened { get; set; }
    }
}
