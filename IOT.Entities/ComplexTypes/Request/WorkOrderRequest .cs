﻿using System;
using System.Collections.Generic;
using System.Text;
using IOT.Entities.ComplexTypes.DTO;

namespace IOT.Entities.ComplexTypes.Request
{
    public class WorkOrderRequest : BaseRequest
    {
        public WorkOrderDto WorkOrder { get; set; }
        public string deviceName { get; set; }
        public int deviceId { get; set; }
        public string tagId { get; set; }
        public bool includeProduct { get; set; }
        public bool includeWarpStock { get; set; }
        public bool includeDevice{ get; set; }


        public bool IsOpened { get; set; }
    }
}
