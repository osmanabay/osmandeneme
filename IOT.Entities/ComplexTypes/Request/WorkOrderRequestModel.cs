﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IOT.Entities.Concrete;
using static IOT.Entities.ComplexTypes.Enums;

namespace IOT.Entities.ComplexTypes.Request
{
    public class WorkOrderRequestModel
    {
        public int Id { get; set; }
        public decimal WeftDensity { get; set; }
        public string WorkOrderNo { get; set; } = null;
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public int Status { get; set; }

        public decimal Meter { get; set; }

        public decimal TargetMeter { get; set; }
        public string WarpNo { get; set; }
        public List<WarpStock> WarpStocks { get; set; }
        public string DeviceId { get; set; }
        public int OrderId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public RequestTypes RequestType { get; set; }



    }
}
