﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes
{
    public class ResponseData
    {
        public int Id { get; set; }
        public bool IsSucceed { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public string ClientIpAddress { get; set; }
    }
}
