﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities.ComplexTypes.ViewModel
{
    public class TeamViewModel
    {
        public TeamDto TeamDto { get; set; }
        public IEnumerable<EmployeeDto> EmployeesByTeamDtos { get; set; }
        public IEnumerable<EmployeeDto> EmployeesOtherDtos { get; set; }
        public IEnumerable<DeviceDto> DeviceDtos{ get; set; }
        public IEnumerable<StopTypeDto> StopTypeDtos{ get; set; }

    }
}
