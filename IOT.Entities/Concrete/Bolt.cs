﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities
{
    public class Bolt:BaseEntity
    {
        public int DailyTeamWorkOrderId  { get; set; }
        public string Meter { get; set; }
        public string WeftAmount { get; set; }
        public string TargetAmount { get; set; }

        public virtual DailyTeamWorkOrder DailyTeamWorkOrder { get; set; }
    }
}
