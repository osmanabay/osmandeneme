﻿using IOT.Core;
using System.Collections.Generic;

namespace IOT.Entities
{
    public partial class DailyTeam:IBaseEntity
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public int? DeviceId { get; set; }
        public int? LeadEmployeeId { get; set; }
        public string LeadEmployeeName { get; set; }


        public virtual Device Device { get; set; }
        public virtual Employee LeadEmployee { get; set; }
        public virtual Team Team { get; set; }

        public virtual ICollection<DailyTeamEmployee> DailyTeamEmployees { get; set; }
        public virtual ICollection<DailyTeamWorkOrder> DailyTeamWorkOrders { get; set; }
        public virtual ICollection<DailyTeamStop> DailyTeamStops { get; set; }
    }
}