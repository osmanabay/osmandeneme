﻿using IOT.Core;

namespace IOT.Entities
{
    public partial class DailyTeamEmployee:IBaseEntity
    {
        public int DailyTeamId { get; set; }
        public int EmployeeId { get; set; }

        public virtual DailyTeam DailyTeam { get; set; }
        public virtual Employee Employee { get; set; }

     
    }
}