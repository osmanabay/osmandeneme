﻿using IOT.Core;
using System;

namespace IOT.Entities
{
    public class DailyTeamStop:IBaseEntity
    {
        public int Id { get; set; }
        public int DailyTeamId { get; set; }
        public int StopId { get; set; }
        public int? Duration { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DailyTeam DailyTeam { get; set; }
        public Stop Stop { get; set; }
    }
}