﻿using IOT.Core;
using System;
using System.Collections.Generic;

namespace IOT.Entities
{
    public class DailyTeamWorkOrder:IBaseEntity
    {
        public int Id { get; set; }
        public int DailyTeamId { get; set; }
        public int WorkOrderId { get; set; }
        public int ShiftId { get; set; }
        public int? Status { get; set; }
        public decimal? TargetAmount { get; set; }
        public decimal?  Amount { get; set; }
        public decimal? WeftAmount { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreatedDate { get; set; } = DateTime.Now;
        public decimal? StartupTime { get; set; }
        public decimal? TotalTimeout { get; set; }

        public WorkOrder1 WorkOrder { get; set; }
        public Shift Shift { get; set; }
        public DailyTeam DailyTeam { get; set; }

        public ICollection<Bolt> Devices { get; set; }
    }
}