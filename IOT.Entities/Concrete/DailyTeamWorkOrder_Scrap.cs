﻿using IOT.Core;
using System;


namespace IOT.Entities
{
    public class DailyTeamWorkOrder_Scrap : IBaseEntity
    {
        public int Id { get; set; }
        public int DailyTeamWorkOrderId { get; set; }
        public int ScrapId { get; set; }
        public decimal?  Amount { get; set; }
        public DateTime? CreatedDate { get; set; }

        public DailyTeamWorkOrder DailyTeamWorkOrder { get; set; }
        public Scrap Scrap { get; set; }
    }
}