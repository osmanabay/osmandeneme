﻿using System.Collections.Generic;

namespace IOT.Entities
{
    public partial class Department:BaseEntity
    {
        public string Name { get; set; }
        public int FactoryId { get; set; }
        public Factory Factory { get; set; }

        public ICollection<DeviceGroup> DeviceGroups { get; set; }
    }
}
