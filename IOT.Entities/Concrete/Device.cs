﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IOT.Entities
{
    public partial class Device:BaseEntity
    {
        public Device()
        {
            Tags = new HashSet<Tag>();
            DailyTeams= new HashSet<DailyTeam>();
        }

        public string Name { get; set; }
        public string DeviceNo { get; set; }
        public bool IsOpened { get; set; }
        public int? DeviceGroupId { get; set; }

        public virtual ICollection<DailyTeam> DailyTeams { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual DeviceGroup DeviceGroup { get; set; }
    }
}
