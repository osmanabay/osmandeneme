﻿using System.Collections.Generic;

namespace IOT.Entities
{
    public class DeviceGroup:BaseEntity
    {
        public string Name { get; set; }
        public string IPAddress { get; set; }

        public int DepartmentId { get; set; }

        public ICollection<Device> Devices { get; set; }
        public ICollection<DeviceGroupStop> DeviceGroupStops { get; set; }
        public Department Department { get; set; }
    }
}