﻿using IOT.Core;
using System;

namespace IOT.Entities
{
    public class DeviceGroupStop:IBaseEntity
    {
        public int Id { get; set; }
        public int? DeviceGroupId { get; set; }
        public int? StopId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public DeviceGroup DeviceGroup { get; set; }
        public Stop Stop { get; set; }
    }
}