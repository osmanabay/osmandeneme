﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IOT.Entities
{
    public partial class Employee:BaseEntity
    {
        public Employee()
        {
            DailyTeams = new HashSet<DailyTeam>();
            Teams = new HashSet<Team>();
            TeamEmployees = new HashSet<TeamEmployee>();
            DailyTeamEmployees = new HashSet<DailyTeamEmployee>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string Password { get; set; }


        public virtual ICollection<DailyTeam> DailyTeams { get; set; }
        public virtual ICollection<DailyTeamEmployee> DailyTeamEmployees { get; set; }
        public virtual ICollection<Team> Teams { get; set; }
        public virtual ICollection<TeamEmployee> TeamEmployees { get; set; }
    }
}