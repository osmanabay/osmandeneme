﻿using System.Collections.Generic;

namespace IOT.Entities
{
    public partial class Factory : BaseEntity
    {
        public Factory()
        {
            Departments = new HashSet<Department>();
        }
        public string Name { get; set; }
        public ICollection<Department> Departments { get; set; }
    }
}
