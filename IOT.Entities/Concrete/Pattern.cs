﻿using System.Collections.Generic;

namespace IOT.Entities
{
    public class Pattern:BaseEntity
    {
        public string Name { get; set; }
        public string No { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}