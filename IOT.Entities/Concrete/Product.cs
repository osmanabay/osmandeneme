﻿using System;
using System.Collections.Generic;

namespace IOT.Entities
{
    public partial class Product : BaseEntity
    {
        public int PatternId { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public string StockCode { get; set; }
        public decimal WeftDensity { get; set; }


        public Pattern Pattern { get; set; }
        public ICollection<WorkOrder1> WorkOrders { get; set; }
    }
}
