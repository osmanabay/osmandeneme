﻿using IOT.Core;
using System;
using System.Collections.Generic;

namespace IOT.Entities
{
    public partial class Quality : IBaseEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Quality_PropertyId { get; set; }
        public string Value { get; set; }
        public DateTime? CreatedDate { get; set; }

        public Product Product { get; set; }
        public Quality_Property Quality_Property { get; set; }
    }
}
