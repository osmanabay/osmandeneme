﻿using System;
using System.Collections.Generic;

namespace IOT.Entities
{
    public partial class Quality_Property : BaseEntity
    {
        public Quality_Property()
        {
            CreatedDate = DateTime.Now;
        }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public Product Product { get; set; }
        public IEnumerable<Quality> Qualitys { get; set; }
    }
}
