﻿using System.Collections.Generic;

namespace IOT.Entities
{
    public partial class Scrap : BaseEntity
    {
        
        public string Name { get; set; }
        public ICollection<DailyTeamWorkOrder_Scrap> DailyTeamWorkOrder_Scraps { get; set; }
    }
}
