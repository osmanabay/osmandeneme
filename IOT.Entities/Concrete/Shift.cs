﻿using System;
using System.Collections.Generic;

namespace IOT.Entities
{
    public partial class Shift : BaseEntity
    {
        public string Name { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public ICollection<DailyTeamWorkOrder> DailyTeamWorkOrders { get; set; }
    }
}
