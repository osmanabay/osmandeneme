﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities
{
    public class Stop:BaseEntity
    {
        public int StopTypeId { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }

        public ICollection<DailyTeamStop> DailyTeamStops { get; set; }
        public ICollection<DeviceGroupStop> DeviceGroupStops { get; set; }

        public StopType StopType { get; set; }
    }
}
