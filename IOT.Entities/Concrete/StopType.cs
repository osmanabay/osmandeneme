﻿using IOT.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities
{
    public class StopType : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Stop> Stops { get; set; }
    }
}
