﻿namespace IOT.Entities
{
    public partial class Tag : BaseEntity
    {
        public int DeviceId { get; set; }
        public int? TypeId { get; set; }
        //public int? TagType { get; set; }
        public string Name { get; set; }
        public int? Value { get; set; }
        public int? CycleTime { get; set; }
        public int SignalFromWhere { get; set; }
        public int StartupTime { get; set; }
        public int TimeOut { get; set; }
        //public string WorkOrderNo { get; set; }
        //public string WorkOrderId { get; set; }
        //public string Status { get; set; }
        //public string DailyTeamId { get; set; }
        //public string TargetAmount { get; set; }
        //public string StartDate { get; set; }

        public Device Device { get; set; }
        public TagType Type { get; set; }
    }
}
