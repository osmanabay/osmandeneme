﻿using IOT.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace IOT.Entities
{
    public class TagType:BaseEntity
    {
        public string Name { get; set; }

        public ICollection<Tag> Tags { get; set; }
    }
}
