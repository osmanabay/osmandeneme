﻿using System.Collections.Generic;

namespace IOT.Entities
{
    public partial class Team:BaseEntity
    {
        public Team()
        {
            DailyTeams = new HashSet<DailyTeam>();
            TeamEmployees = new HashSet<TeamEmployee>();
        }

        public int? LeadEmployeeId { get; set; }
        public string LeadEmployeeName { get; set; }
        public string Name { get; set; }

        public virtual Employee LeadEmployee { get; set; }
        public virtual ICollection<DailyTeam> DailyTeams { get; set; }
        public virtual ICollection<TeamEmployee> TeamEmployees { get; set; }
    }
}