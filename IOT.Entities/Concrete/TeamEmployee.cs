﻿using IOT.Core;
namespace IOT.Entities
{
    public partial class TeamEmployee:IBaseEntity
    {
        public int TeamId { get; set; }
        public int EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Team Team { get; set; }
    }
}