﻿using System;
using IOT.Core;

namespace IOT.Entities.Concrete
{
    public class WarpStock : IBaseEntity
    {
        public int Id { get; set; }
        public int WorkOrderId { get; set; }
        public string WarpStockNo { get; set; }
        public decimal WarpStockMeter { get; set; }
        public decimal LeftMeter { get; set; }
        public decimal UsedMeter { get; set; }

        public short Status { get; set; }
        public DateTime? CutWarpDate { get; set; }

        public WorkOrder WorkOrder { get; set; }
    }
}
