﻿using System;
using System.Collections.Generic;
using IOT.Entities.Concrete;


namespace IOT.Entities
{
    public class WorkOrder : BaseEntity
    {
        public int DeviceId { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string No { get; set; }
        public bool? IsOpened { get; set; }
        public decimal? TargetAmount { get; set; }
        public decimal? Amount { get; set; }
        public string WarpNo { get; set; }
        public decimal WeftDensity { get; set; }

        public decimal? WeftAmount { get; set; }
        public bool Sample { get; set; } = false;
        public int Status { get; set; } = 0;
        public bool ProductionContinue { get; set; } = false;
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public List<WarpStock> WarpStocks { get; set; }
        public Device Device { get; set; }

    }
}