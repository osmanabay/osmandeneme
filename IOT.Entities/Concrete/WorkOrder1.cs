﻿using System;
using System.Collections.Generic;

namespace IOT.Entities
{
    public class WorkOrder1:BaseEntity
    {
        public int ProductId { get; set; }
        public string No { get; set; }
        public bool? IsOpened { get; set; }
        public decimal? TargetAmount { get; set; }
        public decimal? Amount { get; set; }
        public string WarpNo { get; set; }
        public bool Sample { get; set; } = false;
        public bool ProductionContinue { get; set; } = false;

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public ICollection<DailyTeamWorkOrder> DailyTeamWorkOrders { get; set; }
        public Product Product { get; set; }
    }
}