﻿using IOT.Entities;
using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Signals.Models.Abstract;
using IOT.Signals.Models.MongoDB.Concrete;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IOT.Entities.ComplexTypes.DTO;

namespace IOT.Signals.Api
{
    public class Api
    {
        public static string ApiUrl;
        public static string SignalUrl;
        public static bool shiftTransitionControl = true;
        public static bool stopControl = true;



        readonly ITagRepository<MTag> _context;
        public Api(ITagRepository<MTag> context)
        {
            _context = context;
        }

        internal static ResponseData AddWorkOrder(MTag mTag)
        {

            ResponseData response = new ResponseData();
            try
            {
                using (var client = new HttpClient())
                {

                    WorkOrderRequest workOrderRequest = new WorkOrderRequest
                    {
                        Type = Enums.RequestTypes.Create
                    };

                    WorkOrderDto workOrderDto = new WorkOrderDto()
                    {
                        TargetAmount = 0,
                        ProductId = "1",
                        Amount = 0,
                        DeviceId=Convert.ToInt32(mTag.DeviceId),
                        IsOpened = true,
                        No = "#" + DateTime.Now.ToString().Replace(".", "").Replace(" ", "").Replace(":", ""),
                        WarpNo = mTag.WarpNo,
                        ProductionContinue = true,
                        Sample = false
                    };
                    workOrderRequest.WorkOrder = workOrderDto;

                    string jsonUpdateBody = JsonConvert.SerializeObject(workOrderRequest);

                    var createWorkOrder = client.PostAsync(ApiUrl + "api/WorkOrder/", new StringContent(jsonUpdateBody,
                                             Encoding.UTF8,
                                             "application/json")).Result;

                    var jsonString = createWorkOrder.Content.ReadAsStringAsync().Result;
                    response = JsonConvert.DeserializeObject<ResponseData>(jsonString);

                    WorkOrder workOrder = JsonConvert.DeserializeObject<WorkOrder>(response.Data.ToString());

                    DailyTeamDto dto = new DailyTeamDto
                    {
                        DeviceId = Convert.ToInt32(mTag.DeviceId)
                    };

                    DailyTeamRequest dailyTeamRequest = new DailyTeamRequest
                    {
                        DailyTeam = dto,
                        Type = Enums.RequestTypes.Create
                    };


                    jsonUpdateBody = JsonConvert.SerializeObject(dailyTeamRequest);

                    var createDailyTeam = client.PostAsync(ApiUrl + "api/DailyTeam/", new StringContent(jsonUpdateBody,
                                             Encoding.UTF8,
                                             "application/json")).Result;

                    jsonString = createDailyTeam.Content.ReadAsStringAsync().Result;
                    response = JsonConvert.DeserializeObject<ResponseData>(jsonString);

                    DailyTeam dailyTeam = JsonConvert.DeserializeObject<DailyTeam>(response.Data.ToString());

                    DailyTeamWorkOrderDto dailyTeamWorkOrderDto = new DailyTeamWorkOrderDto
                    {
                        DailyTeamId = dailyTeam.Id,
                        WorkOrderId = workOrder.Id,
                        WorkOrderNo = workOrder.No,
                        Status = 1,
                        DeviceId = Convert.ToInt32(mTag.DeviceId),
                        DeviceName = mTag.Name,
                        TagId = Convert.ToInt32(mTag.Id),
                        CycleTime = "0",
                        StartupTime = Convert.ToDecimal(mTag.StartupTime),
                        TargetAmount = workOrder.TargetAmount,

                    };


                    DailyTeamWorkOrderRequest dailyTeamWorkOrderRequest = new DailyTeamWorkOrderRequest
                    {
                        DailyTeamWorkOrder = dailyTeamWorkOrderDto,
                        Type = Enums.RequestTypes.Create
                    };

                    var jsonBody1 = JsonConvert.SerializeObject(dailyTeamWorkOrderRequest);

                    var readResponse1 = client.PostAsync(ApiUrl + "api/DailyTeamWorkOrder/", new StringContent(jsonBody1,
                                             Encoding.UTF8,
                                             "application/json")).Result;

                    var jsonString1 = readResponse1.Content.ReadAsStringAsync().Result;
                    response = JsonConvert.DeserializeObject<ResponseData>(jsonString1);

                    //DailyTeamWorkOrder dailyTeamWorkOrder = JsonConvert.DeserializeObject<DailyTeamWorkOrder>(data.Data.ToString());



                    //var jsonBody2 = Newtonsoft.Json.JsonConvert.SerializeObject(mTag.Id);

                    var httpResponse = client.GetAsync(SignalUrl + "api/signal/getDevice/" + mTag.Id.ToString()).Result;
                    var jsonString2 = httpResponse.Content.ReadAsStringAsync().Result;
                    response.Data = JsonConvert.DeserializeObject<MTag>(jsonString2);

                }

                response.IsSucceed = true;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSucceed = false;
                response.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                return response;

            }
        }

        internal static async Task<ResponseData> FinishDailyWorkOrder(MTag mtag)
        {
            ResponseData data = new ResponseData();

            try
            {
                if (mtag == null)
                {
                    throw new ArgumentNullException(nameof(mtag));
                }

                using (var client = new HttpClient())
                {
                    //DailyTeamWorkOrderRequest request = new DailyTeamWorkOrderRequest
                    //{
                    //    Id = Convert.ToInt32(mtag.DailyTeamWorkOrderId),
                    //    Type = Enums.RequestTypes.Read
                    //};

                    //string jsonBody = JsonConvert.SerializeObject(request);

                    //var readResponse = await client.PostAsync(ApiUrl + "api/DailyTeamWorkOrder/", new StringContent(jsonBody,
                    //                        Encoding.UTF8,
                    //                        "application/json"));

                    //var jsonString = await readResponse.Content.ReadAsStringAsync();
                    //data = JsonConvert.DeserializeObject<ResponseData>(jsonString);
                    //if (data.IsSucceed)
                    //{

                    //    DailyTeamWorkOrder dailyTeamWorkOrder = JsonConvert.DeserializeObject<DailyTeamWorkOrder>(data.Data.ToString());
                    //    var mtagValue = Convert.ToDecimal(mtag.Value);
                    //    var mtagStartupTime = Convert.ToDecimal(mtag.StartupTime);
                    //    var mtagTotalTimeout = Convert.ToDecimal(Math.Round(Convert.ToDecimal(mtag.TotalTimeout), 0));

                    //    DailyTeamWorkOrderDto dto = new DailyTeamWorkOrderDto
                    //    {
                    //        Amount = Convert.ToDecimal(mtag.TotalShiftMeter),
                    //        TargetAmount = dailyTeamWorkOrder.TargetAmount,
                    //        ShiftId = dailyTeamWorkOrder.ShiftId,
                    //        StartDate = dailyTeamWorkOrder.StartDate,
                    //        EndDate = DateTime.Now,
                    //        DailyTeamId = dailyTeamWorkOrder.DailyTeamId,
                    //        Status = 0,
                    //        WorkOrderId = dailyTeamWorkOrder.WorkOrderId,
                    //        Id = (int)request.Id,
                    //        TagId = Convert.ToInt32(mtag.Id),
                    //        CreatedDate = dailyTeamWorkOrder.CreatedDate,
                    //        StartupTime = mtagStartupTime,
                    //        TotalTimeout = mtagTotalTimeout
                    //    };

                    //    request.Type = Enums.RequestTypes.EndShift;
                    //    request.DailyTeamWorkOrder = dto;

                    //    string jsonUpdateBody = JsonConvert.SerializeObject(request);

                    //    var updateResponse = await client.PostAsync(ApiUrl + "api/DailyTeamWorkOrder/", new StringContent(jsonUpdateBody,
                    //                             Encoding.UTF8,
                    //                             "application/json"));

                    //    jsonString = await updateResponse.Content.ReadAsStringAsync();
                    //    data = JsonConvert.DeserializeObject<ResponseData>(jsonString);


                    //    var jsonBody2 = Newtonsoft.Json.JsonConvert.SerializeObject(mtag.Id);


                    //    var httpResponse = await client.GetAsync(SignalUrl + "api/signal/getDevice/" + mtag.Id.ToString());
                    //    var jsonString2 = await httpResponse.Content.ReadAsStringAsync();
                    //    data.Data = JsonConvert.DeserializeObject<MTag>(jsonString2);
                    //    data.IsSucceed = true;

                    //    return data;

                    //}

                    //----------------------------------------------------------------------------------


                    DailyTeamWorkOrderRequest request = new DailyTeamWorkOrderRequest();

                    request.Type = Enums.RequestTypes.EndShift;
                    request.ETag = mtag;

                    string jsonUpdateBody = JsonConvert.SerializeObject(request);

                    var updateResponse = await client.PostAsync(ApiUrl + "api/DailyTeamWorkOrder/", new StringContent(jsonUpdateBody,
                                             Encoding.UTF8,
                                             "application/json"));

                    var jsonString = await updateResponse.Content.ReadAsStringAsync();
                    data = JsonConvert.DeserializeObject<ResponseData>(jsonString);

                }

                return data;

            }
            catch (Exception ex)
            {
                data.IsSucceed = false;
                data.Message = ex.InnerException == null ?
                               ex.Message :
                               ex.InnerException.Message;
                return data;
            }
        }

        internal static async Task<ResponseData> StartDailyWorkOrder(MTag mtag)
        {
            try
            {
                ResponseData data = new ResponseData();
                if (mtag == null)
                {
                    throw new ArgumentNullException(nameof(mtag));
                }

                using (var client = new HttpClient())
                {

                    DailyTeamWorkOrderRequest request = new DailyTeamWorkOrderRequest();

                    request.Type = Enums.RequestTypes.StartShift;
                    //request.Id = Convert.ToInt32(mtag.Id);
                    request.ETag = mtag;


                    string jsonUpdateBody = JsonConvert.SerializeObject(request);

                    var updateResponse = await client.PostAsync(ApiUrl + "api/DailyTeamWorkOrder/", new StringContent(jsonUpdateBody,
                                             Encoding.UTF8,
                                             "application/json"));

                    var jsonString = await updateResponse.Content.ReadAsStringAsync();
                    data = JsonConvert.DeserializeObject<ResponseData>(jsonString);

                    //-------------------------------------------------------------------------------------------

                    //    DailyTeamDto dto = new DailyTeamDto
                    //    {
                    //        DeviceId = Convert.ToInt32(mtag.DeviceId)
                    //    };

                    //    DailyTeamRequest request = new DailyTeamRequest
                    //    {
                    //        DailyTeam = dto,
                    //        Type = Enums.RequestTypes.Create
                    //    };


                    //    string jsonUpdateBody = JsonConvert.SerializeObject(request);

                    //    var createDailyTeam =await client.PostAsync(ApiUrl + "api/DailyTeam/", new StringContent(jsonUpdateBody,
                    //                             Encoding.UTF8,
                    //                             "application/json"));

                    //    var jsonString = await createDailyTeam.Content.ReadAsStringAsync();
                    //    data = JsonConvert.DeserializeObject<ResponseData>(jsonString);

                    //    DailyTeam dailyTeam = JsonConvert.DeserializeObject<DailyTeam>(data.Data.ToString());


                    //    WorkOrderRequest workOrderRequest = new WorkOrderRequest
                    //    {
                    //        Id = Convert.ToInt32(mtag.WorkOrderId),
                    //        Type = Enums.RequestTypes.Read
                    //    };

                    //    string jsonBody = JsonConvert.SerializeObject(workOrderRequest);

                    //    var readResponse = await client.PostAsync(ApiUrl + "api/WorkOrder/", new StringContent(jsonBody,
                    //                            Encoding.UTF8,
                    //                            "application/json"));

                    //    jsonString = await readResponse.Content.ReadAsStringAsync();
                    //    data = JsonConvert.DeserializeObject<ResponseData>(jsonString);

                    //    WorkOrder workOrder = JsonConvert.DeserializeObject<WorkOrder>(data.Data.ToString());

                    //    DailyTeamWorkOrderDto dailyTeamWorkOrderDto = new DailyTeamWorkOrderDto
                    //    {
                    //        Id = Convert.ToInt32(mtag.DailyTeamWorkOrderId),
                    //        DailyTeamId = dailyTeam.Id,
                    //        WorkOrderId = Convert.ToInt32(mtag.WorkOrderId),
                    //        WorkOrderNo = mtag.WorkOrderNo,
                    //        Status = 1,
                    //        DeviceId = Convert.ToInt32(mtag.DeviceId),
                    //        DeviceName = mtag.Name,
                    //        TagId = Convert.ToInt32(mtag.Id),
                    //        CycleTime = "0",
                    //        StartupTime = Convert.ToDecimal(mtag.StartupTime),
                    //        TargetAmount = workOrder.TargetAmount,

                    //    };


                    //    DailyTeamWorkOrderRequest dailyTeamWorkOrderRequest = new DailyTeamWorkOrderRequest
                    //    {
                    //        DailyTeamWorkOrder = dailyTeamWorkOrderDto,
                    //        Type = Enums.RequestTypes.Create
                    //    };

                    //    var jsonBody1 = JsonConvert.SerializeObject(dailyTeamWorkOrderRequest);

                    //    var readResponse1 = await client.PostAsync(ApiUrl + "api/DailyTeamWorkOrder/", new StringContent(jsonBody1,
                    //                             Encoding.UTF8,
                    //                             "application/json"));

                    //    var jsonString1 = await readResponse1.Content.ReadAsStringAsync();
                    //    data = JsonConvert.DeserializeObject<ResponseData>(jsonString1);


                    //    var jsonBody2 = Newtonsoft.Json.JsonConvert.SerializeObject(mtag.Id);


                    //    var httpResponse = await client.GetAsync(SignalUrl + "api/signal/getDevice/" + mtag.Id.ToString());
                    //    var jsonString2 = await httpResponse.Content.ReadAsStringAsync();
                    //    data.Data = JsonConvert.DeserializeObject<MTag>(jsonString2);
                    //    data.IsSucceed = true;

                }
                return data;
            }
            catch (Exception)
            {
                return new ResponseData()
                {
                    IsSucceed = false
                };
            }
        }

        public static MTag DefaultTagValue(MTag mtag)
        {
            mtag.DailyTeamWorkOrderId = "0";
            mtag.LastAmount = "0";
            mtag.Value = "0";
            mtag.Availability = "0";
            mtag.Efficiency = "0";
            mtag.LastSignalDate = null;
            mtag.OEE = "0";
            mtag.ProductionProgressBar = "0";
            mtag.Productivity = "0";
            mtag.RealTimeProductionProgressBar = "0";
            mtag.RealTimeTargetAmount = "0";
            mtag.DisplayProducedMeter = "0";
            mtag.DisplayRealTimeTargetAmount = "0";

            //mtag.ShiftEndDate = null;
            //mtag.ShiftStartDate = null;
            //mtag.StartDate = null;
            mtag.Status = "0";
            mtag.IsStartUp = true;
            mtag.StopContinue = false;
            //mtag.TargetAmount = "0";
            mtag.TotalTimeout = "0";
            mtag.NewStartupTime = "0";
            mtag.WarpAmount = "0";
            mtag.WeftAmount = "0";
            mtag.Rpm = "0";
            mtag.ProducedMeter = "0";
            mtag.WeftDensity = "10";
            mtag.TotalStopDuration = "0";
            mtag.PlannedStopDuration = "0";
            mtag.UnPlannedStopDuration = "0";
            mtag.RealTimeTotalPlannedStop = "0";
            mtag.RealTimeTotalUnPlannedStop = "0";
            mtag.TotalBoltWeftAmount = "0";
            mtag.TotalBoltMeter = "0";
            mtag.WeftAmountByBolt = "0";
            mtag.ProducedMeterByBolt = "0";
            //mtag.WarpProgressBar = "0";
            mtag.TotalBoltProgressBar = "0";
            mtag.LastBoltMeterDate = null;
            mtag.BoltTargetMeter = "0";
            mtag.TotalBoltTargetMeter = "0";
            mtag.TotalStopAmount = "0";
            mtag.BoltCutDurationTime = "0";

            return mtag;
        }

        public static MTag Reset(MTag mtag)
        {
            mtag.ProductionContinue = false;
            mtag.WorkOrderShiftWeftAmount = "0";
            mtag.Value = "0";
            mtag.WorkOrderNo = "0";
            mtag.WorkOrderId = "0";
            mtag.DailyTeamWorkOrderId = "0";
            mtag.Status = "0";
            mtag.DailyTeamId = "0";
            mtag.StartDate = "0";
            mtag.CycleTime = "0";
            mtag.RealTimeProductionProgressBar = "0";
            mtag.ProductionProgressBar = "0";
            mtag.RealTimeTargetAmount = "0";
            mtag.TargetAmount = "0";
            mtag.Productivity = "0";
            mtag.Efficiency = "0";
            mtag.Availability = "0";
            mtag.OEE = "0";
            mtag.ShiftEndDate = null;
            mtag.ShiftStartDate = null;
            mtag.TotalShiftMeter = "0";
            mtag.TotalShiftWeftAmount = "0";
            mtag.ShiftTargetMeter = "0";
            mtag.StopContinue = false;
            mtag.DaliyTeamStopId = "0";
            //mtag.TimeOut = "0";
            //mtag.StartupTime = "0";
            mtag.NewStartupTime = "0";
            mtag.TotalTimeout = "0";
            //mtag.TimeOut = "0";
            mtag.LastSignalDate = null;
            mtag.LastAmount = "0";
            mtag.IsStartUp = false;
            mtag.Cycle = "0";
            mtag.TotalScrap = "0";
            mtag.WeftAmount = "0";
            mtag.WarpAmount = "0";
            mtag.Rpm = "0";
            mtag.ProducedMeter = "0";
            mtag.DisplayProducedMeter = "0";
            mtag.DisplayRealTimeTargetAmount = "0";
            mtag.WeftDensity = "1";
            mtag.TotalStopDuration = "0";
            mtag.PlannedStopDuration = "0";
            mtag.UnPlannedStopDuration = "0";
            mtag.RealTimeTotalPlannedStop = "0";
            mtag.RealTimeTotalUnPlannedStop = "0";
            mtag.WarpNo = "0";
            mtag.WarpMeter = "0";
            mtag.LeftWarpMeter = "0";
            mtag.DisplayLeftWarpMeter = "0";
            mtag.WarpProgressBar = "0";
            mtag.TotalBoltMeter = "0";
            mtag.TotalBoltWeftAmount = "0";
            mtag.TotalBoltProgressBar = "0";
            mtag.LastBoltMeterDate = null;
            mtag.BoltTargetMeter = "0";
            mtag.TotalBoltTargetMeter = "0";
            mtag.BoltCutDurationTime = "0";
            mtag.WeftAmountByBolt = "0";
            mtag.ProducedMeterByBolt = "0";
            mtag.Value2 = "0";
            mtag.TotalStopAmount = "0";
            mtag.ShiftId = "0";
            mtag.WarpUsedWeftAmount = "0";
            mtag.WarpUsedMeter = "0";
            mtag.WorkOrderShiftMeter = "0";


            return mtag;
        }

        public List<MTag> GetTags()
        {
            ResponseData response = _context.GetList();
            response.Data = ((List<MTag>)response.Data)/*.Select(a => new MTag(a.Id, a.TypeId, a.Name, a.Value, a.WorkOrderNo, a.WorkOrderId, a.DailyTeamWorkOrderId, a.Status, a.DailyTeamId, a.StartDate, a.CycleTime, a.RealTimeProductionProgressBar, a.ProductionProgressBar, a.RealTimeTargetAmount, a.TargetAmount, a.Productivity, a.Efficiency, a.Availability, a.OEE, a.ShiftEndDate, a.ShiftStartDate, a.SignalFromWhere, a.TimeOut, a.StartupTime, a.LastAmount, a.LastSignalDate, a.StopContinue, a.DaliyTeamStopId, a.DeviceId, a.IsStartUp,a.NewStartupTime,a.Cycle))*/.ToList();

            List<MTag> mlist = (List<MTag>)response.Data;
            return mlist;
        }

        internal static async Task<MTag> TagControlAsync(MTag mtag)
        {
            try
            {
                //İş emri yoksa hesaplamaları yapma
                if (mtag.DailyTeamWorkOrderId != "0")
                {
                    #region Hesaplamalar
                    //Hazırlık süresinden önce üretim başladıysa bu zaman kadar olan süreye göre hesaplamaları yap
                    var startupTime = Convert.ToDecimal(mtag.NewStartupTime == "0" ? mtag.StartupTime : mtag.NewStartupTime);

                    //Vardiyadaki iş emrinin başladığı saatten şuankini çıkararak geçen süreyi saniye olarak bul
                    var totalSeconds = Convert.ToDecimal((DateTime.Now - Convert.ToDateTime(mtag.StartDate)).TotalSeconds);

                    //var cycleTime = Convert.ToInt32(mtag.CycleTime);
                    var amount = Convert.ToInt32(mtag.Value);

                    #region Startup & TimeOut Controls
                    //Hazırlanma süresi 
                    if (totalSeconds < startupTime && mtag.IsStartUp)
                    {
                        if (Convert.ToInt32(mtag.WeftAmount) > 0) // Hazırlık sürecinde sinyal gelirse startuptime ve hedefi belirle
                        {

                            var firstStartupTime = Convert.ToInt32(mtag.StartupTime);
                            var newStartupTime = Convert.ToInt32(totalSeconds);
                            //mtag.StartupTime = newStartupTime.ToString();
                            //targetAmount = (firstStartupTime - newStartupTime) / cycleTime;

                            //Hazırlık sürecini bitirmek için false yapıldı
                            mtag.IsStartUp = false;
                            mtag.NewStartupTime = newStartupTime.ToString();
                            mtag.LastAmount = amount.ToString();
                        }
                        else
                            totalSeconds = 0;

                        //sinyalin son geldiği saat
                        mtag.LastSignalDate = DateTime.Now.ToString();
                    }
                    else // Hazırlık Sürecinden çıktı
                    {
                        totalSeconds -= startupTime;

                        var now = DateTime.Now;
                        var lastSignalDate = Convert.ToDateTime(mtag.LastSignalDate);
                        var timeOut = Convert.ToInt32(mtag.TimeOut);
                        var lastAmount = Convert.ToInt32(mtag.LastAmount);


                        if ((now - lastSignalDate).TotalSeconds > timeOut && !mtag.StopContinue) //son sinyal saatine göre timeOut süresini geçmiş ve herhangi bir duruş başlamaışsa duruşu başlat.
                        {
                            mtag.Status = "9"; // Timemout bittiği için  duruşu başlat
                            mtag.LastSignalDate = DateTime.Now.ToString();
                        }

                        if (Convert.ToDouble(mtag.LeftWarpMeter) <= 0 && !mtag.StopContinue && (!mtag.ProductionContinue&&!mtag.Sample))
                        {
                            mtag.LeftWarpMeter = "0";
                            mtag.StopContinue = true;
                            mtag.Status = "21";

                        }

                        // Makine durmuyorsa veya üretim yoksa duruş kontrolü yap.
                        if (mtag.Status != "0" && mtag.Status != "1")
                        {
                            int totalStopDuration = Convert.ToInt32((now - Convert.ToDateTime(mtag.LastSignalDate)).TotalSeconds);

                            //Toplam duruş süresi
                            mtag.TotalStopDuration = totalStopDuration.ToString();

                            // Planlı duruşlarda geçen süre
                            if (mtag.Status == "7" || mtag.Status == "8")
                            {
                                mtag.RealTimeTotalPlannedStop = (totalStopDuration + Convert.ToInt32(mtag.PlannedStopDuration)).ToString();
                            }
                            // Plansız duruşlarda geçen süre
                            else if (mtag.Status == "3" || mtag.Status == "4" || mtag.Status == "5" || mtag.Status == "6" || mtag.Status == "9" || mtag.Status == "10" || mtag.Status == "21")
                            {
                                // Top kesiminde geçen süre
                                if (mtag.Status == "10")
                                {
                                    mtag.BoltCutDurationTime = (totalStopDuration + Convert.ToInt32(mtag.BoltCutDurationTime)).ToString();
                                }
                                // Top kesiminde yapılan duruşuda plansız olarak ekliyoruz!
                                mtag.RealTimeTotalUnPlannedStop = (totalStopDuration + Convert.ToInt32(mtag.UnPlannedStopDuration)).ToString();
                            }
                        }
                    }
                    #endregion


                    if (totalSeconds == 0)
                    {
                        mtag.Productivity = "0";
                        mtag.Efficiency = "0";
                        mtag.Availability = "0";
                        mtag.OEE = "0";
                        mtag.RealTimeTargetAmount = "0";
                    }
                    else
                    {
                        //Üretilen metre
                        var meter = Convert.ToDecimal(mtag.ProducedMeter);
                        //Vardiyanın toplam saniyesi
                        var shiftTotalSeconds = Convert.ToDecimal((Convert.ToDateTime(mtag.ShiftEndDate) - Convert.ToDateTime(mtag.ShiftStartDate)).TotalSeconds);
                        if (shiftTotalSeconds < 0)
                        {
                            mtag.ShiftEndDate = Convert.ToDateTime(mtag.ShiftEndDate).AddDays(1).ToString();
                            shiftTotalSeconds = Convert.ToDecimal((Convert.ToDateTime(mtag.ShiftEndDate) - Convert.ToDateTime(mtag.ShiftStartDate)).TotalSeconds);
                        }

                        //Devir hızına göre atkı hesabı
                        //var weftAmount = Convert.ToDouble(mtag.Rpm) / 60;  

                        // RPM 'e göre saniyede atılan atkı sayısı
                        var weftAmountBySecond = Convert.ToDecimal(mtag.Rpm) / 60;

                        //var stopByWeftAmount = Convert.ToInt32(mtag.RealTimeTotalUnPlannedStop) * weftAmount;
                        //Anlık Üretilmesi Gereken Atkı
                        var targetWeftAmount = Math.Round(weftAmountBySecond * (totalSeconds - Convert.ToDecimal(mtag.RealTimeTotalPlannedStop)), 2);
                        //Vardiyalık Üretilmesi Gereken Atkı ve Metre
                        var targetShiftWeftAmount = Math.Round(weftAmountBySecond * shiftTotalSeconds, 2);

                        // Geçen süreye göre top kestikten sonra atılması gereken atkı sayısı
                        var targetWeftAmountCutBolt = Math.Round(weftAmountBySecond * (totalSeconds - Convert.ToDecimal(mtag.BoltCutDurationTime) - Convert.ToDecimal(mtag.RealTimeTotalPlannedStop)), 2);


                        decimal targetShiftMeter = 0, targetMeterCutBolt = 0, targetMeter = 0;

                        // Numune testi varsa hesaplama yapılmayacak alanlar.
                        // NOT: Hesaplama yapılamama sebebi atkı sıklığını bilmediğimizden kaynaklı.
                        if (!mtag.Sample)
                        {
                            // İş emri başlatılmadan üretim başladıysa açılan iş emrinde hesaplama yapılmayacak alanlar.
                            if (!mtag.ProductionContinue)
                            {
                                var weftDensity = Convert.ToDecimal(mtag.WeftDensity);
                                //Anlık Üretilmesi Gereken Metre
                                targetMeter = Math.Round(targetWeftAmount / (weftDensity * 100), 2);
                                targetShiftMeter = Math.Round(targetShiftWeftAmount / (weftDensity * 100), 2);
                                targetMeterCutBolt = Math.Round(targetWeftAmountCutBolt / (weftDensity * 100), 2);
                            }
                           
                        }

                        //Anlık Üretilen Metreye göre proggress bar durumu mongoya at
                        mtag.ShiftTargetMeter = targetShiftMeter.ToString(CultureInfo.CurrentCulture);


                        //Anlık Üretim Top Kesimi
                        //mtag.WeftAmountByBolt = Convert.ToInt32((Convert.ToDouble(mtag.WeftAmount) - Convert.ToDouble(mtag.TotalBoltWeftAmount))).ToString();
                        //mtag.ProducedMeterByBolt = Math.Round((Convert.ToDouble(mtag.ProducedMeter) - Convert.ToDouble(mtag.TotalBoltMeter)), 2).ToString();

                        if (mtag.TotalBoltTargetMeter == "0")
                        {
                            mtag.BoltTargetMeter = Math.Round(targetMeterCutBolt, 2).ToString(CultureInfo.CurrentCulture);
                        }
                        else
                        {
                            mtag.BoltTargetMeter = Math.Round((targetMeter - Convert.ToDecimal(mtag.TotalBoltTargetMeter)), 2).ToString(CultureInfo.CurrentCulture);
                        }

                        // Top kesildikten sonraki progressbar için yapılan hesaplama
                        mtag.TotalBoltProgressBar = Convert.ToInt32(Convert.ToDouble(mtag.ProducedMeterByBolt) * 100 / Convert.ToDouble(mtag.BoltTargetMeter == "0" ? "1" : mtag.BoltTargetMeter)).ToString();


                        //Vardiyalık üretilecek metre
                        //var shiftTargetMeter = (shiftTotalSeconds * weftAmount) / (weftDensity * 100); 

                        //Vardiyalık hedef metre mongoya at
                        //mtag.TargetAmount = targetShiftMeter.ToString();

                        //Vardiyalık Üretilen Metreye göre proggress bar durumu mongoya at
                        //mtag.ProductionProgressBar = Convert.ToInt32(meter * 100 / (targetShiftMeter == 0 ? 1 : targetShiftMeter)).ToString(); 26.12.2019

                        //Anlık Üretilen Metreyi mongoya at


                        //mtag.RealTimeTargetAmount = targetMeter.ToString();
                        //mtag.RealTimeTargetAmount =(Convert.ToDouble(mtag.TargetAmount)-meter).ToString(); 
                        // realTimeTargetAmount'a kalan metre bilgisi atıldı

                        // İş emrinde üretilen metre ve atanan hedef metreye göre progressbar ilerlemesi
                        mtag.RealTimeProductionProgressBar = Convert.ToInt32(Convert.ToDecimal(mtag.RealTimeTargetAmount) * 100 / (Convert.ToDecimal(mtag.TargetAmount) == 0 ? 1 : Convert.ToDecimal(mtag.TargetAmount))).ToString();


                        var plannedHalt = Convert.ToInt32(mtag.RealTimeTotalPlannedStop);
                        var unPlannedHalt = Convert.ToInt32(mtag.RealTimeTotalUnPlannedStop);

                        // Üretkenlik Hesabı
                        //var productivity = Math.Round((totalSeconds - plannedHalt - unPlannedHalt) * 100 / totalSeconds, 2);
                        //var productivity = Math.Round((totalSeconds) * 100 / totalSeconds, 2);
                        //mtag.Productivity = productivity.ToString();

                        var productivity = Math.Round((Convert.ToInt32(mtag.WorkOrderShiftWeftAmount) / (totalSeconds* weftAmountBySecond))*100, 2);
                        mtag.Productivity = productivity.ToString(CultureInfo.CurrentCulture);

                        // Vardiyada üretilen metre
                        var totalShiftMeter = Convert.ToDecimal(mtag.TotalShiftMeter);

                        // Verimlilik Hesabı
                        //var efficiency = Math.Round(totalShiftMeter * 100 / (targetMeter == 0 ? 1 : targetMeter), 2);
                        //mtag.Efficiency = efficiency.ToString();

                        var efficiency = Math.Round((Convert.ToInt32(mtag.WorkOrderShiftWeftAmount) / ((totalSeconds-plannedHalt) * weftAmountBySecond)) * 100, 2);
                        mtag.Efficiency = efficiency.ToString(CultureInfo.CurrentCulture);


                        //var availability = Math.Round((totalSeconds - plannedHalt - unPlannedHalt) / (totalSeconds - plannedHalt) * 100, 2);

                        // Kalitesizlik Kırayteks te olmadığı için 1 olarak alındı.
                        //var availability = 1;

                        //mtag.Availability = availability.ToString();

                        //var discard = 0;
                        //var quality = (amount - discard) / amount * 100;



                        //var oee = Math.Round(productivity * efficiency * availability / 100, 2); Bodoplast için geçerli kod
                        var oee = Math.Round(productivity * efficiency / 100, 2);

                        mtag.OEE = oee.ToString(CultureInfo.CurrentCulture);
                    }

                    #endregion

                    #region Controls
                    //var shiftEndDate = Convert.ToDateTime(mtag.ShiftEndDate);
                    //var shiftStartDate = Convert.ToDateTime(mtag.ShiftStartDate);
                    //var datetimeNow = DateTime.Now;
                    //var nowHour = datetimeNow.Hour;
                    //var nowMinute = datetimeNow.Minute;
                    //var nowSecond = datetimeNow.Second;
                    if (mtag.Status == "1") //LeftWarpMeter=0 ise çözgüyü değiştirene kadar duruştan çıkma eklenecek
                    {

                        // Duruş varken sinyal geldiyse duruşu bitir kontorlü

                        if (stopControl)
                        {
                            stopControl = false;
                            await EndStop(mtag).ContinueWith(task => {
                                if (task.IsCompleted)
                                {
                                    stopControl = true;
                                    mtag = task.Result;
                                }
                            });
                        }
                        //mtag =await EndStop(mtag);

                        // Vardiya kontrolü
                        if (ShiftControl(mtag))
                        {
                            // Vardiya geçişi sırasında asenkron çalıştığı için tekrar girmesini engellemek adına static bool değer tanımlandı.
                            if (shiftTransitionControl)
                            {
                                // Vardiya bitir yapılana kadar gelen thread 'lerin tekrar bitir metoduna girmemesi için false yapıldı
                                shiftTransitionControl = false;
                                // Vardiya bitir
                                await EndShift(mtag).ContinueWith(task => {
                                    if (task.IsCompleted)
                                    {
                                        shiftTransitionControl = true;
                                        mtag = task.Result;
                                    }
                                });
                            }
                        }
                        mtag.StopContinue = false;
                    }
                    else
                    {
                        // Makinede üretim durduğun bu alana girmekte.
                        // Duruş varsa mtag aynı gelir duruş yoksa duruş başlatır ve mtag güncellenir.
                        if (stopControl)
                        {
                            stopControl = false;
                            await StartStop(mtag).ContinueWith(task => {
                                if (task.IsCompleted)
                                {
                                    stopControl = true;
                                    mtag = task.Result;
                                }
                            });
                        }

                        //mtag =await StartStop(mtag);

                        // Vardiya kontrolü adımları
                        if (ShiftControl(mtag))
                        {
                            //Vardiya geçişi varsa mongodayı bir alanı true yap. Client tarafında logine yönelndir. Login girişi yapıldığı zaman deviceGroup any metodunda tekrar false olarak güncelle
                            if (stopControl)
                            {
                                stopControl = false;
                                await EndStop(mtag).ContinueWith(task => {
                                    if (task.IsCompleted)
                                    {
                                        stopControl = true;
                                        mtag = task.Result;
                                    }
                                });
                            }
                            //mtag =await EndStop(mtag);
                            if (shiftTransitionControl)
                            {
                                shiftTransitionControl = false;
                                await EndShift(mtag).ContinueWith(task => {
                                    if (task.IsCompleted)
                                    {
                                        shiftTransitionControl = true;
                                        mtag = task.Result;
                                    }
                                });
                            }
                        }
                        //if (shiftEndDate.Hour <= nowHour &&
                        //    shiftStartDate.Hour <= nowHour &&
                        //    shiftEndDate.Hour > shiftStartDate.Hour &&
                        //    shiftEndDate.Minute <= nowMinute &&
                        //    shiftEndDate.Second <= nowSecond)
                        //{
                        //    mtag = await EndStop(mtag);
                        //    mtag = await EndShift(mtag);

                        //}
                        ////Vardiya endate'i gece yarısından sonrasına sarkıyorsa 
                        //else if (shiftEndDate.Hour <= nowHour &&
                        //         shiftStartDate.Hour > nowHour &&
                        //    shiftEndDate.Minute <= nowMinute &&
                        //    shiftEndDate.Second <= nowSecond)// EndDate gece yarısondan sonrasına sarkıyorsa
                        //{
                        //    mtag = await EndStop(mtag);
                        //    mtag = await EndShift(mtag);
                        //}


                    }
                    #endregion

                }

                return mtag;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static bool ShiftControl(MTag mtag) // Vardiya Kontrolü
        {
            var shiftEndDate = Convert.ToDateTime(mtag.ShiftEndDate);
            var shiftStartDate = Convert.ToDateTime(mtag.ShiftStartDate);
            var datetimeNow = DateTime.Now;
            var nowHour = datetimeNow.Hour;
            var nowMinute = datetimeNow.Minute;
            var nowSecond = datetimeNow.Second;

            if (shiftEndDate.Hour <= nowHour &&
                    shiftStartDate.Hour <= nowHour &&
                    shiftEndDate.Hour > shiftStartDate.Hour &&
                    shiftEndDate.Minute <= nowMinute &&
                    shiftEndDate.Second <= nowSecond)
            {
                return true;
            }
            //Vardiya endate'i gece yarısından sonrasına sarkıyorsa 
            else if (shiftEndDate.Hour <= nowHour &&
                     shiftStartDate.Hour > nowHour &&
                shiftEndDate.Minute <= nowMinute &&
                shiftEndDate.Second <= nowSecond)
            {
                return true;
            }

            return false;
        }

        private static async Task<MTag> EndStop(MTag mtag) // Duruşu Bitir
        {
            if (mtag.StopContinue)
            {
                //Duruşu bitir.
                ResponseData endStopData =await EndStopAsync(mtag);
                if (endStopData.IsSucceed)
                {
                    mtag = (MTag)endStopData.Data;
                }
            }

            return mtag;
        }

        private static async Task<MTag> StartStop(MTag mtag) // Duruşu Başlat
        {
            if (!mtag.StopContinue)
            {
                //Duruşu Başlat
                ResponseData stopData =await StartStopAsync(mtag);
                if (stopData.IsSucceed)
                {
                    mtag.TotalStopAmount = (Convert.ToInt32(mtag.TotalStopAmount) + 1).ToString();
                }
                mtag = (MTag)stopData.Data;
            }

            return mtag;
        }


        private static async Task<MTag> EndShift(MTag mtag)
        {
            var response = await FinishDailyWorkOrder(mtag);
            if (response.IsSucceed)
            {
                mtag.TotalStopAmount = "0";
                var mongoTag = response.Data.ToString();
                mtag = JsonConvert.DeserializeObject<MTag>(mongoTag);
                //mtag = (MTag)response.Data;
                //mtag = DefaultTagValue(mtag);
                //mtag.Status = "0";


                //mtag = await StartShift(mtag); 23.12.2019
            }

            return mtag;
        } //Vardiya Bitir

        private static async Task<MTag> StartShift(MTag mtag)
        {
            var response = await StartDailyWorkOrder(mtag);
            if (response.IsSucceed)
            {
                var mongoTag = response.Data.ToString();
                mtag = JsonConvert.DeserializeObject<MTag>(mongoTag);
                //mtag = (MTag)response.Data;
                //mtag.ShiftStartDate = dailyTeamWorkOrder.StartDate.ToString();
                //mtag.ShiftEndDate = dailyTeamWorkOrder.EndDate.ToString();
                //mtag.DailyTeamId = dailyTeamWorkOrder.DailyTeamId.ToString();
                //mtag.DailyTeamWorkOrderId = dailyTeamWorkOrder.Id.ToString();
                //mtag.TotalShiftWeftAmount = "0";
                //mtag.TotalShiftMeter = "0";
                //mtag.ShiftTargetMeter = "0";

                //mtag.DailyTeamId = dailyTeamWorkOrder.DailyTeamId.ToString();


                mtag.Status = "1";
            }

            return mtag;
        } //Vardiya Başlat

        private static async Task<ResponseData>  EndStopAsync(MTag mtag)
        {
            ResponseData data = new ResponseData();
            if (mtag == null)
            {
                data.IsSucceed = false;
                return data;
            }

            int tagId = Convert.ToInt32(mtag.Id);
            DailyTeamStopRequest request = new DailyTeamStopRequest
            {
                Type = Enums.RequestTypes.Update,
                Id = Convert.ToInt32(mtag.DaliyTeamStopId),
                DailyTeamStop = new DailyTeamStopDto
                {
                    IsManuel = false,
                }

            };
            using (var client = new HttpClient())
            {

                string jsonBody = JsonConvert.SerializeObject(request);

                var httpResponse =await client.PostAsync(ApiUrl + "api/DailyTeamStop/", new StringContent(jsonBody,
                                        Encoding.UTF8,
                                        "application/json"));

                var jsonString =await httpResponse.Content.ReadAsStringAsync();
                data = JsonConvert.DeserializeObject<ResponseData>(jsonString);
                if (data.IsSucceed)
                {
                    mtag.StopContinue = false;
                    mtag.Status = "1";
                    data.Data = mtag;
                    return data;
                }
                data.IsSucceed = false;
                return data;
            }
        }

        private static async Task<ResponseData> StartStopAsync(MTag mtag)
        {
            ResponseData data = new ResponseData();
            if (mtag == null)
            {
                data.IsSucceed = false;
                return data;
            }

            using (var client = new HttpClient())
            {
                DailyTeamStopDto dto = new DailyTeamStopDto
                {
                    DailyTeamId = Convert.ToInt32(mtag.DailyTeamId),
                    StopId = Convert.ToInt32(mtag.Status),
                    TagId = Convert.ToInt32(mtag.Id),
                    IsManuel = false
                };
                DailyTeamStopRequest request = new DailyTeamStopRequest
                {
                    Type = Enums.RequestTypes.Create,
                    DailyTeamStop = dto
                };

                string jsonBody = JsonConvert.SerializeObject(request);

                var httpResponse =await client.PostAsync(ApiUrl + "api/DailyTeamStop/", new StringContent(jsonBody,
                                        Encoding.UTF8,
                                        "application/json"));

                var jsonString =await httpResponse.Content.ReadAsStringAsync();
                data = JsonConvert.DeserializeObject<ResponseData>(jsonString);
                if (data.IsSucceed)
                {

                    DailyTeamStop dailyTeamStop = JsonConvert.DeserializeObject<DailyTeamStop>(data.Data.ToString());
                    var mtagValue = Convert.ToDecimal(mtag.Value);
                    data.Id = dailyTeamStop.Id;
                    data.IsSucceed = true;
                    mtag.StopContinue = true;
                    mtag.DaliyTeamStopId = dailyTeamStop.Id.ToString();
                    mtag.Status = dailyTeamStop.StopId.ToString();
                    data.Data = mtag;
                }
                else
                {
                    data.IsSucceed = false;
                }
                return data;
            }
        }
    }
}
