﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using IOT.Api.Models;
using IOT.Signals.Hubs;
using IOT.Signals.Models;
using IOT.Signals.Models.Abstract;
using IOT.Signals.Models.MongoDB.Concrete;
using IOT.Signals.Models.Request;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace IOT.Signals.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SignalController : ControllerBase
    {
        // GET api/values
        ISignal<AdvantechResponseModel> _signal;
        private IHubContext<SignalHub> _hubContext;
        public static ITagRepository<MTag> _deviceContext;
        public static bool workOrderControl = true;

        public SignalController(ISignal<AdvantechResponseModel> signal,
           IHubContext<SignalHub> hubContext,
           ITagRepository<MTag> deviceContext)
        {
            _signal = signal;
            _hubContext = hubContext;
            _deviceContext = deviceContext;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            AdvantechResponseModel model = await _signal.Get();
            if (model.SignalDatas.Count > 0)
            {
                await Post(model);
            }
            return Ok(model);
        }

        [HttpPost]
        public async Task Post([FromBody] AdvantechResponseModel model)
        {

            try
            {
                // Operator ekranları makine bazlı websocket
                foreach (var item in model.SignalDatas)
                {
                    var data = model.SignalDatas.SingleOrDefault(x => x.DeviceName == item.DeviceName);
                    await _hubContext.Clients.All.SendAsync(item.DeviceName, data.Tags);
                }
                // Operator ekranları makine grubu websocket
                foreach (var item in model.SignalDataDeviceGroups)
                {

                    var data = model.SignalDataDeviceGroups.SingleOrDefault(x => x.DeviceGroupName == item.DeviceGroupName);
                    data.Tags.OrderBy(x => x.DeviceId);
                    await _hubContext.Clients.All.SendAsync(item.DeviceGroupName, model.SignalDataDeviceGroups);
                }
                // Tüm makineleri göstermek için websocket
                // Andon ekranlar için (Özelleştirilebilir)
                await _hubContext.Clients.All.SendAsync("all", model.SignalDatas);
            }
            catch (Exception ex)
            {

            }
        }


        [HttpPost]
        [Route("addDevice")]
        public async Task<bool> AddDevices([FromBody] MTag device)
        {
            ResponseData response = await _deviceContext.Add(device);
            return response.IsSucceed;
        }


        [HttpPost("deleteDevice")]
        public async Task<bool> DeleteDevice([FromBody] string deviceId)
        {
            ResponseData response = await _deviceContext.Remove(deviceId);
            return response.IsSucceed;
        }


        [HttpPost("updateDevice")]
        public async Task<bool> UpdateDevice([FromBody] MTag device)
        {
            ResponseData response = await _deviceContext.Update(device.Id, device);
            return response.IsSucceed;
        }


        [HttpGet("getDevice/{id}")]
        public async Task<MTag> GetDevice([FromRoute] string id)
        {
            ResponseData response = await _deviceContext.Get(id);
            return (MTag)response.Data;
        }


        [HttpGet("getDevices")]
        public async Task<IEnumerable<MTag>> GetDevices()
        {
            ResponseData response = _deviceContext.GetList();
            return (List<MTag>)response.Data;
        }


        [HttpPost("deleteDevices")]
        public async Task<bool> DeleteDevicesAsync()
        {
            ResponseData response = await _deviceContext.RemoveAll();
            return response.IsSucceed;
        }


        [HttpPost("reset/{deviceName}/{tagId}")]
        public async Task<bool> Reset([FromRoute]string deviceName, [FromRoute]string tagId)
        {
            try
            {
                ResponseData getResponse = await _deviceContext.Get(tagId);
                if (getResponse.IsSucceed)
                {
                    MTag mTag = (MTag)getResponse.Data;
                    mTag = Api.Api.DefaultTagValue(mTag);
                    ResponseData updateResponse = await _deviceContext.Update(tagId, mTag);
                    if (updateResponse.IsSucceed)
                    {
                        if (mTag.SignalFromWhere == "1")
                        {
                            return await _signal.ResetAdvantech(deviceName);
                        }
                        else
                        {
                            return updateResponse.IsSucceed;
                        }
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                return false;
            }
        }


        [HttpPost("fromRasberypi")]
        public async Task<bool> FromRasberypi([FromBody] MTag device)
        {
            try
            {
                /*
                 * Display tagındaki tanımlamalar client tarafındaki gösterim için kullanılmakta hesaplamalara dahil edilmeyecek.
                 * Hassas hesaplamalar için ayrı tanımlamalar mevcut
                 * Kırayteks tarafında veri girişleri virgül olarak yapılmakta
                 */


                CultureInfo cultures = new CultureInfo("tr-TR");
                ResponseData getResponse = await _deviceContext.Get(device.Id);
                MTag getMtag = (MTag)getResponse.Data;

                getMtag.Status = device.Status == "2" ? "9" : device.Status;
                getMtag.Cycle = device.Cycle;
                getMtag.Rpm = device.Value2;
                decimal rpm = Convert.ToInt32(device.Value2);


                //if (getMtag.Status == "1" && getMtag.WorkOrderId!="0")
                if (getMtag.Status == "1" && getMtag.DailyTeamWorkOrderId != "0")
                {
                    decimal realWeft = Convert.ToDecimal(device.Value);

                    var secondWeftAmount = rpm / 60;

                    getMtag.Value = (Convert.ToInt32(getMtag.Value) + Convert.ToInt32(device.Value)).ToString();
                 
                    getMtag.WarpAmount = device.WarpAmount;
                    getMtag.WeftDensity = getMtag.WeftDensity;
                    getMtag.WeftAmount = getMtag.Value;



                    getMtag.TotalShiftWeftAmount = (Convert.ToInt32(getMtag.TotalShiftWeftAmount) + Convert.ToInt32(device.Value)).ToString();
                    getMtag.WorkOrderShiftWeftAmount = (Convert.ToInt32(getMtag.WorkOrderShiftWeftAmount) + Convert.ToInt32(device.Value)).ToString();
                    getMtag.WeftAmountByBolt = Convert.ToInt32((Convert.ToDecimal(getMtag.WeftAmount) - Convert.ToDecimal(getMtag.TotalBoltWeftAmount))).ToString();

                 
                    getMtag.LastSignalDate = DateTime.Now.ToString();

                    if (getMtag.IsStartUp)
                    {
                        getMtag.NewStartupTime = (DateTime.Now - Convert.ToDateTime(getMtag.StartDate)).TotalSeconds.ToString();
                        getMtag.IsStartUp = false;
                    }
                    decimal realMeter = 0, meter = 0;
                    var leftWarpMeter = Convert.ToDecimal(getMtag.LeftWarpMeter.Replace(".", ","), cultures);


                    getMtag.WarpUsedWeftAmount = (Convert.ToInt32(getMtag.WarpUsedWeftAmount) + Convert.ToInt32(device.Value)).ToString();

                    // Numune testi veya İş emri yokken üretimin başlaması durumlarında yapılmaması gereken hesaplamalar
                    if (!getMtag.Sample)
                    {
                        if (!getMtag.ProductionContinue)
                        {
                            
                            getMtag.WarpUsedMeter = Math.Round(Convert.ToDecimal(Convert.ToInt32(getMtag.WarpUsedWeftAmount) / (Convert.ToDecimal(getMtag.WeftDensity) * 100)),2).ToString();
                            realMeter = Math.Round(realWeft / (Convert.ToDecimal(getMtag.WeftDensity) * 100), 9);
                            meter = Math.Round(Convert.ToDecimal(getMtag.WeftAmount, cultures) / (Convert.ToDecimal(getMtag.WeftDensity, cultures) * 100), 7);
                            getMtag.ProducedMeter = meter.ToString();
                            getMtag.TotalShiftMeter = Math.Round(Convert.ToDecimal(getMtag.TotalShiftWeftAmount) / (Convert.ToDecimal(getMtag.WeftDensity, cultures) * 100), 2).ToString();
                            getMtag.WorkOrderShiftMeter = Math.Round(Convert.ToDecimal(getMtag.WorkOrderShiftWeftAmount) / (Convert.ToDecimal(getMtag.WeftDensity, cultures) * 100), 2).ToString();
                            getMtag.ProductionProgressBar = Convert.ToInt32(meter * 100 / (Convert.ToDecimal(getMtag.TargetAmount) == 0 ? 1 : Convert.ToDecimal(getMtag.TargetAmount))).ToString();
                            getMtag.ProducedMeterByBolt = Math.Round((Convert.ToDecimal(getMtag.ProducedMeter, cultures) - Convert.ToDecimal(getMtag.TotalBoltMeter, cultures)), 2).ToString();

                            //getMtag.WeftAmountByBolt = Convert.ToInt32((Convert.ToDecimal(getMtag.WorkOrderShiftWeftAmount) - Convert.ToDecimal(getMtag.TotalBoltWeftAmount))).ToString();
                            getMtag.RealTimeTargetAmount = (Convert.ToDecimal(getMtag.TargetAmount, cultures) - meter).ToString();
                            getMtag.LeftWarpMeter = (leftWarpMeter - realMeter).ToString();
                        }
                    }
                    // İş emri bağlama yapıldığında üretim varken atılan atkı sayısını tutup çözgü metre hesaplaması yapabilmek için var.
                    if (getMtag.WarpId == "0")
                    {
                        getMtag.NotWarpWeftAmount = (Convert.ToDecimal(getMtag.NotWarpWeftAmount) + Convert.ToDecimal(device.Value)).ToString();
                    }
                    else
                    {
                        if (Convert.ToDecimal(getMtag.WeftDensity) == 0)
                        {
                            getMtag.NotWarpWeftAmount = (Convert.ToDecimal(getMtag.NotWarpWeftAmount) + Convert.ToDecimal(device.Value)).ToString();
                        }
                        else
                        {
                            decimal notWarpMeter = Convert.ToDecimal(getMtag.NotWarpWeftAmount) / (Convert.ToDecimal(getMtag.WeftDensity) * 100);
                            getMtag.LeftWarpMeter = (Convert.ToDecimal(getMtag.LeftWarpMeter) - notWarpMeter).ToString();
                            getMtag.NotWarpWeftAmount = "0";
                        }

                    }

                    //getMtag.WeftAmount = (Convert.ToInt32(getMtag.WeftAmount) + (rpm / 60)).ToString();



                    getMtag.DisplayProducedMeter = Math.Round(Convert.ToDecimal(getMtag.ProducedMeter, cultures), 2).ToString();


                    getMtag.DisplayRealTimeTargetAmount = Math.Round(Convert.ToDecimal(getMtag.RealTimeTargetAmount, cultures), 2).ToString();

                    if (getMtag.WarpMeter != "0")
                    {
                        getMtag.WarpProgressBar = (Convert.ToInt32(leftWarpMeter * 100 / Convert.ToDecimal(getMtag.WarpMeter.Replace(".", ","), cultures))).ToString();
                    }

                    getMtag.DisplayLeftWarpMeter = Math.Round(Convert.ToDecimal(getMtag.LeftWarpMeter, cultures), 2).ToString();


                }

                // İş emri yokken üretim başladıysa iş emri aç
                else if (getMtag.Status == "1" && getMtag.WorkOrderId == "0" && getMtag.DailyTeamWorkOrderId == "0" && rpm > 0 && getMtag.WeftAmount == "0")
                {
                    if (workOrderControl)
                    {
                        workOrderControl = false;
                        ResponseData response = _signal.CreateWorkOrder(getMtag);
                        if (response.IsSucceed)
                        {
                            getMtag = (MTag)response.Data;
                            getMtag.ProductionContinue = true;
                            workOrderControl = true;

                        }
                        if (getMtag.WeftDensity != "0") //atkı sıklığı varsa yapılacak hesaplamalar
                        {

                        }
                        else //yoksa yapılacak hesaplamalar
                        {

                        }
                    }
                }
                //getMtag.StopContinue = false;
                ResponseData updateResponse = await _deviceContext.Update(device.Id, getMtag);
                return updateResponse.IsSucceed;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        [HttpPost("Warp")]
        public async Task<bool> SetWarp([FromBody] WarpRequest request)
        {
            try
            {
                ResponseData getResponse = await _deviceContext.Get(request.Id.ToString());
                MTag getMtag = (MTag)getResponse.Data;
                if (request.RequestType)// warpControl true ise çözgü ekle
                {
                    getMtag.WarpNo = request.Warp.No;
                    getMtag.WarpMeter = request.Warp.Meter;
                    getMtag.WarpAmount = request.Warp.Meter;
                    getMtag.LeftWarpMeter = request.Warp.Meter;
                    getMtag.DisplayLeftWarpMeter = request.Warp.Meter;

                }
                else // warpControl false ise çözgü çıkar
                {
                    getMtag.WarpNo = "0";
                    getMtag.WarpMeter = "0";
                    getMtag.WarpAmount = "0";
                    getMtag.LeftWarpMeter = "0";
                    getMtag.WarpProgressBar = "0";
                    getMtag.DisplayLeftWarpMeter = "0";
                }





                //getMtag.StopContinue = false;
                ResponseData updateResponse = await _deviceContext.Update(request.Id.ToString(), getMtag);
                return updateResponse.IsSucceed;
            }
            catch (Exception ex)
            {
                return false;
            }
        } //Çözgü api tarafına taşınacak veritabanı oluşturulacak



        [HttpGet("Reset/{id}")]
        public async Task<IActionResult> ResetMongoDevice([FromRoute] string id)
        {
            ResponseData getResponse = await _deviceContext.Get(id);
            if (getResponse.IsSucceed)
            {
                MTag mTag = (MTag)getResponse.Data;
                mTag = Api.Api.Reset(mTag);
                ResponseData updateResponse = await _deviceContext.Update(id, mTag);
                if (updateResponse.IsSucceed)
                {
                    if (mTag.SignalFromWhere == "1")
                    {
                        string deviceName = mTag.Name.Substring(0, mTag.Name.IndexOf("_")).ToString();
                        if (await _signal.ResetAdvantech(deviceName))
                        {
                            return Ok("Reset Atıldı.");
                        }
                        else
                        {
                            return Ok("Reset İşlemi Başarısız!");

                        }
                    }
                    else
                    {
                        return Ok("Reset Atıldı.");
                    }
                }
                else
                {
                    return Ok("Reset İşlemi Başarısız!");
                }
            }
            else
            {
                return Ok("Veriler Okunamadı!");
            }
        }
    }
}
