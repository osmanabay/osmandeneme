﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Signals.Hubs
{
    public interface ITypedHubClient
    {
        Task BroadcastSignal(string type, string payload);
    }
}
