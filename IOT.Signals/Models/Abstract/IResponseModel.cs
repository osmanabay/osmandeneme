﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Signals.Models.Abstract
{
    public interface IResponseModel
    {
        string Message { get; set; }
    }
}
