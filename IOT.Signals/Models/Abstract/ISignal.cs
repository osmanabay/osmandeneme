﻿using IOT.Signals.Models.Concrete;
using IOT.Signals.Models.MongoDB.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Signals.Models.Abstract
{
    public interface ISignal<T> where T:IResponseModel
    {
        Task<T> Get();
        Task<bool> ResetAdvantech(string deviceName);
        ResponseData CreateWorkOrder(MTag mTag);
    }
}
