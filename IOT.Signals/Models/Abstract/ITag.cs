﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Signals.Models.Abstract
{
    public interface ITag
    {
        string Id { get; set; }
        string Name { get; set; }
        string Value { get; set; }
    }
}
