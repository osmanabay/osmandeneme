﻿using IOT.Signals.Models.MongoDB.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Signals.Models.Abstract
{
    public interface ITagRepository<T> where T:ITag
    {
        ResponseData GetList();
        Task<ResponseData> Get(string id);
        
        Task<ResponseData> Add(T device);
        Task<ResponseData> Update(string id, T device);
        Task<ResponseData> Remove(string id);
        Task<ResponseData> RemoveAll();
    }
}
