﻿using IOT.Signals.Models.Abstract;
using IOT.Signals.Models.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Signals.Models
{
    public class AdvantechResponseModel:ResponseModel
    {
        public ASResult Result { get; set; }
        public List<AdvantechTag> Values { get; set; }
       
    }

    public class ASResult
    {
        public decimal Ret { get; set; }
        public decimal Total { get; set; }
    }

   
}
