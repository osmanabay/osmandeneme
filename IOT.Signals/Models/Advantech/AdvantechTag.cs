﻿using IOT.Signals.Models.Abstract;

namespace IOT.Signals.Models.Concrete
{
    public class AdvantechTag : ITag
    {
        public AdvantechTag(string id, string type, string tagName, string value,  string workOrderNo, string workOrderId, string status, string dailyTeamId, string startDate, string cycleTime,decimal quality = 0)
        {
            TypeId = type;
            Id = id;
            Name = tagName;
            Value = value;
            Quality = quality;
            WorkOrderNo = workOrderNo;
            WorkOrderId = workOrderId;
            Status = status;
            DailyTeamId = dailyTeamId;
            StartDate = startDate;
            CycleTime = cycleTime;
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string TypeId { get; set; }
        public decimal Quality { get; set; }
        public string WorkOrderNo { get; set; }
        public string WorkOrderId { get; set; }
        public string Status { get; set; }
        public string DailyTeamId { get; set; }
        public string StartDate { get; set; }
        public string CycleTime { get; set; }

    }
}
