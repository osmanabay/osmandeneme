﻿using IOT.Signals.Models;
using IOT.Signals.Models.Concrete;
using IOT.Signals.Models.MongoDB.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Api.Models
{
    public class SignalData
    {
        public string DeviceName { get; set; }
        public string DeviceGroupName { get; set; }

        public List<MTag> Tags { get; set; }
    }

   

}
