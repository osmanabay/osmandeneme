﻿using IOT.Entities.ComplexTypes;
using IOT.Entities.ComplexTypes.Request;
using IOT.Signals.Models.Abstract;
using IOT.Signals.Models.MongoDB.Concrete;
using IOT.Signals.Models.MongoDB.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace IOT.Signals.Models.MongoDB.Repository
{
    public class TagRepository : ITagRepository<MTag>
    {
        private readonly ObjectContext _context = null;


        public TagRepository(IOptions<Settings> settings)
        {
            _context = new ObjectContext(settings);
        }

        public async Task<ResponseData> Add(MTag device)
        {
          
            ResponseData response = new ResponseData();
            try
            {
                await _context.Tags.InsertOneAsync(device);
                response.IsSucceed = true;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSucceed = false;
                response.Message = ex.InnerException == null ? ex.Message.ToString() : ex.InnerException.Message.ToString();
                return response;

            }
        }

        public async Task<ResponseData> Get(string id)
        {
            ResponseData response = new ResponseData();
            try
            {
                var device = Builders<MTag>.Filter.Eq("Id", id);
                var tag = await _context.Tags.Find(device).FirstOrDefaultAsync();
                response.Data = tag;
                response.IsSucceed = true;
                return response;
            }
            catch (Exception ex)
            {
                response.IsSucceed = false;
                response.Message = ex.InnerException == null ? ex.Message.ToString() : ex.InnerException.Message.ToString();
                return response;

            }
        }

        public ResponseData GetList()
        {
            ResponseData response = new ResponseData();

            try
            {
                IEnumerable<MTag> mlist =  _context.Tags.Find(x => true).ToList();
                response.Data = mlist;
                response.IsSucceed = true;
                return response;

            }
            catch (Exception ex)
            {
                response.IsSucceed = false;
                response.Message = ex.InnerException == null ? ex.Message.ToString() : ex.InnerException.Message.ToString();
                return response;

            }
        }

        public async Task<ResponseData> Remove(string id)
        {
            ResponseData response = new ResponseData();

            try
            {
                var tag = await _context.Tags.DeleteOneAsync(Builders<MTag>.Filter.Eq("Id", id));
                response.Data = tag;
                response.IsSucceed = true;
                return response;

            }
            catch (Exception ex)
            {
                response.IsSucceed = false;
                response.Message = ex.InnerException == null ? ex.Message.ToString() : ex.InnerException.Message.ToString();
                return response;
            }
        }

        public Task<ResponseData> RemoveAll()
        {
            throw new NotImplementedException();
        }

        public async Task<ResponseData> Update(string id, MTag device)
        {
            ResponseData response = new ResponseData();

            try
            {
                var result = await _context.Tags.ReplaceOneAsync(x => x.Id == id, device);
                response.Data = result;
                response.IsSucceed = true;
                return response;

            }
            catch (Exception ex)
            {
                response.IsSucceed = false;
                response.Message = ex.InnerException == null ? ex.Message.ToString() : ex.InnerException.Message.ToString();
                return response;
            }
            
        }



    }
}
