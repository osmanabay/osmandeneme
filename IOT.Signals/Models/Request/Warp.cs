﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Signals.Models.Request
{
    public class Warp
    {
        public string No { get; set; }
        public string Meter { get; set; }
        public string LeftMeter { get; set; }
    }
}
