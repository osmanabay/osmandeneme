﻿namespace IOT.Signals.Models.Request
{
    public class WarpRequest
    {
        public Warp Warp { get; set; }
        public int Id { get; set; }
        public bool RequestType { get; set; }
    }
}
