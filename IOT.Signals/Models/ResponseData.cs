﻿namespace IOT.Signals
{
    public class ResponseData
    {
        public int Id { get; set; }
        public bool IsSucceed { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
