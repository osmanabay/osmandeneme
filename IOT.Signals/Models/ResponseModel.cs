﻿using IOT.Api.Models;
using System.Collections.Generic;

namespace IOT.Signals.Models.Abstract
{
    public class ResponseModel:IResponseModel
    {
        public string Message { get; set; }
        public List<SignalData> SignalDatas{ get; set; }
        public List<SignalData> SignalDataDeviceGroups { get; set; }
        public ResponseModel()
        {
            this.SignalDatas = new List<SignalData>();
            this.SignalDataDeviceGroups = new List<SignalData>();

        }
    }
}
