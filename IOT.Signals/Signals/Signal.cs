﻿using IOT.Api.Models;
using IOT.Signals.Models;
using IOT.Signals.Models.Abstract;
using IOT.Signals.Models.Concrete;
using IOT.Signals.Models.MongoDB.Concrete;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IOT.Signals.Signals
{
    public class Signal : ISignal<AdvantechResponseModel>
    {
        public static string ConnectionUrl { get; set; }
        public static string ProjectName { get; set; }
        private readonly ITagRepository<MTag> _deviceRepository;

        public Signal(ITagRepository<MTag> deviceRepository)
        {
            _deviceRepository = deviceRepository;
        }

        public async Task<AdvantechResponseModel> Get()
        {
            AdvantechResponseModel model = new AdvantechResponseModel();
            try
            {
                //Mongo Taglar gelsin
                List<MTag> Tags = GetTagsFromMongo();

                //Advatech Tagleri
                var aTags = Tags.Where(x => x.SignalFromWhere == "1" && x.Status != "0").ToList();
                var tagList = new List<MTag>();
                //Advantech'ten yeni değerleri al
                if (aTags.Count > 0)
                {
                    model = GetValuesFromAdvantech(aTags);
                    // Advanech'ten gelen veriler ile mongodb verilerin değişim kontrolü
                    foreach (var item in model.Values)
                    {
                        if (item.Value == "-1")
                        {
                            //-1 ile sinyal gelmiyor
                        }
                        else if (item.Value == "-2")
                        {
                            // -2 tanımlama yok
                        }
                        else if (aTags.Where(x => x.Name == item.Name).Any())
                        {
                            var mtag = aTags.SingleOrDefault(x => x.Name == item.Name);
                            mtag.Value = item.Value;
                            var newMtag =await Api.Api.TagControlAsync(mtag);
                            await _deviceRepository.Update(newMtag.Id.ToString(), newMtag);
                            tagList.Add(newMtag);
                        }
                    }
                }




                //Plc Tagleri
                //var pTags = Tags.Where(x => x.SignalFromWhere == "2" && x.Status != "0").ToList();
                var pTags = Tags.Where(x => x.SignalFromWhere == "2").ToList();

                if (pTags.Count > 0)
                {
                    foreach (var item in pTags)
                    {
                        if (item.Status != "0" && item.DailyTeamWorkOrderId != "0")
                        {
                            var mTag = await Api.Api.TagControlAsync(item);
                            if (mTag != null)
                            {
                                await _deviceRepository.Update(item.Id.ToString(), mTag);
                            }
                        }
                        else if (false) { } // iş emri yoksa cihaz çalışıyorsa şunları yap
                        tagList.Add(item);
                    }
                }



                //DeviceName'ine göre grupla
                if (Tags.Count > 0)
                {
                    IEnumerable<SignalData> signalDatas = GroupByDeviceName(tagList);
                    IEnumerable<SignalData> signalDataDeviceGroups = GroupByDeviceGroupName(tagList);

                    model.Values = null;
                    model.SignalDatas = signalDatas.ToList();
                    model.SignalDataDeviceGroups = signalDataDeviceGroups.ToList();
                }

                model.Message = "Başarılı";
                return model;


            }
            catch (Exception ex)
            {
                model = new AdvantechResponseModel
                {
                    Message = ex.InnerException == null
                            ? ex.Message
                            : ex.InnerException.Message
                };
                return model;
            };
        }
        // İş emri yokken üretim başlatılırsa iş emri oluştur
        public ResponseData CreateWorkOrder(MTag mTag)
        {
            ResponseData response = new ResponseData();
            try
            {
                mTag.WeftAmount = "0";
                mTag.TotalBoltWeftAmount = "0";
                mTag.TotalShiftWeftAmount = "0";
                mTag.WeftAmountByBolt = "0";
                mTag.ProducedMeter = "0";
                mTag.TotalShiftMeter = "0";
                mTag.TotalBoltMeter = "0";
                mTag.DisplayProducedMeter = "0";

                mTag.ProductionContinue = true;
                response = Api.Api.AddWorkOrder(mTag);
                response.IsSucceed = true;
                return response;

            }
            catch (Exception)
            {
                response.IsSucceed = false;
                return response;
            }
        }

        private static IEnumerable<SignalData> GroupByDeviceName(List<MTag> tagList)
        {
            return from v in tagList
                   group v by v.Name.Substring(0, v.Name.IndexOf('_')) into g
                   select new SignalData { DeviceName = g.Key, Tags = g.ToList() };
        }
        private static IEnumerable<SignalData> GroupByDeviceGroupName(List<MTag> tagList)
        {
            return from v in tagList
                   group v by v.DeviceGroupName into g
                   select new SignalData { DeviceGroupName = g.Key, Tags = g.ToList() };
        }

        private static AdvantechResponseModel GetValuesFromAdvantech(List<MTag> aTags)
        {
            var serviceUrl = Path.Combine(ConnectionUrl, "Json/GetTagValue/", ProjectName);
            var encoding = new UTF8Encoding();

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            var advantechTags = new { Tags = aTags };
            var tagsJson = JsonConvert.SerializeObject(advantechTags, jsonSerializerSettings);

            var dataByte = encoding.GetBytes(tagsJson);

            var postRequest = (HttpWebRequest)WebRequest.Create(serviceUrl);
            postRequest.Method = "POST";
            postRequest.Credentials = new NetworkCredential("admin", string.Empty);
            postRequest.ContentType = "application/json";
            postRequest.KeepAlive = false;
            postRequest.Timeout = 5000;
            postRequest.ContentLength = dataByte.Length;

            Stream postStream = postRequest.GetRequestStream();
            postStream.Write(dataByte, 0, dataByte.Length);

            HttpWebResponse POSTResponse = (HttpWebResponse)postRequest.GetResponse();
            StreamReader reader = new StreamReader(POSTResponse.GetResponseStream(), Encoding.UTF8);

            var result = reader.ReadToEnd().ToString();
            var model = JsonConvert.DeserializeObject<AdvantechResponseModel>(result);
            return model;
        }

        private List<MTag> GetTagsFromMongo()
        {
            Api.Api mongoApi = new Api.Api(_deviceRepository);
            List<MTag> Tags = mongoApi.GetTags();
            return Tags;
        }

        public async Task<bool> ResetAdvantech(string deviceName)
        {
            try
            {
                //Mongo Gelsin
                Api.Api mongoApi = new Api.Api(_deviceRepository);
                List<MTag> Tags = mongoApi.GetTags();

                var serviceUrl = Path.Combine(ConnectionUrl, "Json/SetTagValue/", ProjectName);
                var encoding = new UTF8Encoding();

                // İş emri bitince cihazın ilgili taglerini sıfırlama
                #region Value Reset
                Tags = Tags.Where(x => x.Name.Contains(deviceName)).ToList();

                for (int i = 0; i < Tags.Count; i++)
                {
                    Tags[i].Value = "0";
                }
                #endregion

                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                };
                var advantechTags = new { Tags };
                var tagsJson = JsonConvert.SerializeObject(advantechTags, jsonSerializerSettings);

                var dataByte = encoding.GetBytes(tagsJson);


                var postRequest = (HttpWebRequest)WebRequest.Create(serviceUrl);
                postRequest.Method = "POST";
                postRequest.Credentials = new NetworkCredential("admin", string.Empty);
                postRequest.ContentType = "application/json";
                postRequest.KeepAlive = false;
                postRequest.Timeout = 5000;
                postRequest.ContentLength = dataByte.Length;

                Stream postStream = postRequest.GetRequestStream();
                postStream.Write(dataByte, 0, dataByte.Length);

                HttpWebResponse POSTResponse = (HttpWebResponse)postRequest.GetResponse();
                StreamReader reader = new StreamReader(POSTResponse.GetResponseStream(), Encoding.UTF8);

                var result = reader.ReadToEnd().ToString();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
