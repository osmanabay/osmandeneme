﻿using IOT.Signals.Hubs;
using IOT.Signals.Models;
using IOT.Signals.Models.Abstract;
using IOT.Signals.Models.MongoDB.Concrete;
using IOT.Signals.Models.MongoDB.Model;
using IOT.Signals.Models.MongoDB.Repository;
using IOT.Signals.Signals;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IOT.Signals
{
    public class Startup
    {
        private IConfigurationRoot _appSettings;
        public Startup(IConfiguration configuration,
                       IHostingEnvironment env)
        {
            Configuration = configuration;
            ConfigurationRoot = (IConfigurationRoot)configuration;
            _appSettings = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json")
          .Build();
        }

        public IConfiguration Configuration { get; }
        public IConfigurationRoot ConfigurationRoot { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //services.AddScoped<DBIOTContext>();
            //services.AddScoped<Api.Api>();
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder
                .AllowAnyOrigin()
                //.SetIsOriginAllowed(isOriginAllowed: _ => true)

                //.SetIsOriginAllowed((host) => true) //krayteks aktif olan kod
                //.WithOrigins("http://192.168.1.10:1011")
                //.WithOrigins(
                //        "http://192.168.1.100:1000",
                //        "https://192.168.0.174:1000",
                //        "http://192.168.0.174:1011",
                //        "https://192.168.0.174:1011")
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials();


            }));
            services.AddSignalR();
            Api.Api.ApiUrl = _appSettings.GetSection("ApiUrl").Value;
            Api.Api.SignalUrl = _appSettings.GetSection("SignalUrl").Value;
            Signal.ConnectionUrl = _appSettings.GetSection("ServiceRootUrl").Value;
            Signal.ProjectName = _appSettings.GetSection("ProjectName").Value;


            //services.AddMvc();

            services.AddTransient<ITagRepository<MTag>, TagRepository>();
            services.AddScoped<ISignal<AdvantechResponseModel>, Signal>();
            services.Configure<Settings>(s => { s.iConfigurationRoot = ConfigurationRoot; });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                             .AddJsonOptions(opt => opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("CorsPolicy");
            //app.UseWebSockets();
            app.UseSignalR(routes =>
            {
                routes.MapHub<SignalHub>("/notify");
            });

            app.UseMvc();
        }
    }
}
