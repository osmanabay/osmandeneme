﻿using IOT.Business.Abstract;
using IOT.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IOT.Web.Controllers
{
    public class FactoryController:Controller
    {
        private IFactoryService _factoryService;
        public FactoryController(IFactoryService factoryService)
        {
            _factoryService = factoryService;
        }

        public ActionResult Index()
        {
            var model = new FactoryListViewModel {
                Fabrikalar = _factoryService.GetAll()
            };
            return View(model);
        }

    }
}
