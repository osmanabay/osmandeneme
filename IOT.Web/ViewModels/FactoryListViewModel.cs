﻿using System.Collections.Generic;
using IOT.Entities;

namespace IOT.Web.ViewModels
{
    public class FactoryListViewModel
    {
        public List<Factory> Fabrikalar { get; set; }
    }
}