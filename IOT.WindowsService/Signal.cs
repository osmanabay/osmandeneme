﻿
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IOT.WindowsService
{
    public partial class Signal : ServiceBase
    {
        private Thread thread;

        HttpClient _signalClient;

        bool control = true;
        public Signal()
        {
            InitializeComponent();
            _signalClient = new HttpClient();
        }

        protected override void OnStart(string[] args)
        {
            InitThread();
        }

        protected override void OnStop()
        {
        }

        private void InitThread()
        {
            thread = new Thread(new ThreadStart(GetSignal));
            thread.Start();
        }

        private void  GetSignal()
        {
            for (int i = 0; control; i++)
            {
                try
                {
                    HttpClient client = _signalClient;
                    client.GetAsync("http://localhost:58221/api/signal");
                    //client.GetAsync("http://localhost:1002/api/signal");


                    //Yalın
                    //HttpClient client = _signalClient;
                    //client.GetAsync("http://172.23.165.248:1002/api/signal");

                    //Bodoplast Server
                    //var httpResponse = client.GetAsync("http://192.168.1.252:1001/api/signal").Result;
                    // client.GetAsync("http://192.168.1.252:1001/api/signal"); 

                    //Kırayteks Old Server
                    //var httpResponse = client.GetAsync("http://192.168.0.174:1001/api/signal").Result;
                    //client.GetAsync("http://192.168.0.174:1001/api/signal");

                    //Test PC
                    //var httpResponse = client.GetAsync("http://192.168.1.105:1002/api/signal").Result;
                    //client.GetAsync("http://192.168.1.105:1002/api/signal");


                    //Kırayteks New Server
                    //var httpResponse = client.GetAsync("http://192.168.0.98:50001/api/signal").Result; 
                    //client.GetAsync("http://192.168.0.98:50001/api/signal");

                    //Local
                    //client.GetAsync("http://192.168.1.100:1002/api/signal");
                    //var httpResponse = client.GetAsync("http://192.168.1.100:1002/api/signal").Result;

                    Thread.Sleep(1000);
                }
                catch (Exception)
                {

                }
            }

        }
    }
}
