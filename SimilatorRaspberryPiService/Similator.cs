﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimilatorRaspberryPiService
{
    public partial class Similator : ServiceBase
    {
        private Thread thread;

        HttpClient _signalClient;

        private Random rndStatus = new Random();
        private Random rndStop = new Random();
        int total = 0;

        bool control = true;
        public Similator()
        {
            InitializeComponent();
            _signalClient = new HttpClient();

        }
        protected override void OnStart(string[] args)
        {
            InitThread();
        }


        protected override void OnStop()
        {
        }

        private void InitThread()
        {
            thread = new Thread(new ThreadStart(PostRassberryPi));
            thread.Start();
        }

        private async void PostRassberryPi()
        {
            try
            {
            
                while (true)
                {
                    RassberryPiDto dto = new RassberryPiDto();
                  
                    dto.id = "25";

                    int status = rndStatus.Next(1, 18);
                    if (status < 17)
                    {
                        status = 1;
                        dto.value = "45";
                        total = 45;
                        dto.value2 = "270";
                    }
                    else
                    {
                        int stopId = rndStop.Next(3, 12);
                        if (stopId == 11)
                        {
                            status = 21;
                        }
                        else
                        {
                            status = stopId;

                        }
                    }
                    dto.status = status.ToString();



                    string jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(dto);
                    using (var client = new HttpClient())
                    {
                        dto.value = total.ToString();
                        var httpResponse = await client.PostAsync("http://localhost:58221/api/signal/fromRasberypi", new StringContent(jsonBody,
                                        Encoding.UTF8,
                                        "application/json"));
                        //dto.value = total.ToString();
                        //var httpResponse = await client.PostAsync("http://192.168.1.100:1002/api/signal/fromRasberypi", new StringContent(jsonBody,
                        //                Encoding.UTF8,
                        //                "application/json"));
                        //var httpResponse = await client.PostAsync("http://192.168.1.100:1002/api/signal/fromRasberypi", new StringContent(jsonBody,
                        //              Encoding.UTF8,
                        //              "application/json"));
                        //var httpResponse = await client.PostAsync("http://192.168.1.105:1002/api/signal/fromRasberypi", new StringContent(jsonBody,
                        //             Encoding.UTF8,
                        //             "application/json"));

                        //var httpResponse = await client.PostAsync("http://192.168.0.98:50001/api/signal/fromRasberypi", new StringContent(jsonBody,
                        //              Encoding.UTF8,
                        //              "application/json"));

                        if (!httpResponse.IsSuccessStatusCode)
                        {
                            total += 45;
                        }
                    }

                    Thread.Sleep(10000);
                }
            }
            catch (Exception ex)
            {
              
            }


        }

    }
}
